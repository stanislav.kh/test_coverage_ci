import { Validator } from "class-validator";

import { ContainsSpecialCharacter } from "./contains-special-character";

class Obj {
    @ContainsSpecialCharacter()
    public title?: number;

    constructor(title: any) {
        this.title = title;
    }
}

describe("contains-special-character", () => {
    test("should pass", async () => {
        const obj = new Obj("Test!");
        const validator = new Validator();
        const err = await validator.validate(obj);
        expect(err.length).toBe(0);
    });

    test("should fail", async () => {
        const obj = new Obj("Test");
        const validator = new Validator();
        const err = await validator.validate(obj);
        expect(err.length).toBe(1);
    });
});
