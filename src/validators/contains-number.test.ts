import { Validator } from "class-validator";

import { ContainsNumber } from "./contains-number";

class Obj {
    @ContainsNumber()
    public title?: number;

    constructor(title: any) {
        this.title = title;
    }
}

describe("contains-number", () => {
    test("should pass", async () => {
        const obj = new Obj("Test1");
        const validator = new Validator();
        const err = await validator.validate(obj);
        expect(err.length).toBe(0);
    });

    test("should fail", async () => {
        const obj = new Obj("Test");
        const validator = new Validator();
        const err = await validator.validate(obj);
        expect(err.length).toBe(1);
    });
});
