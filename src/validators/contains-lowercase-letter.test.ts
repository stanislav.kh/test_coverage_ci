import { Validator } from "class-validator";

import { ContainsLowercaseLetter } from "./contains-lowercase-letter";

class Obj {
    @ContainsLowercaseLetter()
    public title?: string;

    constructor(title: any) {
        this.title = title;
    }
}

describe("contains-lowercase-letter", () => {
    test("should pass", async () => {
        const obj = new Obj("Test");
        const validator = new Validator();
        const err = await validator.validate(obj);
        expect(err.length).toBe(0);
    });

    test("should fail", async () => {
        const obj = new Obj("TEST");
        const validator = new Validator();
        const err = await validator.validate(obj);
        expect(err.length).toBe(1);
    });
});
