import { Validator } from "class-validator";

import { ContainsUppercaseLetter } from "./contains-uppercase-letter";

class Obj {
    @ContainsUppercaseLetter()
    public title?: number;

    constructor(title: any) {
        this.title = title;
    }
}

describe("contains-uppercase-letter", () => {
    test("should pass", async () => {
        const obj = new Obj("Test");
        const validator = new Validator();
        const err = await validator.validate(obj);
        expect(err.length).toBe(0);
    });

    test("should fail", async () => {
        const obj = new Obj("test");
        const validator = new Validator();
        const err = await validator.validate(obj);
        expect(err.length).toBe(1);
    });
});
