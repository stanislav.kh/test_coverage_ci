import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from "class-validator";

@ValidatorConstraint()
class ContainsUppercaseLetterConstraint implements ValidatorConstraintInterface {
    public validate(value: any, _: ValidationArguments): boolean {
        const regexp = new RegExp(/(?=.*[A-Z])/);
        return regexp.test(value);
    }
}

export function ContainsUppercaseLetter(validationOptions?: ValidationOptions): any {
    return (object: {}, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: ContainsUppercaseLetterConstraint,
        });
    };
}
