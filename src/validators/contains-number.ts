import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from "class-validator";

@ValidatorConstraint()
class ContainsNumberConstraint implements ValidatorConstraintInterface {
    public validate(value: any, _: ValidationArguments): boolean {
        const regexp = new RegExp(/(?=.*[0-9])/);
        return regexp.test(value);
    }
}

export function ContainsNumber(validationOptions?: ValidationOptions): any {
    return (object: {}, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: ContainsNumberConstraint,
        });
    };
}
