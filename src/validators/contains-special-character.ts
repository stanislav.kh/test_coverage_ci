import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from "class-validator";

@ValidatorConstraint()
class ContainsSpecialCharacterConstraint implements ValidatorConstraintInterface {
    public validate(value: any, _: ValidationArguments): boolean {
        const regexp = new RegExp(/.*[!@#\$%\^&]/);
        return regexp.test(value);
    }
}

export function ContainsSpecialCharacter(validationOptions?: ValidationOptions): any {
    return (object: {}, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: ContainsSpecialCharacterConstraint,
        });
    };
}
