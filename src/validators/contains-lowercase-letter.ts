import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from "class-validator";

@ValidatorConstraint()
class ContainsLowercaseLetterConstraint implements ValidatorConstraintInterface {
    public validate(value: any, _: ValidationArguments): boolean {
        const regexp = new RegExp(/(?=.*[a-z])/);
        return regexp.test(value);
    }
}

export function ContainsLowercaseLetter(validationOptions?: ValidationOptions): any {
    return (object: {}, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: ContainsLowercaseLetterConstraint,
        });
    };
}
