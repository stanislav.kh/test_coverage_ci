import { registerDecorator, ValidationOptions } from "class-validator";

type decoratorFunc = (object: {}, propertyName: string) => void;

export function DoesNotHaveSingleQuote(validationOptions?: ValidationOptions): decoratorFunc {
    return (object: {}, propertyName: string) => {
        registerDecorator({
            name: "doesNotHaveSingleQuite",
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: {
                validate(value: any): boolean {
                    return typeof value === "string" && !value.includes("'");
                },
            },
        });
    };
}
