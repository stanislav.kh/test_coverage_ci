import * as bodyParser from "body-parser";
import * as express from "express";
import * as http from "http";
import { PayloadTooLargeError, RequestTimeoutError } from "syntropy-common-ts";
import { Service } from "typedi";

import { HttpServerAdapter } from "./http-server-adapter";
import { getEnv } from "../config/env";
import { Logger } from "../config/logger";

@Service()
export class PlatformAdapter implements HttpServerAdapter {
    public name: string = "platform";
    public protocol: "http" = "http";
    public host: string = "0.0.0.0";
    public port: number = 8080;

    constructor() {
        this.$app.use(bodyParser.urlencoded({ extended: true }));
        this.$app.use(
            bodyParser.json({
                limit: getEnv().HTTP_API_MAX_BODY_SIZE,
            })
        );
        // this should be done by api gateway not internally

        // const originsWhitelist = [getEnv().PLATFORM_ADDRESS];

        // if (getEnv().LOCALHOST_CORS_ADDRESS) {
        //     originsWhitelist.push(getEnv().LOCALHOST_CORS_ADDRESS);
        // }

        // const corsMiddleware = cors({
        //     origin: (origin: string | undefined, callback: (err: Error | null, allow?: boolean | undefined) => void): void => {
        //         if (origin && !originsWhitelist.includes(origin)) {
        //             callback(new ForbiddenError("Not allowed by CORS"));
        //         } else {
        //             callback(null, true);
        //         }
        //     },
        //     credentials: true,
        // });

        // this.$app.use(corsMiddleware);

        this.$app.use((_: express.Request, res, next) => {
            const timeoutMs = parseInt(getEnv().HTTP_API_SOCKET_TIMEOUT!) * 1000;
            res.setTimeout(timeoutMs, () => {
                next(new RequestTimeoutError(timeoutMs));
            });

            next();
        });
        this.$app.use((err, req: express.Request, _, next) => {
            if (err instanceof SyntaxError) {
                Logger.error({
                    message: `'${req.method}' '${req.url}' '${JSON.stringify(req.body)}'`,
                    type: "api.syntax-error",
                    header: 500,
                });
            }
            // error is constructed by raw-body package when exceeds body-parser.json fn limit parameter (default 100kb).
            if (err?.type === "entity.too.large" && typeof err.limit === "number" && typeof err.length === "number") {
                return next(new PayloadTooLargeError(err.limit, err.length));
            }

            next();
        });
    }

    public $app: express.Express = express();
    private httpServer: http.Server = http.createServer(this.$app);

    /**
     * Start accepting API requests.
     */
    public async listen(port: number, hostname: string): Promise<http.Server>;
    public async listen(): Promise<http.Server>;
    public async listen(port?: number, hostname?: string): Promise<http.Server> {
        this.port = port || this.port;
        this.host = hostname || this.host;

        return new Promise<http.Server>((resolve, reject) => {
            this.httpServer.on("listening", () => {
                this.httpServer.removeAllListeners("listening");
                this.httpServer.removeAllListeners("error");
                resolve(this.httpServer);
            });
            this.httpServer.on("error", (err) => {
                this.httpServer.removeAllListeners("listening");
                this.httpServer.removeAllListeners("error");
                reject(err);
            });

            this.httpServer.listen(this.port, this.host);
        });
    }

    /**
     * Stop accepting API requests.
     */
    public async close(): Promise<void> {
        return new Promise<void>((resolve) => {
            this.httpServer.close(() => {
                resolve();
            });
        });
    }

    public setHost(host?: string): void {
        if (host != null) {
            this.host = host;
        }
    }

    public setPort(port?: number): void {
        if (port != null) {
            this.port = port;
        }
    }
}
