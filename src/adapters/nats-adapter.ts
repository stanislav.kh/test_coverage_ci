import { Stan, connect } from "node-nats-streaming";
import { Service } from "typedi";
import { Logger } from "../config/logger";

@Service()
export class NatsAdapter {
    private $client?: Stan;

    public get client(): Stan {
        if (!this.$client) {
            throw new Error(`Cannot access NATS client before connecting (${process.pid}).`);
        }

        return this.$client;
    }

    public async connect(clusterId: string, clientId: string, url: string): Promise<Stan> {
        this.$client = connect(clusterId, clientId, { url, ackTimeout: 60_000 });

        return new Promise((resolve, reject) => {
            this.client.on("connect", () => {
                Logger.info({ message: `Service '${clientId}' connected to event bus '${clusterId}'.`, type: "event", header: 200 });
                resolve(this.client);
            });
            this.client.on("error", (err) => {
                reject(err);
            });
        });
    }

    public close(): void {
        this.client.close();
    }

    /* tslint:disable:typedef */
    public get on() {
        return this.client.on.bind(this.client);
    }
    /* tslint:enable:typedef */
}
