import { EnvConfig } from "../config/env";
import { Service } from "typedi";
import { Client } from "@sendgrid/client";
import { getEnv } from "../config/env";
import { Contact } from "../components/auth/auth.contracts";

enum SendgridPropKeys {
    domain = "e2_T",
    fullName = "e3_T",
    company = "e5_T",
    marketing = "e8_T",
}

@Service()
export class SendGridAdapter {
    private client: Client = new Client();
    private env: Partial<EnvConfig> = getEnv();

    constructor() {
        this.client.setApiKey(getEnv().SENDGRID_API_KEY!);
    }

    public async addContact(contact: Contact): Promise<void> {
        await this.client.request({
            url: "/v3/marketing/contacts",
            method: "PUT",
            body: {
                list_ids: [this.env.CONTACT_LIST_ID],
                contacts: [
                    {
                        email: contact.email,
                        custom_fields: {
                            [SendgridPropKeys.fullName]: contact.name,
                            [SendgridPropKeys.company]: contact.company,
                            [SendgridPropKeys.domain]: contact.domain,
                            [SendgridPropKeys.marketing]: contact.marketing_email_agreed.toString(),
                        },
                    },
                ],
            },
        });
    }
}
