import { Server } from "http";

export interface HttpServerAdapter {
    name: string;
    port: number;
    host: string;
    protocol: "http";
    setHost(host?: string): void;
    setPort(port?: number): void;
    listen(port: number, hostname: string): Promise<Server>;
    listen(): Promise<Server>;
    close(): Promise<void>;
}
