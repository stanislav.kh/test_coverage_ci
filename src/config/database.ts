import "reflect-metadata";
import * as path from "path";
import { ConnectionOptions, Connection, createConnection as typeormCreateConnection } from "typeorm";
import { LoggerOptions } from "typeorm/logger/LoggerOptions";
import { getEnv } from "../config/env";
import { PostgresConnectionCredentialsOptions } from "typeorm/driver/postgres/PostgresConnectionCredentialsOptions";
import { Logger } from "./logger";

export namespace Database {
    export function getConnectionOptions(name: string = "default", logging: LoggerOptions = ["error"]): ConnectionOptions {
        let ca: string | undefined;
        if (getEnv().POSTGRES_CA) {
            ca = getEnv().POSTGRES_CA!;
        }

        const getDbOpts = (host?: string | null, database?: string | null): PostgresConnectionCredentialsOptions => ({
            host: host ?? getEnv().POSTGRES_HOST,
            port: getEnv().POSTGRES_PORT,
            username: getEnv().POSTGRES_USER,
            password: getEnv().POSTGRES_PASSWORD,
            database: database ?? getEnv().POSTGRES_DB,
            ssl: getEnv().POSTGRES_CA && ca ? { rejectUnauthorized: true, ca } : false,
        });

        let connectionOptions: ConnectionOptions = {
            name: name,
            type: "postgres",
            replication: {
                master: getDbOpts(),
                slaves: [...(getEnv().POSTGRES_HOST_REPLICA != null ? [getDbOpts(getEnv().POSTGRES_HOST_REPLICA)] : [])],
            },
            synchronize: false,
            entities: [path.join(__dirname, "../components/**/*.model.ts")],
            logging: logging,
        };

        if (getEnv().REDIS_HOST) {
            connectionOptions = {
                ...connectionOptions,
                cache: {
                    type: "redis",
                    duration: (getEnv().REDIS_CACHE_DURATION ? parseInt(getEnv().REDIS_CACHE_DURATION!) : 60 * 60) * 1_000,
                    options: {
                        host: getEnv().REDIS_HOST,
                    },
                    alwaysEnabled: false,
                },
            };
        }

        return process.env.NODE_ENV === "test"
            ? ({
                  ...connectionOptions,
                  ...{
                      replication: {
                          master: getDbOpts(null, `${getEnv().POSTGRES_DB}_tests`),
                          slaves: [
                              ...(getEnv().POSTGRES_HOST_REPLICA != null
                                  ? [getDbOpts(getEnv().POSTGRES_HOST_REPLICA, `${getEnv().POSTGRES_DB}_tests`)]
                                  : []),
                          ],
                      },
                  },
              } as ConnectionOptions)
            : connectionOptions;
    }

    export function getTestsConnectionOptions(): ConnectionOptions {
        return {
            ...Database.getConnectionOptions(),
            database: `${getEnv().POSTGRES_DB}_tests`,
        } as ConnectionOptions;
    }

    /**
     * Register TypeDI when creating new connection.
     * @link https://github.com/typeorm/typeorm-typedi-extensions#installation
     */
    export async function createConnection(options: ConnectionOptions): Promise<Connection> {
        try {
            const c = await typeormCreateConnection(options);
            return c;
        } catch (err: any) {
            Logger.error({
                message: err,
                type: "unhandled-rejection",
                header: 500,
            });
            throw err;
        }
    }
}
