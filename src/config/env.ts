import * as path from "path";
import { config as dotenvConfig } from "dotenv";

// FIXME: all types returned as strings.
export interface EnvConfig {
    /**
     * Service API address, e.g. http://syntropy-service.syntropystack.com. Used in swagger.json as a host/server url.
     */
    SERVICE_API_ADDRESS: string;
    /**
     * Controller version. Usually supplied by build and CI/CD tools.
     */
    VERSION: string;
    /**
     * SYNTROPY controller secret key.
     */
    SECRET: string;
    /**
     * SYNTROPY controller HTTP API port.
     */
    HTTP_API_PORT: number;
    /**
     * SYNTROPY controller HTTP API host.
     */
    HTTP_API_HOST: string;
    /**
     * Controls the maximum request body size. If this is a number,
     * then the value specifies the number of bytes; if it is a string,
     * the value is passed to the bytes library for parsing. Defaults to '100kb'.
     */
    HTTP_API_MAX_BODY_SIZE: string;
    /**
     * Sets the timeout value for sockets in seconds.
     */
    HTTP_API_SOCKET_TIMEOUT: string;
    /**
     * NATS cluster id.
     */
    NATS_CLUSTER_ID: string;
    /**
     * NATS url, i.e.: http://127.0.0.1:4222.
     * set this to the actual host name from docker-compose file, for the dockerized service localhost is not valid
     */
    NATS_URL: string;
    /**
     * NATS auth service id.
     */
    NATS_SERVICE_CLIENT_ID: string;
    /**
     * Time to wait for event retreansmission in seconds.
     */
    NATS_ACK_WAIT: string;
    /**
     * PostgreSQL database name.
     */
    POSTGRES_DB: string;
    /**
     * PostgreSQL user name.
     */
    POSTGRES_USER: string;
    /**
     * PostgreSQL user password.
     */
    POSTGRES_PASSWORD: string;
    /**
     * PostgreSQL hostname.
     */
    POSTGRES_HOST: string;
    /**
     * PostgreSQL read replicate hostname.
     * set this to the actual host name from docker-compose file, for the dockerized service localhost is not valid
     */
    POSTGRES_HOST_REPLICA;
    /**
     * PostgreSQL port.
     */
    POSTGRES_PORT: number;
    /**
     * PostgreSQL CA string.
     */
    POSTGRES_CA: string;
    /**
     * PostgreSQL admin dashboard login email.
     */
    PGADMIN_EMAIL: string;
    /**
     * PostgreSQL admin dashboard password.
     */
    PGADMIN_PASSWORD: string;
    /**
     * PostgreSQL admin dashboard port.
     */
    PGADMIN_PORT: number;
    /**
     * Redis hostname.
     * set this to the actual host name from docker-compose file, for the dockerized service localhost is not valid
     */
    REDIS_HOST: string;
    /**
     * Redis key cache duration in seconds.
     */
    REDIS_CACHE_DURATION: string;
    /**
     * Logs file.
     */
    LOG_FILENAME: string;
    /**
     * Elasticsearch endpoint address.
     */
    ELASTICSEARCH_ENDPOINT_ADDRESS: string;
    /**
     * Service log output format.
     */
    LOG_FORMAT: "ECS";
    /**
     * Elasticsearch apm token.
     */
    ELASTIC_APM_SECRET_TOKEN: string;
    /**
     * Elasticsearch apm token.
     */
    ELASTIC_APM_SERVER_URL: string;
    /**
     * Elasticsearch apm environment.
     */
    ELASTIC_APM_ENVIRONMENT: string;
    /**
     * Duration of jwt token.
     */
    JWT_TOKEN_DURATION: number;
    /**
     * Duration of jwt refresh token.
     */
    JWT_REFRESH_TOKEN_DURATION: number;
    /**
     * Api key that allows traefik to access auth service.
     */
    TRAEFIK_API_KEY: string;
    /**
     * Sendgrid api key.
     */
    SENDGRID_API_KEY: string;
    /**
     * Azure api key.
     */
    AZURE_API_KEY: string;
    /**
     * Azure api key.
     */
    AZURE_TOKEN_URL: string;
    /**
     * Azure client id.
     */
    AZURE_CLIENT_ID: string;
    /**
     * Azure scope.
     */
    AZURE_SCOPE: string;
    /**
     * Azure grant type.
     */
    AZURE_GRANT_TYPE: string;
    /**
     * Azure client secret.
     */
    AZURE_CLIENT_SECRET: string;
    /**
     * Azure extenstion property prefix.
     */
    AZURE_EXTENSION_PROP: string;
    /**
     * Azure public key url.
     */
    AZURE_JWKS_URI: string;
    /**
     * Azure resource owner password credentials flow token url.
     */
    AZURE_ROPC_TOKEN_URL: string;
    /**
     * Azure resource owner password credentials flow scopes.
     */
    AZURE_ROPC_SCOPES: string;
    /**
     * Azure resource owner password credentials flow grant type.
     */
    AZURE_ROPC_GRANT_TYPE: string;
    /**
     * Azure tenant name.
     */
    AZURE_TENANT: string;
    /**
     * JWT cookies options.
     */
    JWT_COOKIES_OPTIONS: string;
    /**
     * Captcha verification service url.
     */
    CAPTCHA_URL: string;
    /**
     * Captcha verification service secret.
     */
    CAPTCHA_SECRET: string;
    /**
     * There are cases when logging is optional and can be skipped (e.g., when running tests).
     */
    SYNTROPY_AUTH_SERVICE_LOG_LEVEL: string;
    /**
     * Platform UI address.
     */
    PLATFORM_ADDRESS: string;
    /**
     * Address with allowed cors requests.
     */
    LOCALHOST_CORS_ADDRESS: string;
    /**
     * Max database users count.
     */
    MAX_USERS_COUNT: string;
    /**
     * Endpoint used for user information.
     */
    CONTACT_LIST_ID: string;
    /**
     * Should service run corupted users fix script on start.
     */
    AUTO_FIX_AZURE_CORRUPTED_USERS: boolean;
    /**
     * MFA issuer.
     */
    MFA_ISSUER: string;
    /**
     * Maximum amount of unacknowledged messages that can be sent to specific subscriber
     */
    NATS_MAX_IN_FLIGHT: string;
    /**
     * Maximum times message should be redelivered before getting automatically acknowledged
     */
    NATS_MAX_REDELIVERY_COUNT: string;
    /**
     * Controller service base url
     */
    CONTROLLER_SERVICE_BASE_URL;
    /**
     * Controller service api key
     */
    CONTROLLER_SERVICE_API_KEY;
}

const envDefaults: Partial<EnvConfig> = {
    HTTP_API_HOST: "0.0.0.0",
    HTTP_API_MAX_BODY_SIZE: "100kb",
    HTTP_API_SOCKET_TIMEOUT: "120",
    JWT_TOKEN_DURATION: 7 * 24 * 60 * 60,
    JWT_REFRESH_TOKEN_DURATION: 4 * 7 * 24 * 60 * 60,
    JWT_COOKIES_OPTIONS: "HttpOnly; Path=/; SameSite=Strict; Secure;",
    SYNTROPY_AUTH_SERVICE_LOG_LEVEL: "7",
    REDIS_CACHE_DURATION: "60",
    AUTO_FIX_AZURE_CORRUPTED_USERS: false,
    MFA_ISSUER: "Syntropy Stack",
    NATS_URL: "nats://127.0.0.1:4222",
    NATS_CLUSTER_ID: "event_bus",
    NATS_SERVICE_CLIENT_ID: "auth-service",
    NATS_MAX_IN_FLIGHT: "100",
    NATS_MAX_REDELIVERY_COUNT: "10",
    NATS_ACK_WAIT: "20",
    POSTGRES_DB: "noia_controller_dev",
    POSTGRES_HOST: "localhost",
    POSTGRES_USER: "dev",
    POSTGRES_PASSWORD: "postgres",
};

export function getEnv(): Partial<EnvConfig> {
    return process.env as Partial<EnvConfig>;
}

export function loadDefaultsAndTestEnv(): void {
    if (process.env.NODE_ENV === "test") {
        const dotenv = dotenvConfig({ path: path.resolve(process.cwd(), "test.env") });
        if (dotenv.error) {
            const error: NodeJS.ErrnoException = dotenv.error;
            // If file does not exist, do nothing.
            if (error.code !== "ENOENT") {
                throw new Error("Failed to parse .env configuration file.");
            }
        }
    }

    for (const [key, value] of Object.entries(envDefaults)) {
        if (process.env[key] === undefined) {
            // @ts-ignore
            process.env[key] = value;
        }
    }
}
