import { getEnv } from "./env";
import { ConsoleTransport } from "../logger/console-transport";
import { Transport, Logger as CommonLogger, Severity } from "syntropy-common-ts";
import { ECSConsoleTransport } from "../logger/ecs-console-transport";
import { FileTransport } from "../logger/file-transport";
import { ECSConsoleMessage } from "../logger/es-console-transport.contract";
import { ApmTransport } from "../logger/apm-transport";
import { LogLevel } from "../common/types";

const defaultTransports: Transport[] = [];
defaultTransports.push(new ConsoleTransport());

if (typeof getEnv().LOG_FILENAME === "string" && getEnv().LOG_FILENAME!.length > 0) {
    defaultTransports.push(new FileTransport(), new ApmTransport());
}

if (getEnv().LOG_FORMAT === "ECS") {
    defaultTransports.push(new ECSConsoleTransport());
} else {
    defaultTransports.push(new ConsoleTransport());
}

const skipLogFilter = (severity: Severity) => !(parseInt(getEnv().SYNTROPY_AUTH_SERVICE_LOG_LEVEL!) && LogLevel[severity]);
export const Logger = new CommonLogger<ECSConsoleMessage>(defaultTransports, skipLogFilter);
