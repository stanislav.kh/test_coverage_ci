import { CreateAzureUserListener, MergeSocialAccountListener, SendVerificationEmailListener } from "./components/auth/auth.listeners";
import { RedisService } from "./components/redis/redis.service";
import "reflect-metadata";
import { Container } from "typedi";
import { Connection } from "typeorm";
import { useContainer } from "typeorm";

import { NatsAdapter } from "./adapters/nats-adapter";
import { Database } from "./config/database";
import { getEnv, loadDefaultsAndTestEnv } from "./config/env";
import { HttpServerAdapter } from "./adapters/http-server-adapter";
import { PlatformAdapter } from "./adapters/platform-adapter";
import { registerPlatformAdapter } from "./register-platform-adapter";
import { EventEmitter } from "typeorm/platform/PlatformTools";
import { Logger } from "./config/logger";
import { AuthService } from "./components/auth/auth.service";

interface StartOpts {
    skipRunMigrations: boolean;
}

interface Controller {
    start: (opts?: StartOpts) => Promise<void>;
    stop: () => Promise<void>;
}

// TODO: refactor to move some parts to common, for less boilerplate when creating new microservice
export function controller(): Controller {
    const disconnectStaleInterval: NodeJS.Timer | null = null;
    const newServersTimeout: NodeJS.Timeout | null = null;
    const adapters: HttpServerAdapter[] = [];
    let connection: Connection | null = null;

    const uncaughtExceptionEv = (err: Error) => {
        Logger.error({ message: err, type: "uncaught-exception", header: 500 });
    };

    const unhandledRejectionEv = (reason: {} | null | undefined) => {
        Logger.error({
            message: reason,
            type: "unhandled-rejection",
            header: 500,
        });
    };

    const sigintEv = async () => {
        await stop();
        process.exit(0);
    };

    const stop = async (): Promise<void> => {
        if (disconnectStaleInterval != null) {
            clearInterval(disconnectStaleInterval);
        }
        if (newServersTimeout != null) {
            clearTimeout(newServersTimeout);
        }
        Container.get(NatsAdapter).close();
        Container.get(RedisService).isCacheAvailable() && Container.get(RedisService).quit!();

        await Promise.all(adapters.map((a) => a.close()));
        for (const adapter of adapters) {
            Logger.info({
                message: `NOIA Auth Service controller ${adapter.name} API endpoint closed.`,
                type: "main",
                header: 200,
            });
        }

        if (connection != null) {
            await connection.close();
        }

        process.removeListener("uncaughtException", uncaughtExceptionEv);
        process.removeListener("unhandledRejection", unhandledRejectionEv);
        process.removeListener("SIGINT", sigintEv);
    };

    const start = async (_?: StartOpts): Promise<void> => {
        // loads default values and test file env variables
        loadDefaultsAndTestEnv();

        // increase global event emitter limit
        EventEmitter.defaultMaxListeners = 30;

        // log uncaught and unhandled errors
        // https://nodejs.org/api/process.html#process_warning_using_uncaughtexception_correctly
        process.on("uncaughtException", uncaughtExceptionEv);
        process.on("unhandledRejection", unhandledRejectionEv);

        // set dependency injections container to be used
        useContainer(Container);

        // create connection to be used by entire application
        connection = await Database.createConnection(Database.getConnectionOptions());
        const platformAdapter = Container.get(PlatformAdapter);
        platformAdapter.setPort(getEnv().HTTP_API_PORT);
        platformAdapter.setHost(getEnv().HTTP_API_HOST);
        registerPlatformAdapter();
        adapters.push(platformAdapter);

        try {
            (await Container.get(NatsAdapter).connect(getEnv().NATS_CLUSTER_ID!, getEnv().NATS_SERVICE_CLIENT_ID!, getEnv().NATS_URL!)).on(
                "close",
                () => {
                    sigintEv();
                }
            );

            Container.get(MergeSocialAccountListener).listen({ manualAcks: true });
            Container.get(CreateAzureUserListener).listen({ manualAcks: true });
            Container.get(SendVerificationEmailListener).listen({ manualAcks: true });

            await Promise.all(adapters.map((a) => a.listen()));

            for (const adapter of adapters) {
                const { name, protocol, host, port } = adapter;
                Logger.info({
                    message: `NOIA Auth Service controller ${name} API endpoint: ${protocol}://${host}:${port}.`,
                    type: "main",
                    header: 200,
                });
            }
        } catch (err: any) {
            Logger.error({ message: err, type: "main", header: 500 });
            await stop();
            process.exit(1);
        }

        Logger.info({
            message: `NOIA Auth Service controller is-cache-available=${Container.get(RedisService).isCacheAvailable()}.`,
            type: "main",
            header: 200,
        });

        // interrupt from keyboard (CTRL+C) signal
        process.once("SIGINT", sigintEv);

        // send listening message to parent process
        // if controller is run as a child process
        if (typeof process.send === "function") {
            process.send("LISTENING");
        }

        if (getEnv().AUTO_FIX_AZURE_CORRUPTED_USERS) {
            await Container.get(AuthService).fixCorruptedUsers();
        }
    };

    return {
        start,
        stop,
    };
}
