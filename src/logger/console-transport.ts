import { Transport } from "syntropy-common-ts";
import * as winston from "winston";

export class ConsoleTransport extends Transport {
    public transport(): winston.transport {
        return new winston.transports.Console({
            handleExceptions: true,
            format: winston.format.combine(winston.format.colorize(), winston.format.timestamp(), this.generateStringFormat()),
            level: "debug",
        });
    }
}
