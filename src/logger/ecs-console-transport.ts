import { Transport } from "syntropy-common-ts";
import * as winston from "winston";
const ecsFormat = require("@elastic/ecs-winston-format");

export class ECSConsoleTransport extends Transport {
    public transport(): winston.transport {
        return new winston.transports.Console({
            handleExceptions: true,
            format: winston.format.combine(ecsFormat()),
        });
    }
}
