import * as winston from "winston";
import { Transport } from "syntropy-common-ts";

import { getEnv } from "../config/env";

export class FileTransport extends Transport {
    public transport(): winston.transport {
        return new winston.transports.File({
            filename: getEnv().LOG_FILENAME,
            format: winston.format.combine(winston.format.timestamp(), this.generateStringFormat()),
        });
    }
}
