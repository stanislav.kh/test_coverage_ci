import * as winston from "winston";
import { Transport } from "syntropy-common-ts";
import * as apm from "elastic-apm-node";
import * as ElasticsearchApmTransport from "@entropy/winston-elasticsearch-apm";

export class ApmTransport extends Transport {
    public transport(): winston.transport {
        return new ElasticsearchApmTransport({
            apm,
        });
    }
}
