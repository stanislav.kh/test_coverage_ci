import { Request, Response, NextFunction } from "express";
import { BaseError, Severity, GenericHelpers } from "syntropy-common-ts";
import { Logger } from "../config/logger";

/**
 * Log controller specific errors.
 */
export function logError(error: any, _: Request, __: Response, next: NextFunction): void {
    let logger = Logger.log.bind(Logger, Severity.Error);
    if (error instanceof BaseError) {
        logger = GenericHelpers.getLoggerByStatusCode(Logger, error.statusCode);
    }

    logger({
        message: [error.message, error.stack, error.query].filter((v) => v != null).join("\n"),
        type: "middleware.error-handler",
        header: error.statusCode || 500,
    });

    next(error);
}
