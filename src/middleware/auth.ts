import * as express from "express";
import { ServerError } from "syntropy-common-ts";
import Container from "typedi";
import { AuthService } from "../components/auth/auth.service";

export async function expressAuthentication(request: express.Request, securityName: string, scopes: string[] = []): Promise<any> {
    const service = Container.get(AuthService);

    if (securityName === "jwt") {
        // @ts-ignore
        const res = await service.validateJwt(request, securityName, scopes);
        return res;
    }

    if (securityName === "api_key") {
        service.validateApiKey(request.headers["x-api-key"], scopes);
        return undefined;
    }

    if (securityName === "azure_api_key") {
        service.validateApiKey(request.headers["x-functions-key"], scopes);
        return undefined;
    }

    throw new ServerError(`Unrecognized security type: '${securityName}'`);
}
