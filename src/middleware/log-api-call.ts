import { Request, Response, NextFunction } from "express";
import { GenericHelpers as GenericHelpersCommon } from "syntropy-common-ts";

import { Logger } from "../config/logger";
import { GenericHelpers } from "../helpers/generic-helpers";

export function logApiCall(req: Request, res: Response, next: NextFunction): void {
    const id = GenericHelpersCommon.getRandomString(16);
    // Record request start time.
    const reqStartAt = process.hrtime();

    const url = getUrl(req);
    const reqLogger = GenericHelpers.getLoggerByStatusCode(res.statusCode, "verbose");
    let msg = [req.method, url, res.statusCode].join(" ");
    reqLogger({
        message: msg,
        type: "api.request",
        header: res.statusCode,
        id: id,
        src: GenericHelpersCommon.getIpFromRequest(req)!,
        dest: url,
    });

    const cleanup = (): void => {
        res.removeListener("finish", logFn);
        res.removeListener("close", abortFn);
        res.removeListener("error", errorFn);
    };

    const logFn = (): void => {
        cleanup();
        // Record response start.
        const resStartAt = process.hrtime();

        const resLogger = GenericHelpers.getLoggerByStatusCode(res.statusCode);
        const resTime = getResponseTime(reqStartAt, resStartAt);
        msg = [req.method, url, res.statusCode, getContentLength(res), "-", resTime, "ms"].join(" ");
        resLogger({
            message: msg,
            type: "api.response",
            header: res.statusCode,
            id: id,
            src: url,
            dest: GenericHelpersCommon.getIpFromRequest(req)!,
            response_time: resTime ? parseInt(resTime) : undefined,
        });
    };

    const abortFn = (): void => {
        cleanup();
        Logger.warn({ message: "Request aborted by the client.", type: "api", id: id, dest: url, header: 500 });
    };

    const errorFn = (err) => {
        cleanup();
        Logger.error({ message: `Request pipeline error: ${err}`, type: "api", id: id, dest: url, header: 500 });
    };

    // Successful pipeline (regardless of its response).
    res.on("finish", logFn);

    // Aborted pipeline.
    res.on("close", abortFn);

    // Pipeline internal error.
    res.on("error", errorFn);

    next();
}

/**
 * Get url (destination)
 */
function getUrl(req: Request): string {
    return req.originalUrl || req.url;
}

/**
 * Get response time in ms.
 */
function getResponseTime(reqStartAt?: [number, number], resStartAt?: [number, number], digits: number = 3): undefined | string {
    if (reqStartAt == null || resStartAt == null) {
        // Missing request and/or response start time.
        return undefined;
    }

    // Calculate difference.
    const ms = (resStartAt[0] - reqStartAt[0]) * 1e3 + (resStartAt[1] - reqStartAt[1]) * 1e-6;

    // Return truncated.
    return ms.toFixed(digits);
}

/**
 * Get content length.
 */
function getContentLength(res: Response): string | number | undefined {
    const field = "content-length";
    const header = res.getHeader(field);

    return Array.isArray(header) ? header.join(", ") : header;
}
