/**
 * @pattern ^((\d+)(,\s*\d+)*)$
 */
export type FilterParamV1 = string;

/**
 * @isInt value must be integer
 * @example 103
 */
export type IdNumber = number;
