import { Stan } from "node-nats-streaming";
import Container, { Service } from "typedi";
import { NatsAdapter } from "../../adapters/nats-adapter";
import { Logger } from "../../config/logger";
import { Event } from "../types";
import { Event as CommonEvent } from "syntropy-common-ts";
@Service()
export abstract class Publisher<
    // Extending both local Event and CommontEvent from common-ts
    TEvent extends Event | CommonEvent
> {
    public abstract subject: TEvent["subject"];
    constructor(protected client: Stan = Container.get(NatsAdapter).client) {
        this.client = client;
    }

    public encode(data: TEvent["data"]): string | Uint8Array {
        return JSON.stringify(data);
    }

    public async publish(data: TEvent["data"]): Promise<void> {
        return new Promise((resolve, reject) => {
            this.client.publish(this.subject, this.encode(data), (err) => {
                if (err) {
                    Logger.error({
                        message: `${this.subject}`,
                        type: "event-published",
                        header: 500,
                        subject: this.subject,
                        data,
                    });
                    return reject(err);
                }
                Logger.info({
                    message: `${this.subject}`,
                    type: "event-published",
                    header: 200,
                    subject: this.subject,
                    data,
                });
                resolve();
            });
        });
    }
}
