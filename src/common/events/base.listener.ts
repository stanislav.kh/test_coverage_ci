import { Message, Stan, StartPosition, Subscription, SubscriptionOptions } from "node-nats-streaming";
import Container, { Service } from "typedi";
import * as apm from "elastic-apm-node";

import { NatsAdapter } from "../../adapters/nats-adapter";
import { getEnv } from "../../config/env";
import { Logger } from "../../config/logger";
import { Event } from "../types";

@Service()
export abstract class Listener<TEvent extends Event> {
    public abstract subject: TEvent["subject"];
    public abstract queueGroupName: string | undefined;
    public abstract onMessage(data: TEvent["data"], msg: Message): Promise<void>;
    protected ackWait: number = parseInt(getEnv().NATS_ACK_WAIT!) * 1_000;

    constructor(protected client: Stan = Container.get(NatsAdapter).client) {}

    public subscriptionOptions(incomingOptions?: Partial<SubscriptionOptions>): SubscriptionOptions {
        const defaultOptions = this.client.subscriptionOptions();
        defaultOptions.maxInFlight = parseInt(getEnv().NATS_MAX_IN_FLIGHT!);
        if (incomingOptions?.manualAcks) {
            defaultOptions.setAckWait(this.ackWait);
        }

        if (!incomingOptions?.durableName) {
            defaultOptions.setStartAt(StartPosition.NEW_ONLY);
        } else {
            defaultOptions.setDeliverAllAvailable();
        }

        return {
            ...defaultOptions,
            ...incomingOptions,
        };
    }

    public listen(options?: Partial<SubscriptionOptions>): void {
        let subscription: Subscription;
        if (this.queueGroupName) {
            subscription = this.client.subscribe(this.subject, this.queueGroupName, this.subscriptionOptions(options));
        } else {
            subscription = this.client.subscribe(this.subject, this.subscriptionOptions(options));
        }

        subscription.on("message", async (msg) => {
            const parsedData = this.parseMessage(msg);
            Logger.info({
                message: `${msg.getSubject()} sequence=${msg.getSequence()} isRedelivered=${msg.isRedelivered()} redeliveryCount=${
                    msg.isRedelivered() ? msg.msg.array[6] : 0
                }`,
                type: "event-received",
                header: 200,
                subject: this.subject,
                sequence: msg.getSequence(),
                data: parsedData,
            });

            if (msg.isRedelivered() && msg.msg.array[6] >= parseInt(getEnv().NATS_MAX_REDELIVERY_COUNT!)) {
                Logger.info({
                    message: `${msg.getSubject()} sequence=${msg.getSequence()} isRedelivered=${msg.isRedelivered()} redeliveryCount=${
                        msg.msg.array[6]
                    }`,
                    type: "event-acknowledged",
                    header: 200,
                    subject: this.subject,
                    sequence: msg.getSequence(),
                    data: parsedData,
                });
                msg.ack();
            } else {
                const txn = apm.startTransaction(this.subject, "event");
                try {
                    await this.onMessage(parsedData, msg);
                    txn?.end();
                } catch (e: any) {
                    txn?.end("error");
                    Logger.error(e);
                }
            }
        });
    }

    public parseMessage(msg: Message): string {
        const data = msg.getData();
        return typeof data === "string" ? JSON.parse(data) : JSON.parse(data.toString("utf8"));
    }
}
