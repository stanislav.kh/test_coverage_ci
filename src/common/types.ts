import * as express from "express";
import { AuthData, Subjects as AuthSubjects } from "../components/auth/auth.contracts";

export interface AuthorizedRequest extends express.Request {
    authData: AuthData;
    token: string;
}

export interface Event {
    subject: AuthSubjects;
    data: any;
}

export enum LogLevel {
    None = 0,
    Error = 1,
    Warn = 2,
    Info = 4,
    Verbose = 8,
    Profiling = 16,
    Default = 4 + 2 + 1,
    All = 8 + 4 + 2 + 1,
}
