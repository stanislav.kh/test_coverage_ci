import { getConnection, Connection, getRepository } from "typeorm";
import { ColumnMetadata } from "typeorm/metadata/ColumnMetadata";
import * as faker from "faker";
import { Container } from "typedi";
import { useContainer } from "typeorm";
import { Clazz, GenericHelpers, Model } from "syntropy-common-ts";
import { Database } from "../config/database";

export namespace TestHelpers {
    export async function describeCrud<TModel extends Model<any>>(ModelClass: Clazz<TModel>): Promise<void> {
        describe.skip(ModelClass.name, () => {
            let connection: Connection;

            // IMPORTANT!: Create "noia_controlled_dev_tests" and rerun migrations before test
            beforeAll(async () => {
                useContainer(Container);
                connection = await Database.createConnection(Database.getTestsConnectionOptions());
                // await getRepository(ModelClass).createQueryBuilder().delete().from(ModelClass).execute();
            }, 10_000);

            afterAll(async () => {
                await connection.close();
            });

            it("performs CRUD operations", async () => {
                const repo = getRepository(ModelClass);
                const { columns, relationsWithJoinColumns } = getConnection().getMetadata(ModelClass);
                const fooRecord = await createRecord(getConnection(), ModelClass);
                const barRecord = await createRecord(getConnection(), ModelClass, false, false, false);

                const primaryColumn = columns.find((column) => column.isPrimary)!;
                const primaryColumnName = primaryColumn.propertyName;
                // Clean up
                await deleteAllRecords(connection, ModelClass);

                // Create one.
                try {
                    await GenericHelpers.validateConstraints(ModelClass, fooRecord);
                } catch (e: any) {
                    console.error(e);
                    expect(e).toBeUndefined();
                }
                const dbRecord = await repo.save(fooRecord);
                checkProperties(columns, dbRecord, fooRecord);

                // Get all.
                const allRecords = await repo.find({ relations: relationsWithJoinColumns.map((r) => r.propertyName) });
                expect(Array.isArray(allRecords)).toBe(true);
                const lastItem: any = allRecords.pop();
                checkProperties(columns, lastItem, fooRecord);

                // Read one.
                const lastItemFromDb = await repo.findOne({ [primaryColumnName]: lastItem?.[primaryColumnName] });
                checkProperties(columns, lastItemFromDb, fooRecord);

                const updatedRecordWithoutNulls: any = {};
                Object.getOwnPropertyNames(barRecord).map((k) => {
                    updatedRecordWithoutNulls[k] = barRecord[k] ?? undefined;
                });

                // Update one.
                await repo.update(lastItem?.[primaryColumnName], updatedRecordWithoutNulls);

                // Read one updated.
                const updatedItemFromDb = await repo.findOne({ [primaryColumnName]: lastItem?.[primaryColumnName] });
                expect(updatedItemFromDb).toEqual(expect.objectContaining(updatedRecordWithoutNulls));

                // Delete one.
                await repo.delete(lastItem?.[primaryColumnName]);

                const deletedItem = await repo.findOne({ [primaryColumnName]: lastItem?.[primaryColumnName] });
                expect(deletedItem).toBeFalsy();
            });
        });
    }

    export async function deleteAllRecords<TModel extends Model<any>>(connection: Connection, ModelClass: Clazz<TModel>): Promise<void> {
        const repo = getRepository(ModelClass);
        const metadata = connection.getMetadata(ModelClass);
        const primaryColumn = metadata.columns.find((column) => column.isPrimary)!;

        try {
            await repo
                .createQueryBuilder()
                .delete()
                .from(ModelClass)
                .where(`${primaryColumn.propertyName}=${primaryColumn.propertyName}`)
                .execute();
        } catch (e: any) {
            // Most likely violates foreign key constraint
            // Delete relations first
            for (const column of metadata.columns) {
                if (column.referencedColumn) {
                    const relationModel = column.referencedColumn.target as Clazz<any>;
                    await deleteAllRecords(connection, relationModel);
                }
            }
        }
        // Try deleting once more
        await repo
            .createQueryBuilder()
            .delete()
            .from(ModelClass)
            .where(`${primaryColumn.propertyName}=${primaryColumn.propertyName}`)
            .execute();
    }

    export async function createRecord<TModel extends Model<any>>(
        connection: Connection,
        ModelClass: Clazz<TModel>,
        /** If true, writes record into database. */
        saveRecord?: boolean,
        setTimestamps: boolean = true,
        setRelations: boolean = true
    ): Promise<TModel> {
        const metadata = connection.getMetadata(ModelClass);
        const record = new ModelClass();
        for (const column of metadata.columns) {
            if (column.isGenerated) {
                continue;
            }

            if (column.isNullable) {
                continue;
            } else if (!column.referencedColumn) {
                const randInt = Math.round(Math.random() * 1e6);
                switch (column.type) {
                    case Number:
                        record[column.propertyName] = randInt;
                        break;
                    case "real":
                        record[column.propertyName] = parseFloat((Math.random() * 100).toPrecision(7));
                        break;
                    case String:
                    case "text":
                    case "varchar":
                        if (column.propertyName.endsWith("_url")) {
                            record[column.propertyName] = `http://example.com/${randInt}`;
                        } else if (column.propertyName.endsWith("_email")) {
                            record[column.propertyName] = `test${Math.round(Math.random() * 1e6)}@test.lt`;
                        } else {
                            record[column.propertyName] = `${randInt}-${column.propertyName}`.slice(0, parseInt(column.length) || 12);
                        }
                        break;
                    // IPv4 and IPv6 hosts and networks.
                    case "inet":
                        record[column.propertyName] = faker.internet.ip();
                        break;
                    case "cidr":
                        record[column.propertyName] = `${faker.internet.ip()}/32`;
                        break;
                    case Boolean:
                        record[column.propertyName] = false;
                        break;
                    case "enum":
                        record[column.propertyName] = column.enum![0];
                        break;
                    case "json":
                    case "jsonb":
                        record[column.propertyName] = JSON.stringify({});
                        break;
                    case "timestamptz":
                    case Date:
                    case "timestamp":
                        if (column.isCreateDate || !setTimestamps) {
                            continue;
                        }
                        record[column.propertyName] = new Date();
                        break;
                    default:
                        throw new Error(`Column '${column.propertyName}' type '${column.type}' mocking not implemented.`);
                }
            } else {
                if (column.relationMetadata?.propertyName && setRelations) {
                    const relationRecord = await createRecord(connection, column.referencedColumn.target as Clazz<any>, true);

                    record[column.relationMetadata?.propertyName] = relationRecord;
                    // Not sure if this is correct, needs more different models to test
                    record[column.propertyName] = relationRecord[column.referencedColumn.propertyName];
                }
            }
        }
        if (saveRecord) {
            const repo = getRepository(ModelClass);
            const savedRecord = await repo.save(record);
            return savedRecord;
        }

        return record;
    }

    export function checkProperties(columns: ColumnMetadata[], expectedObject: any, toBeObject: any): void {
        for (const column of columns) {
            if (column.isGenerated) {
                continue;
            }
            if (column.isCreateDate) {
                expect(toBeObject[column.propertyName]).toBeDefined();
                expect(expectedObject[column.propertyName]).toBeDefined();
                continue;
            }
            if (column.type === "timestamptz") {
                continue;
            }
            if (column.isUpdateDate) {
                // TODO: investigate.
                // expect(toBeObject[column.propertyName]).toBeUndefined();
                // expect(expectedObject[column.propertyName]).toBeDefined();
                continue;
            }
            if (column.isNullable) {
                continue;
            }

            // HACK: 'undefined == null', 'undefined == undefined' and 'null == null' are acceptable.
            // tslint:disable-next-line
            if (expectedObject[column.propertyName] != toBeObject[column.propertyName]) {
                console.info("prop", column.propertyName);
                expect(expectedObject[column.propertyName]).toBe(toBeObject[column.propertyName]);
            }
        }
    }
}
