import { BaseError, ErrorType, SerializedError, StatusCode } from "syntropy-common-ts";

/**
 * The server would like to shut down this connection.
 */
export class UserSelfConflictError extends BaseError {
    public readonly statusCode: StatusCode = StatusCode.ConflictError;

    constructor(user_id: number) {
        super(`User ID ${user_id} is conflicting. Cannot perform operation on self.`);

        // Only because we are extending a built in class
        Object.setPrototypeOf(this, UserSelfConflictError.prototype);
    }

    public serializeErrors(): SerializedError[] {
        return [
            {
                code: ErrorType.ConflictError,
                message: this.message,
                type: ErrorType.ConflictError,
            },
        ];
    }
}
