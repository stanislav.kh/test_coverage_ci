import { BaseError, ErrorType, SerializedError, StatusCode } from "syntropy-common-ts";

/**
 * The server would like to shut down this connection.
 */
export class InvitationServerError extends BaseError {
    public readonly statusCode: StatusCode = StatusCode.ServerError;

    constructor(invitation_id: number, reason?: string) {
        super(`Invitation ${invitation_id} cannot be processed. ${reason}`);

        // Only because we are extending a built in class
        Object.setPrototypeOf(this, InvitationServerError.prototype);
    }

    public serializeErrors(): SerializedError[] {
        return [
            {
                code: ErrorType.ServerError,
                message: this.message,
                type: ErrorType.ServerError,
            },
        ];
    }
}
