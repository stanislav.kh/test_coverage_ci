import { BaseError, ErrorType, SerializedError, StatusCode } from "syntropy-common-ts";

/**
 * The server would like to shut down this connection.
 */
export class WorkspaceNotFoundError extends BaseError {
    public readonly statusCode: StatusCode = StatusCode.NotFound;

    constructor(workspace_id: number) {
        super(`Workspace ${workspace_id} not found.`);

        // Only because we are extending a built in class
        Object.setPrototypeOf(this, WorkspaceNotFoundError.prototype);
    }

    public serializeErrors(): SerializedError[] {
        return [
            {
                code: ErrorType.NotFoundError,
                message: this.message,
                type: ErrorType.NotFoundError,
            },
        ];
    }
}
