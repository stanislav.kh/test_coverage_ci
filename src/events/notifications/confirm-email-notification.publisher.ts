import { Service } from "typedi";
import { Subjects, ConfirmEmailNotificationEvent } from "syntropy-common-ts";
import { Publisher } from "../../common";
import { NotificationMessage } from "syntropy-protobuf";

@Service()
export class ConfirmEmailNotificationPublisher extends Publisher<ConfirmEmailNotificationEvent> {
    public encode(data: ConfirmEmailNotificationEvent["data"]): Uint8Array {
        return NotificationMessage.encode(data).finish();
    }
    public subject: Subjects.ConfirmEmailNotification = Subjects.ConfirmEmailNotification;
}
