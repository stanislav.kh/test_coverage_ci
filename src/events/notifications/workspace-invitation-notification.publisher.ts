import { Service } from "typedi";
import { Subjects, WorkspaceInviteEmailNotificationEvent } from "syntropy-common-ts";
import { Publisher } from "../../common";
import { NotificationMessage } from "syntropy-protobuf";

@Service()
export class WorkspaceInvitationNotificationPublisher extends Publisher<WorkspaceInviteEmailNotificationEvent> {
    public encode(data: WorkspaceInviteEmailNotificationEvent["data"]): Uint8Array {
        return NotificationMessage.encode(data).finish();
    }
    public subject: Subjects.WorkspaceInviteEmailNotification = Subjects.WorkspaceInviteEmailNotification;
}
