export type TsoaPick<TT, TK extends keyof TT> = {
    [TP in TK]: TT[TP];
};
