import "reflect-metadata";
import { ColumnMetadata } from "typeorm/metadata/ColumnMetadata";
import { ValidationError } from "class-validator";
import { Logger } from "../config/logger";
import { orderBy } from "lodash";
import { SerializedError, ErrorType, BaseError, CustomValidationError, SortCondition } from "syntropy-common-ts";
import { ECSConsoleMessage } from "../logger/es-console-transport.contract";
export namespace GenericHelpers {
    /**
     * Temporal Syntropy controller and Syntropy BI credentials.
     */
    export function getBase64Credentials(): string {
        return Buffer.from(":").toString("base64");
        // return Buffer.from("noia:noia2018").toString("base64");
    }

    export function getStatusColumnName(columns: ColumnMetadata[]): string {
        return columns.find((value) => value.propertyName.endsWith("_status"))!.propertyName;
    }

    export function getStatusReasonColumnName(columns: ColumnMetadata[]): string | null {
        const column = columns.find((value) => value.propertyName.endsWith("_status_reason"));

        if (!column) {
            return null;
        }

        return column.propertyName;
    }

    /**
     * Checks if object has any properties.
     */
    export function isObjectEmpty(obj: any): boolean {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    }

    export function validationErrorsToConstraints(validationErrors: ValidationError[]): string[] {
        return (
            validationErrors
                .filter((e) => e.constraints != null)
                // concat
                .map((e) => Object.keys(e.constraints!))
                // flat
                .reduce((acc, val) => acc.concat(val), [])
                // pascal case to underscore
                .map((c) => pascalToUpperUnderscore(c))
        );
    }

    export function pascalToUpperUnderscore(value: string): string {
        return value
            .split(/(?=[A-Z])/)
            .join("_")
            .toUpperCase();
    }

    export function validationApiError(errors: ValidationError[]): BaseError {
        const message = errors.reduce((acc, curr) => acc.concat(curr.toString()), "");
        return new CustomValidationError("BAD_REQUEST", message);
    }

    /**
     * Mask sensitive information.
     */
    export function mask(str: string, showChars: number = 6): string {
        return str.slice(0, showChars) + "*".repeat(str.slice(showChars).length);
    }

    export async function sleep(ms: number): Promise<void> {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }

    export function includesMultiple(arr: string[], matches: string[]): boolean {
        for (const match of matches) {
            if (arr.includes(match)) {
                return true;
            }
        }

        return false;
    }

    export function pushAndReturn<TType>(arr: any[], obj: TType): TType {
        arr.push(obj);
        return obj;
    }

    /**
     * Get logging method by severity.
     */
    export function getLoggerByStatusCode(
        statusCode: number,
        logLevelName: "verbose" | "info" | "warn" | "error" = "info"
    ): (obj: ECSConsoleMessage) => void {
        if (statusCode >= 500) {
            return Logger.error.bind(Logger);
        }
        if (statusCode >= 400) {
            return Logger.warn.bind(Logger);
        }

        return Logger[logLevelName].bind(Logger);
    }

    export function getRandomPort(): number {
        return Math.round(1 + Math.random() * 59999);
    }

    export function sort<TSortObject>(data: TSortObject[], order: string, condition: SortCondition<TSortObject>): TSortObject[] {
        if (typeof condition === "function") {
            return order === "ASC" ? data.sort((a, b) => condition(a, b)) : data.sort((a, b) => -condition(a, b));
        }

        return orderBy(data, condition, order === "ASC" ? "asc" : "desc");
    }

    export function validationErrorsToSerializedErrors(validationErrors: ValidationError[]): SerializedError[] {
        const mappedError: SerializedError[] = [];

        validationErrors.forEach((error) => {
            if (!error.constraints) {
                return;
            }

            Object.keys(error.constraints).forEach((key) =>
                mappedError.push({
                    message: error.constraints![key],
                    code: pascalToUpperUnderscore(key),
                    type: ErrorType.Validation,
                    field: error.property,
                })
            );
        });

        return mappedError;
    }

    export function getCookies(cookie: string): { [key: string]: string } {
        const keyValuesPairs: { [key: string]: string } = {};
        cookie.split(" ").forEach((c) => {
            if (c.includes("=")) {
                const [key, value] = c.split("=");
                keyValuesPairs[key] = value.split(";")[0];
            } else {
                const [valNKey] = c.split(";");
                keyValuesPairs[valNKey] = valNKey;
            }
        });

        return keyValuesPairs;
    }
}
