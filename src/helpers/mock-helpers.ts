import { AuthData } from "../components/auth/auth.contracts";
import { AuthorizedRequest } from "../common/types";
import { AuthService } from "../components/auth/auth.service";

export namespace MockHelpers {
    export function mockAuthentification(authData: AuthData): void {
        AuthService.prototype.validateJwt = jest
            .fn()
            .mockImplementationOnce((request: AuthorizedRequest, _: string, __: string[] = []): void => {
                request.authData = authData;
                request.token = "123";
                return;
            });
    }
}
