import Container from "typedi";
import * as express from "express";
import * as path from "path";
import * as swaggerUi from "swagger-ui-express";
// https://github.com/nfriedly/express-rate-limit/issues/138
import * as RateLimit from "express-rate-limit";
import { PlatformAdapter } from "./adapters/platform-adapter";
import { RegisterRoutes } from "./routes";
import { logApiCall } from "./middleware/log-api-call";
import { errorHandler, GenericHelpers } from "syntropy-common-ts";
import { logError } from "./middleware/log-error";
import { getEnv } from "./config/env";

// TODO: refactor to move some parts to common, for less boilerplate when creating new microservice
export function registerPlatformAdapter(): PlatformAdapter {
    const platformAdapter = Container.get(PlatformAdapter);
    // Register controller namespace.
    platformAdapter.$app.use((req, _, next) => {
        // @ts-ignore
        req.CONTROLLER = {};
        next();
    });

    // Log requests and responses.
    platformAdapter.$app.use(logApiCall);

    platformAdapter.$app.get("/", (_: express.Request, res: express.Response) => {
        res.sendFile(path.join(__dirname + "/public/index.html"));
    });

    // Auth endpoint protection against bruteforce attack.
    platformAdapter.$app.use(
        "/",
        RateLimit({
            windowMs: 15 * 60 * 1000,
            max: 300,
            message: { errors: ["TOO_MANY_REQUESTS"] },
            // FIXME: "trust proxy"?
            keyGenerator: (req) => GenericHelpers.getIpFromRequest(req) || req.ip,
        })
    );
    RegisterRoutes(platformAdapter.$app);
    platformAdapter.$app.use("/docs", (_, res) => {
        res.redirect("/ui");
    });
    const uiSwagger = require("./swagger.json");
    if (getEnv().SERVICE_API_ADDRESS) {
        uiSwagger.servers = [{ url: getEnv().SERVICE_API_ADDRESS }];
    }
    const uiSwaggerBuff = Buffer.from(JSON.stringify(uiSwagger), "utf8");
    platformAdapter.$app.use("/auth/ui", swaggerUi.serve, swaggerUi.setup(uiSwagger));
    platformAdapter.$app.get("/auth/swagger/definitions/:property", (req: express.Request, res: express.Response) => {
        if (typeof req.params.property === "string" && uiSwagger.components.schemas.hasOwnProperty(req.params.property)) {
            const propObj = uiSwagger.components.schemas[req.params.property];
            res.send(propObj);
            return;
        }
        res.sendStatus(404);
    });
    platformAdapter.$app.get("/auth/swagger/components/schemas/:property", (req: express.Request, res: express.Response) => {
        if (typeof req.params.property === "string" && uiSwagger.components.schemas.hasOwnProperty(req.params.property)) {
            const propObj = uiSwagger.components.schemas[req.params.property];
            res.send(propObj);
            return;
        }
        res.sendStatus(404);
    });
    platformAdapter.$app.get("/auth/swagger/:property", (req: express.Request, res: express.Response) => {
        if (typeof req.params.property === "string" && uiSwagger.hasOwnProperty(req.params.property)) {
            const propObj = uiSwagger[req.params.property];
            res.send(propObj);
            return;
        }
        res.sendStatus(404);
    });
    platformAdapter.$app.get("/auth/swagger/:property/:subproperty", (req: express.Request, res: express.Response) => {
        if (typeof req.params.property === "string" && uiSwagger.hasOwnProperty(req.params.property)) {
            const propObj = uiSwagger[req.params.property];
            if (typeof req.params.subproperty === "string" && propObj.hasOwnProperty(req.params.subproperty)) {
                res.send(propObj[req.params.subproperty]);
                return;
            }
            res.send(propObj);
            return;
        }
        res.sendStatus(404);
    });
    platformAdapter.$app.get("/auth/swagger.json", (_: express.Request, res: express.Response) => {
        res.writeHead(200, {
            "Content-Type": "application/json",
        });
        res.end(uiSwaggerBuff);
    });

    platformAdapter.$app.use(logError);
    platformAdapter.$app.use(errorHandler);

    return platformAdapter;
}
