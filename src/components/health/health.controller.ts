import { Route, Get, Response, Tags, Controller, OperationId } from "tsoa";
import { getManager } from "typeorm";

@Tags("Health")
@Route("api/auth/health")
export class HealthController extends Controller {
    /**
     * Health check used for smoke tests.
     * @summary Get health
     */
    @OperationId("GetHealth")
    @Response(500, "Internal Server Error")
    @Get()
    public async health(): Promise<void> {
        (await getManager().query("SELECT 1")) && this.setStatus(200);
    }
}
