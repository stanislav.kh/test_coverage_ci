import Container, { Service } from "typedi";
import { getCustomRepository } from "typeorm";
import { getEnv } from "../../config/env";
import { InvitationServerError } from "../../errors/invitation-server-error";
import { WorkspaceInvitationNotificationPublisher } from "../../events/notifications/workspace-invitation-notification.publisher";
import { RoleRepository } from "../role/role.repository";
import { UserWorkspaceRepository } from "../users-workspaces/user-workspace.repository";
import { UserRepository } from "../users/user.repository";
import { WorkspaceRepository } from "../workspace/workspace.repository";
import { GetUserInvitationResponse, WorkspaceInvitationCreateRequest, WorkspaceInvitationResponse } from "./workspace-invitation.contract";
import { WorkspaceInvitation } from "./workspace-invitation.model";
import { WorkspaceInvitationRepository } from "./workspace-invitation.repository";

@Service()
export class WorkspaceInvitationService {
    private workspaceInvitationRepository: WorkspaceInvitationRepository = getCustomRepository(WorkspaceInvitationRepository);
    private roleRepository: RoleRepository = getCustomRepository(RoleRepository);
    private userRepository: UserRepository = getCustomRepository(UserRepository);
    private workspaceRepository: WorkspaceRepository = getCustomRepository(WorkspaceRepository);
    private userWorkspaceRepository: UserWorkspaceRepository = getCustomRepository(UserWorkspaceRepository);

    public async getAllByWorkspaceId(workspace_id: number): Promise<WorkspaceInvitationResponse[]> {
        const invitations = await this.workspaceInvitationRepository.findByWorkspaceId(workspace_id);
        return invitations.map((i) => ({
            role_name: i.role.role_name,
            user_email: i.user_email,
            user_inviter_id: i.user_inviter_id,
            workspace_invitation_created_at: i.workspace_invitation_created_at,
            workspace_invitation_id: i.workspace_invitation_id,
            workspace_invitation_declined_at: i.workspace_invitation_declined_at,
        }));
    }

    public async getAllByUserId(user_id: number): Promise<GetUserInvitationResponse[]> {
        const user = await this.userRepository.findUserById(user_id);
        if (user) {
            const invitations = await this.workspaceInvitationRepository.findByUserEmail(user.user_email);
            return invitations.map((i) => ({
                workspace_invitation_created_at: i.workspace_invitation_created_at,
                workspace_invitation_id: i.workspace_invitation_id,
                workspace_name: i.workspace.workspace_name,
            }));
        }
        return [];
    }

    public async create(
        workspace_id: number,
        user_inviter_id: number,
        data: WorkspaceInvitationCreateRequest
    ): Promise<WorkspaceInvitation> {
        const role = await this.roleRepository.findRoleByName(data.role_name);
        const invitation = this.workspaceInvitationRepository.create({ workspace_id, user_inviter_id, role_id: role.role_id, ...data });
        const user_inviter = await this.userRepository.findUserById(user_inviter_id);
        const workspace = await this.workspaceRepository.findByAdminUserAndWorkspace(user_inviter_id, workspace_id);

        if (user_inviter && workspace) {
            Container.get(WorkspaceInvitationNotificationPublisher).publish({
                recipient: data.user_email,
                variables: {
                    inviter_email: user_inviter.user_email,
                    workspace_name: workspace.workspace_name,
                    url: getEnv().PLATFORM_ADDRESS || "",
                },
            });
        }

        return invitation;
    }

    public async remove(workspace_id: number, workspace_invitation_ids: number[]): Promise<void> {
        await this.workspaceInvitationRepository.deleteByWorkspaceAndInvitationIds(workspace_id, workspace_invitation_ids);
    }

    public async accept(user_id: number, invitation_id: number): Promise<void> {
        const user = await this.userRepository.findUserById(user_id);
        const invitation = await this.workspaceInvitationRepository.findById(invitation_id);

        if (user?.user_email !== invitation.user_email) {
            throw new InvitationServerError(invitation_id, "Invalid request");
        }

        if (invitation.workspace_invitation_declined_at) {
            throw new InvitationServerError(invitation_id, "Already declined");
        }

        if (!invitation.workspace_invitation_accepted_at) {
            invitation.workspace_invitation_accepted_at = new Date();
            await this.userWorkspaceRepository.create(user_id, invitation);
        }
    }

    public async decline(user_id: number, invitation_id: number): Promise<void> {
        const user = await this.userRepository.findUserById(user_id);
        const invitation = await this.workspaceInvitationRepository.findById(invitation_id);

        if (user?.user_email !== invitation.user_email) {
            throw new InvitationServerError(invitation_id, "Invalid request");
        }

        if (invitation.workspace_invitation_accepted_at) {
            throw new InvitationServerError(invitation_id, "Already accepted");
        }

        if (!invitation.workspace_invitation_declined_at) {
            await this.workspaceInvitationRepository.setDeclinedAtById(invitation_id);
        }
    }
}
