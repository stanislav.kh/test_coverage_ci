import { EntityRepository, EntityManager, In } from "typeorm";
import { WorkspaceInvitation, WorkspaceInvitationObject } from "./workspace-invitation.model";

@EntityRepository()
export class WorkspaceInvitationRepository {
    constructor(private manager: EntityManager) {}

    public async findByWorkspaceId(workspace_id: number): Promise<WorkspaceInvitation[]> {
        return this.manager.find(WorkspaceInvitation, {
            where: { workspace_id, workspace_invitation_accepted_at: null },
            relations: ["role"],
        });
    }

    public async findByUserEmail(user_email: string): Promise<WorkspaceInvitation[]> {
        return this.manager.find(WorkspaceInvitation, {
            where: { user_email, workspace_invitation_accepted_at: null, workspace_invitation_declined_at: null },
            relations: ["workspace"],
        });
    }

    public async findById(workspace_invitation_id: number): Promise<WorkspaceInvitation> {
        return this.manager.findOneOrFail(WorkspaceInvitation, { workspace_invitation_id });
    }

    public async create(data: WorkspaceInvitationObject): Promise<WorkspaceInvitation> {
        let workspaceInvitation = await this.manager.findOne(WorkspaceInvitation, {
            workspace_id: data.workspace_id,
            user_email: data.user_email,
        });

        if (!workspaceInvitation) {
            workspaceInvitation = new WorkspaceInvitation(data);
        } else {
            workspaceInvitation.role_id = data.role_id;
            workspaceInvitation.user_inviter_id = data.user_inviter_id;
        }

        await this.manager.save(workspaceInvitation);
        return workspaceInvitation;
    }

    public async deleteByWorkspaceAndInvitationIds(workspace_id: number, workspace_invitation_ids: number[]): Promise<void> {
        await this.manager.delete(WorkspaceInvitation, { workspace_id, workspace_invitation_id: In(workspace_invitation_ids) });
    }

    public async setDeclinedAtById(workspace_invitation_id: number): Promise<void> {
        await this.manager.update(WorkspaceInvitation, { workspace_invitation_id }, { workspace_invitation_declined_at: new Date() });
    }
}
