import { WorkspaceUserRole } from "../role/role.model";
export interface WorkspaceInvitationCreateRequest {
    /**
     * @minLength 1 workspace name is too short.
     * @maxLength 255 workspace name is too long.
     */
    user_email: string;

    role_name: WorkspaceUserRole;
}

export interface WorkspaceInvitationResponse {
    workspace_invitation_id: number;
    user_email: string;
    role_name: string;
    user_inviter_id: number;
    workspace_invitation_created_at: Date;
    workspace_invitation_declined_at?: Date;
}

export interface GetUserInvitationResponse {
    workspace_invitation_id: number;
    workspace_name: string;
    workspace_invitation_created_at: Date;
}

export type WorkspaceInvitationsRemoveBody = {
    workspace_invitation_ids: number[];
};
