import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne, JoinColumn } from "typeorm";
import { IsDate, IsEmail, IsEmpty, IsInt } from "class-validator";

import { Model } from "syntropy-common-ts";
import { Role } from "../role/role.model";
import { Workspace } from "../workspace/workspace.model";

export interface WorkspaceInvitationObject {
    workspace_id: number;
    user_email: string;
    role_id: number;
    user_inviter_id: number;
    role_name?: string;
}

@Entity("workspaces_invitations")
export class WorkspaceInvitation extends Model<WorkspaceInvitationObject> implements WorkspaceInvitationObject {
    constructor(object?: WorkspaceInvitationObject) {
        super(object);
    }

    @PrimaryGeneratedColumn()
    @IsEmpty()
    public workspace_invitation_id!: number;

    @Column()
    @IsInt()
    public workspace_id!: number;

    @Column()
    @IsEmail()
    public user_email!: string;

    @Column()
    @IsInt()
    public role_id!: number;

    @Column()
    @IsInt()
    public user_inviter_id!: number;

    @CreateDateColumn({ type: "timestamptz" })
    public workspace_invitation_created_at!: Date;

    @Column()
    @IsDate()
    public workspace_invitation_accepted_at?: Date;

    @Column()
    @IsDate()
    public workspace_invitation_declined_at?: Date;

    // --------------------------------------------------
    // Relations
    // --------------------------------------------------

    @ManyToOne(() => Role, (r) => r.workspace_invitations)
    @JoinColumn({ referencedColumnName: "role_id", name: "role_id" })
    public role!: Role;

    @ManyToOne(() => Workspace, (w) => w.workspace_invitations)
    @JoinColumn({ referencedColumnName: "workspace_id", name: "workspace_id" })
    public workspace!: Workspace;
}
