import { BackupCode, BackupCodeObject } from "./backup-code.model";

export class BackupCodeRepositoryMock {
    public async saveMany(_: BackupCodeObject[]): Promise<void> {
        throw new Error("Not implemented");
    }

    public async deleteManyByUserId(_: number): Promise<void> {
        throw new Error("Not implemented");
    }

    public async findAllUnusedByUserId(_: number): Promise<BackupCode[]> {
        throw new Error("Not implemented");
    }

    public async setCodeAsUsed(_: number): Promise<void> {
        throw new Error("Not implemented");
    }
}
