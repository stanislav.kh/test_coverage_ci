import { EntityRepository, EntityManager } from "typeorm";
import { BackupCode, BackupCodeObject } from "./backup-code.model";

@EntityRepository()
export class BackupCodeRepository {
    constructor(private manager: EntityManager) {}

    public async saveMany(codes: BackupCodeObject[]): Promise<void> {
        await this.manager.createQueryBuilder().insert().into(BackupCode).values(codes).execute();
    }

    public async deleteManyByUserId(userId: number): Promise<void> {
        await this.manager.delete(BackupCode, { user_id: userId });
    }

    public async findAllUnusedByUserId(userId: number): Promise<BackupCode[]> {
        return this.manager.find(BackupCode, {
            where: {
                user_id: userId,
                backup_code_used: false,
            },
        });
    }

    public async setCodeAsUsed(id: number): Promise<void> {
        await this.manager
            .createQueryBuilder()
            .update(BackupCode, { backup_code_used: true })
            .where("backup_codes.backup_code_id = :id", { id })
            .execute();
    }
}
