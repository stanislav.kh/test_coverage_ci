/**
 * TODO: This model is confusing since there are many endpoints connected to it and with
 * different scopes (ADMIN, USER). I think there should be two clases in the end AdminUser and User both sharing
 * same database table, but with different validation and default expected output objectss.
 */

import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { IsString, IsBoolean, IsOptional, IsNumber } from "class-validator";

import { Model } from "syntropy-common-ts";
import { User } from "../users/user.model";

export interface BackupCodeObject {
    user_id: number;
    backup_code_value: string;
}

@Entity("backup_codes")
export class BackupCode extends Model<BackupCodeObject> {
    constructor(object?: BackupCodeObject) {
        super(object);
    }

    @PrimaryGeneratedColumn()
    @IsNumber()
    @IsOptional()
    public backup_code_id!: number;

    @Column()
    @IsNumber()
    public user_id!: number;

    @Column()
    @IsString()
    public backup_code_value!: string;

    @Column()
    @IsBoolean()
    public backup_code_used!: boolean;

    @OneToOne((_) => User)
    @JoinColumn({ referencedColumnName: "user_id", name: "user_id" })
    public user?: User;
}
