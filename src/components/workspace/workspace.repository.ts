import { EntityRepository, EntityManager } from "typeorm";
import { Role, RoleName } from "../role/role.model";
import { UserWorkspace } from "../users-workspaces/user-workspace.model";
import { Workspace } from "./workspace.model";
import { WorkspaceUpdateRequest } from "./workspace.contracts";

@EntityRepository()
export class WorkspaceRepository {
    constructor(public manager: EntityManager) {}

    public async findByUserId(
        user_id: number
    ): Promise<Required<Array<Pick<Workspace, "workspace_id" | "workspace_name" | "workspace_created_at">>>> {
        if (!user_id) {
            return [];
        }

        return (await this.manager
            .createQueryBuilder(Workspace, "workspace")
            .innerJoin("workspace.user_workspaces", "user_workspaces")
            .select(["workspace.workspace_id", "workspace.workspace_name", "workspace.workspace_created_at"])
            .where("user_workspaces.user_id = :user_id", {
                user_id,
            })
            .getMany()) as Array<Pick<Workspace, "workspace_id" | "workspace_name" | "workspace_created_at">>;
    }

    public async findByAdminUserAndWorkspace(
        user_id: number,
        workspace_id: number
    ): Promise<Pick<Workspace, "workspace_id" | "workspace_name" | "workspace_created_at"> | undefined> {
        if (!user_id || !workspace_id) {
            return undefined;
        }

        return (await this.manager
            .createQueryBuilder(Workspace, "workspace")
            .innerJoin("workspace.user_workspaces", "user_workspaces")
            .innerJoin("user_workspaces.role", "roles")
            .select(["workspace.workspace_id", "workspace.workspace_name", "workspace.workspace_created_at"])
            .where(
                "user_workspaces.user_id = :user_id AND user_workspaces.workspace_id = :workspace_id AND roles.role_name = :admin_role",
                {
                    user_id,
                    workspace_id,
                    admin_role: RoleName.Admin,
                }
            )
            .getOne()) as Pick<Workspace, "workspace_id" | "workspace_name" | "workspace_created_at"> | undefined;
    }

    public async create(user_id: number, workspace_name: string): Promise<Workspace> {
        const workspace = new Workspace({ workspace_name });
        const adminRole = await this.manager.findOneOrFail(Role, { where: { role_name: RoleName.Admin } });

        await this.manager.transaction(async (txManager) => {
            await txManager.save(workspace);
            const userWorkspace = new UserWorkspace();
            userWorkspace.role = adminRole;
            userWorkspace.user_id = user_id;
            userWorkspace.workspace = workspace;
            await txManager.save(userWorkspace);
        });

        return workspace;
    }

    public async remove(workspace: Workspace): Promise<void> {
        await this.manager.remove(Workspace, workspace);
    }

    public async update(workspace_id: number, data: WorkspaceUpdateRequest): Promise<void> {
        await this.manager.update(Workspace, { workspace_id }, data);
    }
}
