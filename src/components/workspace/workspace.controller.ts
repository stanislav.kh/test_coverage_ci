import {
    Body,
    Controller,
    Delete,
    Deprecated,
    Get,
    OperationId,
    Post,
    Request,
    Response,
    Route,
    Security,
    SuccessResponse,
    Tags,
} from "tsoa";
import Container from "typedi";

import { WorkspaceCreateRequest, WorkspaceDto } from "./workspace.contracts";
import { AuthorizedRequest } from "../../common/types";
import { WorkspaceService } from "./workspace.service";
import { IdNumber } from "../../common/contract";

interface ApiResponse<TData> {
    data?: TData;
}

@Tags("Workspaces")
@Route("auth/workspaces")
export class WorkspaceController extends Controller {
    private workspaceService: WorkspaceService = Container.get(WorkspaceService);

    /**
     * Create workspace.
     * @summary Create workspace
     */
    @OperationId("CreateWorkspace")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Post()
    public async createWorkspace(
        @Body() body: WorkspaceCreateRequest,
        @Request() request: AuthorizedRequest
    ): Promise<ApiResponse<WorkspaceDto>> {
        return { data: await this.workspaceService.create(request.authData.user_id, body) };
    }

    /**
     * Get workspaces.
     * @summary Get workspaces
     */
    @OperationId("GetWorkspaces")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Get()
    public async indexWorkspace(@Request() request: AuthorizedRequest): Promise<ApiResponse<WorkspaceDto[]>> {
        const workspaces = await this.workspaceService.getAllByUserId(request.authData.user_id);

        return { data: workspaces };
    }

    /**
     * Delete workspace.
     * @summary Delete workspace
     */
    @OperationId("DeleteWorkspace")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN"])
    @Response(404, "Not Found")
    @SuccessResponse(204, "No Content")
    @Delete("{workspace_id}")
    public async deleteWorkspace(workspace_id: IdNumber, @Request() request: AuthorizedRequest): Promise<void> {
        await this.workspaceService.remove(workspace_id, request.authData.user_id);
    }
}
