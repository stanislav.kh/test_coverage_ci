import { Route, Controller, OperationId, Tags, Security, SuccessResponse, Post, Body, Get, Delete, Request, Patch } from "tsoa";
import { ApiResponse, ForbiddenError } from "syntropy-common-ts";
import Container from "typedi";
import { IdNumber } from "../../common/contract";
import { AuthorizedRequest } from "../../common/types";
import {
    WorkspaceInvitationCreateRequest,
    WorkspaceInvitationResponse,
    WorkspaceInvitationsRemoveBody,
} from "../workspaces-invitations/workspace-invitation.contract";
import { WorkspaceInvitation } from "../workspaces-invitations/workspace-invitation.model";
import { WorkspaceInvitationService } from "../workspaces-invitations/workspace-invitation.service";
import { WorkspaceCreateRequest, WorkspaceUpdateRequest, WorkspaceDto } from "./workspace.contracts";
import { WorkspaceService } from "./workspace.service";

@Tags("Workspaces")
@Route("api/v1/network/auth/workspaces")
export class WorkspaceControllerV1 extends Controller {
    private workspaceService: WorkspaceService = Container.get(WorkspaceService);
    private workspaceInvitationService: WorkspaceInvitationService = Container.get(WorkspaceInvitationService);

    /**
     * Creates workspace.
     * @summary Create workspace
     */
    @OperationId("CreateWorkspaceV1")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @SuccessResponse(200, "OK")
    @Post()
    public async CreateWorkspace(
        @Body() body: WorkspaceCreateRequest,
        @Request() request: AuthorizedRequest
    ): Promise<ApiResponse<WorkspaceDto>> {
        return { data: await this.workspaceService.create(request.authData.user_id, body) };
    }

    /**
     * Gets workspaces.
     * @summary Get workspaces
     */
    @OperationId("GetWorkspacesV1")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @SuccessResponse(200, "OK")
    @Get()
    public async GetWorkspaces(@Request() request: AuthorizedRequest): Promise<ApiResponse<WorkspaceDto[]>> {
        const workspaces = await this.workspaceService.getAllByUserId(request.authData.user_id);

        return { data: workspaces };
    }

    /**
     * Deletes workspace.
     * @summary Delete workspace
     */
    @OperationId("DeleteWorkspaceV1")
    @SuccessResponse(204, "No Content")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN"])
    @Delete("{workspace_id}")
    public async DeleteWorkspace(workspace_id: IdNumber, @Request() request: AuthorizedRequest): Promise<void> {
        await this.workspaceService.remove(workspace_id, request.authData.user_id);
    }

    /**
     * Updates workspace.
     * @summary Update workspace
     */
     @OperationId("UpdateWorkspaceV1")
     @Security("jwt", ["GLOBAL_ADMIN", "ADMIN"])
     @SuccessResponse(204, "No Content")
     @Patch("{workspace_id}")
     public async UpdateWorkspace(
         workspace_id: IdNumber,
         @Body() body: WorkspaceUpdateRequest,
         @Request() request: AuthorizedRequest
     ): Promise<void> {
         await this.workspaceService.update(workspace_id, request.authData.user_id, body);
     }

    /**
     * Gets workspace invitations.
     * @summary Get workspace invitations
     */
    @OperationId("GetWorkspaceInvitationsV1")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @SuccessResponse(200, "OK")
    @Get("invitations")
    public async GetWorkspaceInvitations(@Request() request: AuthorizedRequest): Promise<ApiResponse<WorkspaceInvitationResponse[]>> {
        if (!request.authData.workspace_id) {
            throw new ForbiddenError();
        }

        const invitations = await this.workspaceInvitationService.getAllByWorkspaceId(request.authData.workspace_id);

        return { data: invitations };
    }

    /**
     * Creates workspace invitation.
     * @summary Create workspace invitation
     */
    @OperationId("CreateWorkspaceInvitationV1")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN"])
    @SuccessResponse(200, "OK")
    @Post("invitations")
    public async CreateWorkspaceInvitation(
        @Body() body: WorkspaceInvitationCreateRequest,
        @Request() request: AuthorizedRequest
    ): Promise<ApiResponse<WorkspaceInvitation>> {
        if (!request.authData.workspace_id) {
            throw new ForbiddenError();
        }
        return { data: await this.workspaceInvitationService.create(request.authData.workspace_id, request.authData.user_id, body) };
    }

    /**
     * Deletes workspace invitations.
     * @summary Delete workspace invitations
     */
    @OperationId("DeleteWorkspaceInvitationsV1")
    @SuccessResponse(204, "No Content")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN"])
    @Post("invitations/remove")
    public async DeleteWorkspaceInvitations(
        @Body() body: WorkspaceInvitationsRemoveBody,
        @Request() request: AuthorizedRequest
    ): Promise<void> {
        if (!request.authData.workspace_id) {
            throw new ForbiddenError();
        }
        await this.workspaceInvitationService.remove(request.authData.workspace_id, body.workspace_invitation_ids);
    }

    /**
     * Accepts workspace invitation.
     * @summary Accept workspace invitation
     */
    @OperationId("AcceptWorkspaceInvitationV1")
    @SuccessResponse(204, "No Content")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Patch("invitations/{invitation_id}/accept")
    public async AcceptWorkspaceInvitation(invitation_id: IdNumber, @Request() request: AuthorizedRequest): Promise<void> {
        await this.workspaceInvitationService.accept(request.authData.user_id, invitation_id);
    }

    /**
     * Declines workspace invitation.
     * @summary Decline workspace invitation
     */
    @OperationId("DeclineWorkspaceInvitationV1")
    @SuccessResponse(204, "No Content")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Patch("invitations/{invitation_id}/decline")
    public async DeclineWorkspaceInvitation(invitation_id: IdNumber, @Request() request: AuthorizedRequest): Promise<void> {
        await this.workspaceInvitationService.decline(request.authData.user_id, invitation_id);
    }

}
