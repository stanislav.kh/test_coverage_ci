import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, OneToMany } from "typeorm";
import { IsString, IsEmpty } from "class-validator";
import { Model } from "syntropy-common-ts";

import { UserWorkspace } from "../users-workspaces/user-workspace.model";
import { User } from "../users/user.model";
import { WorkspaceInvitation } from "../workspaces-invitations/workspace-invitation.model";

export interface WorkspaceObject {
    workspace_name: string;
}

export const DEFAULT_WORKSPACE_NAME = "Default";

@Entity("workspaces")
export class Workspace extends Model<WorkspaceObject> implements WorkspaceObject {
    constructor(object?: WorkspaceObject) {
        super(object);
    }

    @PrimaryGeneratedColumn()
    @IsEmpty()
    public workspace_id!: number;

    @Column({ type: "text" })
    @IsString()
    public workspace_name!: string;

    @CreateDateColumn({ type: "timestamptz" })
    public workspace_created_at!: Date;

    // --------------------------------------------------
    // Relations
    // --------------------------------------------------

    @OneToMany(() => UserWorkspace, (uw) => uw.workspace)
    public user_workspaces!: UserWorkspace[];

    @OneToMany(() => User, (u) => u.default_workspace)
    public default_workspace_users!: User[];

    @OneToMany(() => WorkspaceInvitation, (wi) => wi.workspace)
    public workspace_invitations!: WorkspaceInvitation[];
}
