export interface WorkspaceCommonRequest {
    /**
     * @minLength 1 workspace name is too short.
     * @maxLength 255 workspace name is too long.
     */
    workspace_name: string;
}

export type WorkspaceCreateRequest = WorkspaceCommonRequest;

export type WorkspaceUpdateRequest = WorkspaceCommonRequest;

export interface WorkspaceDto extends WorkspaceCommonRequest {
    workspace_id: number;
    workspace_created_at: Date;
}
