import { ForbiddenError } from "syntropy-common-ts";
import { Service } from "typedi";
import { getCustomRepository } from "typeorm";

import { WorkspaceCreateRequest, WorkspaceUpdateRequest, WorkspaceDto } from "./workspace.contracts";
import { Workspace } from "./workspace.model";
import { WorkspaceRepository } from "./workspace.repository";

@Service()
export class WorkspaceService {
    private workspaceRepository: WorkspaceRepository = getCustomRepository(WorkspaceRepository);

    public async getAllByUserId(user_id: number): Promise<WorkspaceDto[]> {
        const workspaces = await this.workspaceRepository.findByUserId(user_id);
        return workspaces;
    }

    public async create(user_id: number, data: WorkspaceCreateRequest): Promise<Workspace> {
        return this.workspaceRepository.create(user_id, data.workspace_name);
    }

    public async remove(workspace_id: number, user_id: number): Promise<void> {
        const workspace = (await this.workspaceRepository.findByAdminUserAndWorkspace(user_id, workspace_id)) as Workspace;

        if (!workspace) {
            throw new ForbiddenError();
        }

        await this.workspaceRepository.remove(workspace);
    }

    public async update(workspace_id: number, user_id: number, data: WorkspaceUpdateRequest): Promise<void> {
        const workspace = (await this.workspaceRepository.findByAdminUserAndWorkspace(user_id, workspace_id)) as Workspace;

        if (!workspace) {
            throw new ForbiddenError();
        }

        await this.workspaceRepository.update(workspace_id, data);
    }
}
