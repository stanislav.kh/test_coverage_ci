import { BackupCodeRepositoryMock } from "./../backup-codes/backup-code.repository.mock";
import { RedisService } from "./../redis/redis.service";
import { getRepoMockImplementation, RedisServiceMock, UserRepositoryMock } from "./../../tests/mocks";
import { AzureService } from "./../azure-auth/azure-auth.service";
import { ObjectLiteral } from "typeorm";
import * as request from "supertest";
import { registerPlatformAdapter } from "../../register-platform-adapter";
import Container from "typedi";
import { MockHelpers } from "../../helpers/mock-helpers";
import { authenticator } from "otplib";
import { mocked } from "ts-jest/utils";
import { MfaCodeType } from "./mfa.contract";
import * as bcrypt from "bcrypt";

jest.mock("otplib", () => ({
    authenticator: {
        verify: jest.fn().mockReturnValue(true),
        generateSecret: jest.fn().mockReturnValue("AAAA"),
        keyuri: jest.fn().mockReturnValue("AAAA"),
    },
}));

jest.mock("typeorm", () => ({
    getCustomRepository: jest.fn().mockImplementation(getRepoMockImplementation),
}));
const app = registerPlatformAdapter().$app;

describe("MFA", () => {
    const headers: ObjectLiteral = { Accept: "application/json" };
    const authData = {
        user_email: "user_email",
        user_id: 1,
        user_scopes: ["user_scopes"],
        user_settings: {},
        azure_user_id: "1",
        workspace_id: 1,
    };

    beforeAll(async () => {
        Container.set(RedisService, new RedisServiceMock());
        RedisServiceMock.prototype.deleteMatchingKeys = jest.fn().mockResolvedValueOnce(null);
        process.env.SECRET = "SECRET";
        process.env.AZURE_API_KEY = "AZURE_API_KEY";
    });

    describe("generate code", () => {
        it("should succeed when mfa for user is disabled", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(false);

            const retrieveResponse = await request(app).get("/auth/mfa/generate").set(headers);
            expect(retrieveResponse.status).toBe(200);
            expect(retrieveResponse.body.authCode).toBeDefined();
            expect(retrieveResponse.body.secret).toBeDefined();
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
        });

        it("should fail when mfa for user is already enabled", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(true);

            const retrieveResponse = await request(app).get("/auth/mfa/generate").set(headers);
            expect(retrieveResponse.status).toBe(400);
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
        });
    });

    describe("confirm code", () => {
        it("should succeed", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(false);
            AzureService.prototype.addMFASecretForUser = jest.fn().mockResolvedValueOnce({});
            UserRepositoryMock.prototype.findUserById = jest.fn().mockResolvedValueOnce({
                test: "test",
            });
            UserRepositoryMock.prototype.updateSettings = jest.fn().mockResolvedValueOnce({});
            BackupCodeRepositoryMock.prototype.saveMany = jest.fn().mockResolvedValueOnce({});

            const retrieveResponse = await request(app).post("/auth/mfa/confirm").set(headers).send({
                code: "123123",
                secret: "AAA",
            });

            expect(retrieveResponse.status).toBe(200);
            expect(retrieveResponse.body).toBeDefined();
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
            expect(mocked(authenticator.verify)).toBeCalled();
            expect(AzureService.prototype.addMFASecretForUser).toBeCalled();
            expect(BackupCodeRepositoryMock.prototype.saveMany).toBeCalled();
            expect(UserRepositoryMock.prototype.findUserById).toBeCalled();
            expect(UserRepositoryMock.prototype.updateSettings).toBeCalled();
        });

        it("should fail when mfa for user is already enabled", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(true);

            const retrieveResponse = await request(app).post("/auth/mfa/confirm").set(headers).send({
                code: "123123",
                secret: "AAA",
            });

            expect(retrieveResponse.status).toBe(400);
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
        });

        it("should fail when with invalid code", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(false);
            AzureService.prototype.addMFASecretForUser = jest.fn().mockResolvedValueOnce({});

            mocked(authenticator.verify).mockReturnValueOnce(false);
            const retrieveResponse = await request(app).post("/auth/mfa/confirm").set(headers).send({
                code: "123123",
                secret: "AAA",
            });
            expect(retrieveResponse.status).toBe(400);
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
            expect(mocked(authenticator.verify)).toBeCalled();
        });
    });

    describe("verify code from azure", () => {
        it("should succeed with TOTP code", async () => {
            mocked(authenticator.verify).mockReturnValueOnce(true);

            const retrieveResponse = await request(app)
                .post("/auth/mfa/verify")
                .set({ ...headers, "x-functions-key": "AZURE_API_KEY" })
                .send({
                    user_id: 1,
                    code: "123123",
                    codeType: MfaCodeType.Totp,
                    secretKey: "AAA",
                });

            expect(retrieveResponse.status).toBe(204);
            expect(mocked(authenticator.verify)).toBeCalled();
        });

        it("should fail with invalid TOTP code", async () => {
            mocked(authenticator.verify).mockReturnValueOnce(false);

            const retrieveResponse = await request(app)
                .post("/auth/mfa/verify")
                .set({ ...headers, "x-functions-key": "AZURE_API_KEY" })
                .send({
                    user_id: 1,
                    code: "123123",
                    codeType: MfaCodeType.Totp,
                    secretKey: "AAA",
                });

            expect(retrieveResponse.status).toBe(400);
            expect(mocked(authenticator.verify)).toBeCalled();
        });

        it("should succeed with BACKUP code", async () => {
            BackupCodeRepositoryMock.prototype.findAllUnusedByUserId = jest.fn().mockResolvedValueOnce([
                {
                    backup_code_id: 2,
                    backup_code_value: await bcrypt.hash("123123123123", 10),
                },
            ]);
            BackupCodeRepositoryMock.prototype.setCodeAsUsed = jest.fn().mockResolvedValueOnce({});

            const retrieveResponse = await request(app)
                .post("/auth/mfa/verify")
                .set({ ...headers, "x-functions-key": "AZURE_API_KEY" })
                .send({
                    user_id: 1,
                    code: "123123123123",
                    codeType: MfaCodeType.Backup,
                    secretKey: "AAA",
                });

            expect(retrieveResponse.status).toBe(204);
            expect(BackupCodeRepositoryMock.prototype.findAllUnusedByUserId).toBeCalledWith(1);
            expect(BackupCodeRepositoryMock.prototype.setCodeAsUsed).toBeCalledWith(2);
        });

        it("should fail when all BACKUP codes used up", async () => {
            BackupCodeRepositoryMock.prototype.findAllUnusedByUserId = jest.fn().mockResolvedValueOnce([]);

            const retrieveResponse = await request(app)
                .post("/auth/mfa/verify")
                .set({ ...headers, "x-functions-key": "AZURE_API_KEY" })
                .send({
                    user_id: 1,
                    code: "123123123123",
                    codeType: MfaCodeType.Backup,
                    secretKey: "AAA",
                });

            expect(retrieveResponse.status).toBe(400);
            expect(BackupCodeRepositoryMock.prototype.findAllUnusedByUserId).toBeCalledWith(1);
        });

        it("should fail when BACKUP code does not match", async () => {
            BackupCodeRepositoryMock.prototype.findAllUnusedByUserId = jest.fn().mockResolvedValueOnce([
                {
                    backup_code_value: await bcrypt.hash("123123123123", 10),
                },
            ]);

            const retrieveResponse = await request(app)
                .post("/auth/mfa/verify")
                .set({ ...headers, "x-functions-key": "AZURE_API_KEY" })
                .send({
                    user_id: 1,
                    code: "AAAAAAAAAAAA",
                    codeType: MfaCodeType.Backup,
                    secretKey: "AAA",
                });

            expect(retrieveResponse.status).toBe(400);
            expect(BackupCodeRepositoryMock.prototype.findAllUnusedByUserId).toBeCalledWith(1);
        });
    });

    describe("disable mfa with TOTP code", () => {
        it("should succeed", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(true);
            AzureService.prototype.getMFASecretByUserId = jest.fn().mockResolvedValueOnce("AAA");
            mocked(authenticator.verify).mockReturnValueOnce(true);
            AzureService.prototype.removeMFASecretForUser = jest.fn().mockResolvedValueOnce({});
            BackupCodeRepositoryMock.prototype.deleteManyByUserId = jest.fn().mockResolvedValueOnce({});
            UserRepositoryMock.prototype.findUserById = jest.fn().mockResolvedValueOnce({
                test: "test",
            });
            UserRepositoryMock.prototype.updateSettings = jest.fn().mockResolvedValueOnce({});

            const retrieveResponse = await request(app).post("/auth/mfa/disable").set(headers).send({
                code: "123123",
            });

            expect(retrieveResponse.status).toBe(204);
            expect(mocked(authenticator.verify)).toBeCalled();
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
            expect(AzureService.prototype.getMFASecretByUserId).toBeCalled();
            expect(AzureService.prototype.removeMFASecretForUser).toBeCalled();
            expect(BackupCodeRepositoryMock.prototype.deleteManyByUserId).toBeCalled();
            expect(UserRepositoryMock.prototype.findUserById).toBeCalled();
            expect(UserRepositoryMock.prototype.updateSettings).toBeCalled();
        });

        it("should fail when mfa is not enabled", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(false);

            const retrieveResponse = await request(app).post("/auth/mfa/disable").set(headers).send({
                code: "123123",
            });

            expect(retrieveResponse.status).toBe(400);
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
        });

        it("should fail when mfa code is invalid", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(true);
            AzureService.prototype.getMFASecretByUserId = jest.fn().mockResolvedValueOnce("AAA");
            mocked(authenticator.verify).mockReturnValueOnce(false);

            const retrieveResponse = await request(app).post("/auth/mfa/disable").set(headers).send({
                code: "123123",
            });

            expect(retrieveResponse.status).toBe(400);
            expect(mocked(authenticator.verify)).toBeCalled();
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
            expect(AzureService.prototype.getMFASecretByUserId).toBeCalled();
        });
    });

    describe("disable mfa with BACKUP code", () => {
        it("should succeed", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(true);
            BackupCodeRepositoryMock.prototype.findAllUnusedByUserId = jest.fn().mockResolvedValueOnce([
                {
                    backup_code_id: 2,
                    backup_code_value: await bcrypt.hash("123123123123", 10),
                },
            ]);
            AzureService.prototype.removeMFASecretForUser = jest.fn().mockResolvedValueOnce({});
            BackupCodeRepositoryMock.prototype.deleteManyByUserId = jest.fn().mockResolvedValueOnce({});
            UserRepositoryMock.prototype.findUserById = jest.fn().mockResolvedValueOnce({
                test: "test",
            });
            UserRepositoryMock.prototype.updateSettings = jest.fn().mockResolvedValueOnce({});

            const retrieveResponse = await request(app).post("/auth/mfa/disable-using-backup").set(headers).send({
                code: "123123123123",
            });

            expect(retrieveResponse.status).toBe(204);
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
            expect(BackupCodeRepositoryMock.prototype.findAllUnusedByUserId).toBeCalled();
            expect(AzureService.prototype.removeMFASecretForUser).toBeCalled();
            expect(BackupCodeRepositoryMock.prototype.deleteManyByUserId).toBeCalled();
            expect(UserRepositoryMock.prototype.findUserById).toBeCalled();
            expect(UserRepositoryMock.prototype.updateSettings).toBeCalled();
        });

        it("should fail with invalid code", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(true);
            BackupCodeRepositoryMock.prototype.findAllUnusedByUserId = jest.fn().mockResolvedValueOnce([
                {
                    backup_code_id: 2,
                    backup_code_value: await bcrypt.hash("123123123123", 10),
                },
            ]);

            const retrieveResponse = await request(app).post("/auth/mfa/disable-using-backup").set(headers).send({
                code: "AAAAAAAAAAAA",
            });

            expect(retrieveResponse.status).toBe(400);
            expect(BackupCodeRepositoryMock.prototype.findAllUnusedByUserId).toBeCalled();
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
        });

        it("should fail when mfa not enabled", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.isMFAEnabledForUser = jest.fn().mockResolvedValueOnce(false);

            const retrieveResponse = await request(app).post("/auth/mfa/disable-using-backup").set(headers).send({
                code: "AAAAAAAAAAAA",
            });

            expect(retrieveResponse.status).toBe(400);
            expect(AzureService.prototype.isMFAEnabledForUser).toBeCalled();
        });
    });
});
