import { MFAService } from "./mfa.service";
import { AuthorizedRequest } from "./../../common/types";
import Container from "typedi";
import { Route, Get, Response, Tags, Security, Controller, Request, Post, Body, OperationId, Hidden, Deprecated } from "tsoa";
import {
    CheckMFAForNewSocialAccountRequest,
    CheckMFAForNewSocialAccountResponse,
    CodeGenerationResponse,
    ConfirmMFARequest,
    DisableMFARequest,
    DisableMFAUsingBackupRequest,
    VerifyMFARequest,
} from "./mfa.contract";

@Tags("MFA")
@Route("auth/mfa")
export class MFAController extends Controller {
    private mfaService: MFAService = Container.get(MFAService);

    /**
     * Generate MFA.
     * @summary Generate MFA
     */
    @OperationId("GenerateMFASecret")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Response(404, "Not Found")
    @Get("generate")
    public async generate(@Request() request: AuthorizedRequest): Promise<CodeGenerationResponse> {
        return this.mfaService.generate(request.authData);
    }

    /**
     * Confirm MFA.
     * @summary Confirm MFA
     */
    @OperationId("ConfirmMFASecret")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Post("confirm")
    public async confirm(@Request() request: AuthorizedRequest, @Body() data: ConfirmMFARequest): Promise<string[]> {
        return this.mfaService.confirm(request.authData, data);
    }

    /**
     * Disable MFA.
     * @summary Disable MFA
     */
    @OperationId("DisableMFA")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Post("disable")
    public async disable(@Request() request: AuthorizedRequest, @Body() data: DisableMFARequest): Promise<void> {
        await this.mfaService.disable(request.authData, data.code);
    }

    /**
     * Disable MFA (backup).
     * @summary Disable MFA (backup)
     */
    @OperationId("DisableMFAUsingBackup")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Post("disable-using-backup")
    public async disableUsingBackup(@Request() request: AuthorizedRequest, @Body() data: DisableMFAUsingBackupRequest): Promise<void> {
        await this.mfaService.disableUsingBackup(request.authData, data.code);
    }

    @Hidden()
    @OperationId("VerifyMFA")
    @Security("azure_api_key", ["AZURE_API_KEY"])
    @Response(400, "Bad Request")
    @Post("verify")
    public async verify(@Body() data: VerifyMFARequest): Promise<void> {
        await this.mfaService.verify(data);
    }

    @Hidden()
    @OperationId("CheckMFAForSocialAccount")
    @Security("azure_api_key", ["AZURE_API_KEY"])
    @Response(400, "Bad Request")
    @Post("check-mfa-for-social")
    public async checkMFAForNewSocialAccount(
        @Body() data: CheckMFAForNewSocialAccountRequest
    ): Promise<CheckMFAForNewSocialAccountResponse> {
        return this.mfaService.checkMFAForNewSocialAccount(data);
    }
}
