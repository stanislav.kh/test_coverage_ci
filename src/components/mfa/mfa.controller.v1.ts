import { ApiResponse } from "syntropy-common-ts";
import { Route, Controller, Security, Tags, SuccessResponse, OperationId, Get, Post, Body, Request } from "tsoa";
import Container from "typedi";
import { AuthorizedRequest } from "../../common/types";
import { CodeGenerationResponse, ConfirmMFARequest, DisableMFARequest, VerifyMFARequest } from "./mfa.contract";
import { MFAService } from "./mfa.service";

@Route("api/v1/network/auth")
export class MFAControllerV1 extends Controller {
    private mfaService: MFAService = Container.get(MFAService);

    /**
     * Generates MFA.
     * @summary Generate MFA
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Tags("MFA")
    @SuccessResponse(200, "OK")
    @OperationId("V1NetworkAuthMfaGenerate")
    @Get("mfa/generate")
    public async GenerateMfa(@Request() request: AuthorizedRequest): Promise<ApiResponse<CodeGenerationResponse>> {
        return { data: await this.mfaService.generate(request.authData) };
    }

    /**
     * Confirms MFA.
     * @summary Confirm MFA
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Tags("MFA")
    @SuccessResponse(200, "OK")
    @OperationId("V1NetworkAuthMfaConfirm")
    @Post("mfa/confirm")
    public async ConfirmMfa(@Request() request: AuthorizedRequest, @Body() data: ConfirmMFARequest): Promise<ApiResponse<string[]>> {
        return { data: await this.mfaService.confirm(request.authData, data) };
    }

    /**
     * Disables MFA.
     * @summary Disable MFA
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Tags("MFA")
    @SuccessResponse(204, "No Content")
    @OperationId("V1NetworkAuthMfaDisable")
    @Post("mfa/disable")
    public async DisableMfa(@Request() request: AuthorizedRequest, @Body() data: DisableMFARequest): Promise<void> {
        await this.mfaService.disable(request.authData, data.code);
    }

    /**
     * Disables MFA using backup.
     * @summary Disable MFA (backup)
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Tags("MFA")
    @SuccessResponse(204, "No Content")
    @OperationId("V1NetworkAuthMfaDisableUsingBackup")
    @Post("mfa/disable-using-backup")
    public async DisableMfaUsingBackup(@Body() data: VerifyMFARequest): Promise<void> {
        await this.mfaService.verify(data);
    }
}
