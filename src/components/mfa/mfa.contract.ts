/**
 * @isString Invalid code
 * @minLength 6 Invalid code
 * @maxLength 6 Invalid code
 * @pattern ^[0-9]*$ Invalid code
 * @example 123123
 */
export type MFACode = string;

/**
 * @isString Invalid code
 * @minLength 12 Invalid code
 * @maxLength 12 Invalid code
 * @example AAABBBCCCDDD
 */
export type MFABackupCode = string;

export enum MfaCodeType {
    Totp = "TOTP",
    Backup = "BACKUP",
}

export interface VerifyMFARequest {
    user_id: number;
    code: MFACode | MFABackupCode;
    codeType: MfaCodeType;
    secretKey: string;
}

export interface ConfirmMFARequest {
    code: MFACode;
    secret: string;
}

export interface DisableMFARequest {
    code: MFACode;
}

export interface DisableMFAUsingBackupRequest {
    code: MFABackupCode;
}

export interface CodeGenerationResponse {
    authCode: string;
    secret: string;
}
export interface CheckMFAForNewSocialAccountRequest {
    email: string;
}

export interface CheckMFAForNewSocialAccountResponse {
    secret?: string;
    userId?: string;
}
