import { UserService } from "./../users/user.service";
import { BackupCode, BackupCodeObject } from "./../backup-codes/backup-code.model";
import { AuthData } from "./../auth/auth.contracts";
import Container, { Service } from "typedi";
import { authenticator } from "otplib";
import { getEnv } from "../../config/env";
import {
    CheckMFAForNewSocialAccountRequest,
    CheckMFAForNewSocialAccountResponse,
    CodeGenerationResponse,
    ConfirmMFARequest,
    MFABackupCode,
    MFACode,
    MfaCodeType,
    VerifyMFARequest,
} from "./mfa.contract";
import { AzureService } from "../azure-auth/azure-auth.service";
import { CustomValidationError, GenericHelpers } from "syntropy-common-ts";
import * as bcrypt from "bcrypt";
import { getCustomRepository } from "typeorm";
import { BackupCodeRepository } from "../backup-codes/backup-code.repository";

@Service()
export class MFAService {
    private backupCodeRepository: BackupCodeRepository = getCustomRepository(BackupCodeRepository);
    private azureService: AzureService = Container.get(AzureService);
    private userService: UserService = Container.get(UserService);

    public async generate(authData: AuthData): Promise<CodeGenerationResponse> {
        const isMFAEnabled = await this.azureService.isMFAEnabledForUser(authData.azure_user_id);
        if (isMFAEnabled) {
            throw new CustomValidationError("MFA_ALREADY_ENABLED", "MFA already enabled.");
        }

        const secret = authenticator.generateSecret();
        const authCode = authenticator.keyuri(authData.user_email, getEnv().MFA_ISSUER!, secret);
        return { authCode, secret };
    }

    public async confirm(authData: AuthData, { secret, code }: ConfirmMFARequest): Promise<string[]> {
        const isMFAEnabled = await this.azureService.isMFAEnabledForUser(authData.azure_user_id);
        if (isMFAEnabled) {
            throw new CustomValidationError("MFA_ALREADY_ENABLED", "MFA already enabled.");
        }

        const isCodeValid = authenticator.verify({ secret, token: code });
        if (!isCodeValid) {
            throw new CustomValidationError("INVALID_CODE", "Invalid MFA code.");
        }

        const { codes, plainTextCodes } = await this.generateBackupCodes(authData.user_id);
        await this.backupCodeRepository.saveMany(codes);
        await this.azureService.addMFASecretForUser(authData.user_id, secret);
        await this.userService.setTwoFactorAuthentification(authData.user_id, true);

        return plainTextCodes;
    }

    public async disable(authData: AuthData, totpCode: MFACode): Promise<void> {
        const isMFAEnabled = await this.azureService.isMFAEnabledForUser(authData.azure_user_id);
        if (!isMFAEnabled) {
            throw new CustomValidationError("INVALID_REQUEST", "MFA is not enabled.");
        }

        const secretKey = await this.azureService.getMFASecretByUserId(authData.azure_user_id);
        await this.verifyTotpCode(secretKey, totpCode);
        await this.azureService.removeMFASecretForUser(authData.user_id);
        await this.backupCodeRepository.deleteManyByUserId(authData.user_id);
        await this.userService.setTwoFactorAuthentification(authData.user_id, false);
    }

    public async disableUsingBackup(authData: AuthData, code: MFABackupCode): Promise<void> {
        const isMFAEnabled = await this.azureService.isMFAEnabledForUser(authData.azure_user_id);
        if (!isMFAEnabled) {
            throw new CustomValidationError("INVALID_REQUEST", "MFA is not enabled.");
        }

        await this.verifyBackupCode(authData.user_id, code);
        await this.azureService.removeMFASecretForUser(authData.user_id);
        await this.backupCodeRepository.deleteManyByUserId(authData.user_id);
        await this.userService.setTwoFactorAuthentification(authData.user_id, false);
    }

    public async verify({ code, codeType, secretKey, user_id }: VerifyMFARequest): Promise<void> {
        if (codeType === MfaCodeType.Totp) {
            await this.verifyTotpCode(secretKey, code);
        } else if (codeType === MfaCodeType.Backup) {
            const usedBackupCode = await this.verifyBackupCode(user_id, code);
            await this.backupCodeRepository.setCodeAsUsed(usedBackupCode.backup_code_id);
        } else {
            throw new CustomValidationError("INVALID_TOTP_CODE_TYPE", "Provide totp code type is invalid.");
        }
    }

    public async check({ code, codeType, secretKey, user_id }: VerifyMFARequest): Promise<void> {
        if (codeType === MfaCodeType.Totp) {
            await this.verifyTotpCode(secretKey, code);
        } else if (codeType === MfaCodeType.Backup) {
            const usedBackupCode = await this.verifyBackupCode(user_id, code);
            await this.backupCodeRepository.setCodeAsUsed(usedBackupCode.backup_code_id);
        } else {
            throw new CustomValidationError("INVALID_TOTP_CODE_TYPE", "Provide totp code type is invalid.");
        }
    }

    public async checkMFAForNewSocialAccount({ email }: CheckMFAForNewSocialAccountRequest): Promise<CheckMFAForNewSocialAccountResponse> {
        const user = await this.userService.findUserByEmail(email);
        if (user) {
            return {
                userId: user.user_id.toString(),
                secret: await this.azureService.getMFASecretByInternalUserId(user?.user_id),
            };
        }

        return {};
    }

    private async verifyTotpCode(secret: string, token: string): Promise<void> {
        const result = authenticator.verify({ secret, token });

        if (!result) {
            throw new CustomValidationError("INVALID_CODE", "Invalid MFA code.");
        }
    }

    private async verifyBackupCode(userId: number, code: MFABackupCode): Promise<BackupCode> {
        const backupCodes = await this.backupCodeRepository.findAllUnusedByUserId(userId);
        if (!backupCodes?.length) {
            throw new CustomValidationError(
                "BACKUP_CODES_USED_UP",
                "You do not have any unused backup codes left. Please contact support."
            );
        }

        const matchingCode = backupCodes.find(({ backup_code_value }) => bcrypt.compareSync(code, backup_code_value));
        if (!matchingCode) {
            throw new CustomValidationError("INVALID_REQUEST", "Backup code is not valid.");
        }

        return matchingCode;
    }

    private async generateBackupCodes(userId: number): Promise<{ plainTextCodes: string[]; codes: BackupCodeObject[] }> {
        const plainTextCodes: string[] = [];
        const codes: BackupCodeObject[] = [];
        for (let i = 0; i < 8; i++) {
            const code = GenericHelpers.getRandomString(12).toUpperCase();
            plainTextCodes.push(code);
            codes.push({
                backup_code_value: await bcrypt.hash(code, 10),
                user_id: userId,
            });
        }

        return { plainTextCodes, codes };
    }
}
