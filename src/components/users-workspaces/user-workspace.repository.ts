import { EntityRepository, EntityManager, In } from "typeorm";
import { RoleName } from "../role/role.model";
import { WorkspaceInvitation } from "../workspaces-invitations/workspace-invitation.model";
import { UserWorkspace } from "./user-workspace.model";

@EntityRepository()
export class UserWorkspaceRepository {
    constructor(private manager: EntityManager) {}

    public async findByUserAndWorkspaceId(
        user_id: number,
        workspace_id: number
    ): Promise<Required<Pick<UserWorkspace, "user_id" | "role">> | undefined> {
        return this.manager
            .createQueryBuilder(UserWorkspace, "users_workspaces")
            .innerJoinAndSelect("users_workspaces.role", "role")
            .where("users_workspaces.user_id = :user_id AND users_workspaces.workspace_id = :workspace_id", { user_id, workspace_id })
            .getOne() as Promise<Required<Pick<UserWorkspace, "user_id" | "role">> | undefined>;
    }

    public async findByWorkspaceId(
        workspace_id: number
    ): Promise<Array<Required<Pick<UserWorkspace, "user_workspace_id" | "user" | "role">>>> {
        return this.manager
            .createQueryBuilder(UserWorkspace, "users_workspaces")
            .innerJoin("users_workspaces.user", "user")
            .innerJoin("users_workspaces.role", "role")
            .select(["users_workspaces.user_workspace_id", "user", "role"])
            .where("users_workspaces.workspace_id = :workspace_id", { workspace_id })
            .andWhere("role.role_name IN (:...user_workspace_roles)", { user_workspace_roles: [RoleName.Admin, RoleName.User] })
            .getMany();
    }

    public async findOneByUserId(user_id: number): Promise<UserWorkspace | undefined> {
        return this.manager.findOne(UserWorkspace, { user_id });
    }

    public async deleteByWorkspaceIdUserIds(workspace_id: number, user_ids: number[]): Promise<void> {
        await this.manager.delete(UserWorkspace, { workspace_id, user_id: In(user_ids) });
    }

    public async updateRoleByWorkspaceAndUserId(workspace_id: number, user_id: number, new_role_id: number): Promise<void> {
        await this.manager.update(UserWorkspace, { user_id, workspace_id }, { role_id: new_role_id });
    }

    public async create(user_id: number, invitation: WorkspaceInvitation): Promise<void> {
        const new_user_workspace = new UserWorkspace({
            user_id,
            role_id: invitation.role_id,
            workspace_id: invitation.workspace_id,
        });

        await this.manager.transaction(async (txManager) => {
            await txManager.save(new_user_workspace);
            await txManager.save(invitation);
        });
    }
}
