import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne, UpdateDateColumn, OneToOne, JoinColumn } from "typeorm";
import { IsEmpty, IsInt } from "class-validator";

import { Model } from "syntropy-common-ts";
import { User } from "../users/user.model";
import { Workspace } from "../workspace/workspace.model";
import { Role } from "../role/role.model";

export interface UserWorkspaceObject {
    user_id: number;
    workspace_id: number;
    role_id: number;
}

@Entity("users_workspaces")
export class UserWorkspace extends Model<UserWorkspaceObject> implements UserWorkspaceObject {
    constructor(object?: UserWorkspaceObject) {
        super(object);
    }

    @PrimaryGeneratedColumn()
    @IsEmpty()
    public user_workspace_id!: number;

    @Column()
    @IsInt()
    public user_id!: number;

    @Column()
    @IsInt()
    public workspace_id!: number;

    @Column()
    @IsInt()
    public role_id!: number;

    @CreateDateColumn({ type: "timestamptz" })
    public user_workspace_created_at!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    public user_workspace_updated_at!: Date;

    // --------------------------------------------------
    // Relations
    // --------------------------------------------------

    @ManyToOne(() => User, (u) => u.user_workspaces)
    @JoinColumn({ referencedColumnName: "user_id", name: "user_id" })
    public user!: User;

    @ManyToOne(() => Workspace, (w) => w.user_workspaces)
    @JoinColumn({ referencedColumnName: "workspace_id", name: "workspace_id" })
    public workspace!: Workspace;

    @OneToOne((_) => Role)
    @JoinColumn({ referencedColumnName: "role_id", name: "role_id" })
    public role!: Role;
}
