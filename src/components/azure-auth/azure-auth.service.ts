import { CorruptedUserData, AccessTokenReadData } from "./../auth/auth.contracts";
// MS graph client neeeds this
import "isomorphic-fetch";

import Container, { Service } from "typedi";
import * as jwksRsa from "jwks-rsa";
import { AuthenticationProvider, Client } from "@microsoft/microsoft-graph-client";
import { ForbiddenError, ServerError } from "syntropy-common-ts";
import * as jwt from "jsonwebtoken";
import fetch from "node-fetch";
import { AzureUserTokenDto, AzureUser, AzureTokenPayload } from "./azure-auth.contracts";
import { EnvConfig, getEnv } from "../../config/env";
import { RedisService } from "../redis/redis.service";
import { GenericHelpers } from "../../helpers/generic-helpers";
import { User } from "../users/user.model";

class AzureAuthenticationProvider implements AuthenticationProvider {
    public async getAccessToken(): Promise<string> {
        const env = getEnv();
        const res = await fetch(env.AZURE_TOKEN_URL!, {
            method: "POST",
            body: new URLSearchParams({
                client_id: env.AZURE_CLIENT_ID!,
                scope: env.AZURE_SCOPE!,
                grant_type: env.AZURE_GRANT_TYPE!,
                client_secret: env.AZURE_CLIENT_SECRET!,
            }).toString(),
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
        });

        const data = await res.json();
        return data.access_token;
    }
}

const jwksClient: jwksRsa.JwksClient = jwksRsa({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: getEnv().AZURE_JWKS_URI!,
});

export enum AuthSource {
    social = "socialIdpAuthentication",
    local = "localAccountAuthentication",
}

@Service()
export class AzureService {
    private env: Partial<EnvConfig> = getEnv();
    private accessTokenExpProp: string = `${this.env.AZURE_EXTENSION_PROP}_accessTokenExp`;
    private accessTokenDescProp: string = `${this.env.AZURE_EXTENSION_PROP}_accessTokenDesc`;
    private accessTokenProp: string = `${this.env.AZURE_EXTENSION_PROP}_accessToken`;
    private internalUserProp: string = `${this.env.AZURE_EXTENSION_PROP}_internalUser`;
    private ownerUserProp: string = `${this.env.AZURE_EXTENSION_PROP}_ownerUser`;
    private accessTokenWorkspaceProp: string = `${this.env.AZURE_EXTENSION_PROP}_accessTokenWorkspace`;
    private verifiedProp: string = `${this.env.AZURE_EXTENSION_PROP}_verified`;
    private mfaSecretProp: string = `${this.env.AZURE_EXTENSION_PROP}_strongAuthenticationAppSecretKey`;

    public client: Client = Client.initWithMiddleware({
        authProvider: new AzureAuthenticationProvider(),
    });

    private getAzureKey(header: any, callback: any): void {
        jwksClient.getSigningKey(header.kid, (_, key: any): void => {
            const signingKey = key?.publicKey || key?.rsaPublicKey;
            callback(null, signingKey);
        });
    }

    private async verifyAzureToken(token: string): Promise<AzureTokenPayload> {
        return new Promise<AzureTokenPayload>((resolve, reject) => {
            jwt.verify(token, this.getAzureKey, (err, decoded) => {
                if (err) {
                    reject(new ForbiddenError());
                } else {
                    resolve(decoded as AzureTokenPayload);
                }
            });
        });
    }

    private serializeAzureUser(userId: string, tokenPayload: AzureTokenPayload): AzureUser {
        const isFirstSocialLogin = !tokenPayload.extension_internalUser && tokenPayload.extension_verified;

        // Token does not contain scopes and internalUser when first time loging in using social idp
        if (isFirstSocialLogin) {
            return {
                id: userId,
                verified: tokenPayload.extension_verified,
                isFirstSocialLogin: true,
                email: tokenPayload.email,
                name: tokenPayload.name,
            };
        }

        return {
            id: userId,
            internalUser: Number(tokenPayload.extension_internalUser),
            ownerUser: tokenPayload.extension_ownerUser,
            accessToken: tokenPayload.extension_accessToken,
            accessTokenExp: tokenPayload.extension_accessTokenExp,
            verified: tokenPayload.extension_verified,
            isFirstSocialLogin: false,
            email: tokenPayload.email,
            name: tokenPayload.name,
            workspace_id: Number(tokenPayload.workspace_id),
            roles: tokenPayload.roles,
            permissions: tokenPayload.permissions,
            scopes: tokenPayload.scopes,
        };
    }

    public async getUserFromToken(token: string): Promise<AzureUser> {
        const tokenPayload = await this.verifyAzureToken(token);
        const userId = tokenPayload.oid || tokenPayload.sub;
        return this.serializeAzureUser(userId, tokenPayload);
    }

    public async createUser(azureUserId: string, internalUserId: number, email: string): Promise<void> {
        await this.client.api(`/users/${azureUserId}`).patch({
            [this.accessTokenProp]: false,
            [this.internalUserProp]: internalUserId.toString(),
            [this.ownerUserProp]: internalUserId,
            [this.verifiedProp]: false,
            displayName: email,
        });
    }

    public async hasAccessTokenName(tokenName: string, workspaceId: number): Promise<boolean> {
        const res = (await this.client
            .api(`/users`)
            .select(`id`)
            .filter(`displayName eq '${tokenName}' and ${this.accessTokenWorkspaceProp} eq '${workspaceId}'`)
            .get()) as { "@odata.context": string; value: any[] };

        return res.value && res.value.length > 0;
    }

    public async createAccessToken(
        ownerUserExternalId: number,
        workspaceId: number,
        tokenExternalId: number,
        username: string,
        password: string,
        tokenExpAt: string,
        tokenName: string,
        tokenDescription: string | undefined
    ): Promise<void> {
        await this.client.api(`/users`).post({
            [this.internalUserProp]: String(tokenExternalId),
            [this.ownerUserProp]: ownerUserExternalId,
            [this.accessTokenWorkspaceProp]: String(workspaceId),
            [this.verifiedProp]: true,
            [this.accessTokenProp]: true,
            [this.accessTokenDescProp]: tokenDescription,
            [this.accessTokenExpProp]: tokenExpAt,
            displayName: tokenName,
            identities: [
                {
                    signInType: "userName",
                    issuer: getEnv().AZURE_TENANT,
                    issuerAssignedId: username,
                },
            ],
            passwordPolicies: "DisablePasswordExpiration",
            passwordProfile: {
                password: password,
                forceChangePasswordNextSignIn: false,
            },
            accountEnabled: true,
        });
        await Container.get(RedisService).deleteMatchingKeys(`${workspaceId}/accessTokens/*`);
    }

    public async listAccessTokensAndCount(
        workspaceId: number,
        skip: number,
        take: number,
        order?: string
    ): Promise<[AccessTokenReadData[], number]> {
        // azure does not support $count, $skip, $order for users collection, so dumb way is the only way
        let accessTokensToReturn = await Container.get(RedisService).get(`${workspaceId}/accessTokens/${order}`);

        if (!accessTokensToReturn) {
            let accessTokens: any = null;
            if (order) {
                const tokens = await Container.get(RedisService).get(`${workspaceId}/accessTokens/undefined`);
                if (tokens) {
                    accessTokens = JSON.parse(tokens);
                }
            }
            if (!accessTokens) {
                const res = (await this.client
                    .api(`/users`)
                    .select(`displayName,${this.accessTokenDescProp},${this.accessTokenExpProp},id`)
                    .filter(`${this.accessTokenProp} eq true and ${this.accessTokenWorkspaceProp} eq '${workspaceId}'`)
                    .get()) as { "@odata.context": string; value: any[] };

                accessTokens = res.value.map((item) => ({
                    access_token_name: item.displayName,
                    access_token_description: item[this.accessTokenDescProp],
                    access_token_expiration: item[this.accessTokenExpProp],
                    access_token_id: item.id,
                }));
                Container.get(RedisService).set(`${workspaceId}/accessTokens/undefined`, JSON.stringify(accessTokens));
            }

            if (order) {
                const [sortField, sortOrder] = order.split(":");
                accessTokens = GenericHelpers.sort(accessTokens, sortOrder, sortField);
                Container.get(RedisService).set(`${workspaceId}/accessTokens/${order}`, JSON.stringify(accessTokens));
            }
            accessTokensToReturn = accessTokens;
        }
        const parsedAccessTokens = typeof accessTokensToReturn === "string" ? JSON.parse(accessTokensToReturn) : accessTokensToReturn;
        return [parsedAccessTokens.slice(skip, skip + take), parsedAccessTokens.length];
    }

    /**
     * Returns access token user id within authorized user's scope.
     */
    public async findAccessTokenUserId(workspaceId: number, accessTokenId: string): Promise<number | undefined> {
        const res = (await this.client
            .api(`/users`)
            .select(`displayName,${this.accessTokenDescProp},${this.accessTokenExpProp},${this.internalUserProp},id`)
            .filter(`${this.accessTokenProp} eq true and ${this.accessTokenWorkspaceProp} eq '${workspaceId}' and id eq '${accessTokenId}'`)
            .get()) as { "@odata.context": string; value: any[] };
        // sanity check
        if (res.value.length > 1) {
            throw new ServerError();
        }
        let userId: string | undefined = undefined;
        if (res.value.length === 1) {
            userId = (res.value[0][this.internalUserProp] ||
                // HACK: for some reason Azure started to return *_internalUser instead of *_InternalUser.
                // Azure acknowledged this as a bug, see: https://syntropy.atlassian.net/browse/NOIASDN-3504
                res.value[0][`${this.env.AZURE_EXTENSION_PROP}_InternalUser`]) as string | undefined;
        }

        if (userId) {
            const userIdInt = parseInt(userId);
            if (Number.isInteger(userIdInt)) {
                return userIdInt;
            }
            return undefined;
        }
        return undefined;
    }

    public async getInternalUserId(objectId: string): Promise<number | undefined> {
        const res = (await this.client.api(`/users/${objectId}`).select(`${this.internalUserProp}`).get()) as {
            "@odata.context": string;
            value: any[];
        };
        const userId: string | undefined = (res[this.internalUserProp] ||
            // HACK: for some reason Azure started to return *_internalUser instead of *_InternalUser.
            // Azure acknowledged this as a bug, see: https://syntropy.atlassian.net/browse/NOIASDN-3504
            res[`${this.env.AZURE_EXTENSION_PROP}_InternalUser`]) as string | undefined;

        if (userId) {
            const userIdInt = parseInt(userId);
            if (Number.isInteger(userIdInt)) {
                return userIdInt;
            }
            return undefined;
        }
        return undefined;
    }

    /**
     * Deletes access token by access token id (uuid).
     * Caller must make sure that all filter params are not malicious.
     */
    public async deleteAccessToken(workspaceId: number, accessTokenId: string): Promise<number | undefined> {
        const userId = await this.findAccessTokenUserId(workspaceId, accessTokenId);

        if (userId != null) {
            await this.client.api(`/users/${accessTokenId}`).delete();
        }
        await Container.get(RedisService).deleteMatchingKeys(`${workspaceId}/accessTokens/*`);
        return userId;
    }

    public async createUserFromSocialIdp(azureUserId: string, internalUserId: number, email: string): Promise<void> {
        await this.client.api(`/users/${azureUserId}`).patch({
            [this.accessTokenProp]: false,
            [this.internalUserProp]: internalUserId.toString(),
            [this.ownerUserProp]: internalUserId,
            [this.verifiedProp]: true,
            displayName: email,
        });
    }

    public async verifyUserEmail(userId: string): Promise<void> {
        await this.client.api(`/users/${userId}`).patch({
            [this.verifiedProp]: true,
        });
    }

    public async addMFASecretForUser(userId: number, secret: string): Promise<void> {
        const res = (await this.client.api(`/users`).select(["id"]).filter(`${this.ownerUserProp} eq ${userId}`).get()) as {
            "@odata.context": string;
            value: Array<{ id: string }>;
        };

        await Promise.all(
            res.value.map(async ({ id }) => {
                await this.client.api(`/users/${id}`).patch({
                    [this.mfaSecretProp]: secret,
                });
            })
        );
    }

    public async transferMFASecretForUser(internalUserId: number, azureUserId: string): Promise<void> {
        const res = (await this.client
            .api(`/users`)
            .select(`${this.mfaSecretProp}`)
            .filter(`${this.ownerUserProp} eq ${internalUserId}`)
            .get()) as {
            "@odata.context": string;
            value: any[];
        };

        const value = res.value.find((data) => !!data[this.mfaSecretProp]);
        if (value) {
            const secret: string = value[this.mfaSecretProp];
            await this.client.api(`/users/${azureUserId}`).patch({
                [this.mfaSecretProp]: secret,
            });
        }
    }

    public async removeMFASecretForUser(userId: number): Promise<void> {
        const res = (await this.client.api(`/users`).select(["id"]).filter(`${this.ownerUserProp} eq ${userId}`).get()) as {
            "@odata.context": string;
            value: Array<{ id: string }>;
        };

        await Promise.all(
            res.value.map(async ({ id }) => {
                await this.client.api(`/users/${id}`).patch({
                    [this.mfaSecretProp]: null,
                });
            })
        );
    }

    public async getMFASecretByInternalUserId(userId: number): Promise<string | undefined> {
        const res = (await this.client
            .api(`/users`)
            .select(`${this.mfaSecretProp}`)
            .filter(`${this.ownerUserProp} eq ${userId}`)
            .get()) as {
            "@odata.context": string;
            value: any;
        };

        if (!res.value?.length) {
            return undefined;
        }

        const data = res.value.find((item) => !!item[this.mfaSecretProp]);
        return data ? data[this.mfaSecretProp] : undefined;
    }

    public async getMFASecretByUserId(userId: string): Promise<string> {
        const data = await this.client.api(`/users/${userId}`).select([this.mfaSecretProp]).get();
        return data[this.mfaSecretProp];
    }

    public async isMFAEnabledForUser(userId: string): Promise<boolean> {
        return !!(await this.getMFASecretByUserId(userId));
    }

    public async getTokenForUser({ email, password }: { email: string; password: string }): Promise<AzureUserTokenDto> {
        const res = await fetch(this.env.AZURE_ROPC_TOKEN_URL!, {
            method: "POST",
            body: new URLSearchParams({
                client_id: this.env.AZURE_CLIENT_ID!,
                scope: this.env.AZURE_ROPC_SCOPES!,
                grant_type: this.env.AZURE_ROPC_GRANT_TYPE!,
                username: email,
                client_secret: this.env.AZURE_CLIENT_SECRET!,
                password,
                tenant: this.env.AZURE_TENANT!,
            }).toString(),
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
        });

        const data: any = await res.json();
        if (data.error) {
            throw new ForbiddenError(data.error_description);
        }

        return data as AzureUserTokenDto;
    }

    public async getCorruptedUsers(): Promise<CorruptedUserData[]> {
        const batchSize = 20;
        const select = [this.internalUserProp, "id", "displayName", "otherMails", "creationType"];
        const filter = (u) => !u[this.internalUserProp] && u.displayName === "unknown";
        const delay = async () =>
            new Promise((resolve) => {
                setTimeout(() => {
                    resolve(true);
                }, 2000);
            });

        let data = await this.client.api(`/users`).version("beta").select(select).top(batchSize).get();
        let skipToken = data["@odata.nextLink"].split("skiptoken=")[1];
        let azureUsers = data.value;

        const unverifiedAzureUsers: any[] = azureUsers.filter(filter);
        while (skipToken) {
            await delay();
            data = await this.client.api(`/users`).version("beta").select(select).top(batchSize).skipToken(skipToken).get();

            skipToken = data["@odata.nextLink"]?.split("skipToken=")[1];
            azureUsers = data.value;
            unverifiedAzureUsers.push(...azureUsers.filter(filter));
        }

        return unverifiedAzureUsers.map((u) => ({
            email: u.otherMails[0],
            id: u.id,
            authSource: u.creationType === "LocalAccount" ? AuthSource.local : AuthSource.social,
        }));
    }

    public async getAllUsersForOwnerPropMigration(): Promise<void> {
        const batchSize = 20;
        const select = `${this.internalUserProp}, ${this.ownerUserProp}, id`;
        const filter = (u) => u[this.internalUserProp] && !u[this.ownerUserProp];

        const delay = async () =>
            new Promise((resolve) => {
                setTimeout(() => {
                    resolve(true);
                }, 2000);
            });

        let data = await this.client.api(`/users`).version("beta").select(select).top(batchSize).get();
        let skipToken = data["@odata.nextLink"].split("skiptoken=")[1];
        let azureUsers = data.value;

        const unpatchedUsers: any[] = azureUsers.filter(filter);
        while (skipToken) {
            await delay();
            data = await this.client.api(`/users`).version("beta").select(select).top(batchSize).skipToken(skipToken).get();

            skipToken = data["@odata.nextLink"]?.split("skipToken=")[1];
            azureUsers = data.value;
            unpatchedUsers.push(...azureUsers.filter(filter));
        }

        for (const user of unpatchedUsers) {
            await delay();
            await this.client.api(`/users/${user.id}`).patch({
                [this.ownerUserProp]: user[this.internalUserProp],
            });
        }
    }

    public async migrateAccessTokenWorkspacesFromDefaultUserWorkspace(users: User[]): Promise<void> {
        const batchSize = 20;
        const select = `${this.accessTokenProp}, ${this.ownerUserProp}, ${this.accessTokenWorkspaceProp}, id`;
        const filter = (u) => u[this.accessTokenProp] && u[this.ownerUserProp] && !u[this.accessTokenWorkspaceProp];

        const delay = async () =>
            new Promise((resolve) => {
                setTimeout(() => {
                    resolve(true);
                }, 2000);
            });

        let data = await this.client.api(`/users`).version("beta").select(select).top(batchSize).get();
        let skipToken = data["@odata.nextLink"].split("skiptoken=")[1];
        let azureUsers = data.value;

        const unpatchedUsers: any[] = azureUsers.filter(filter);
        while (skipToken) {
            await delay();
            data = await this.client.api(`/users`).version("beta").select(select).top(batchSize).skipToken(skipToken).get();

            skipToken = data["@odata.nextLink"]?.split("skipToken=")[1];
            azureUsers = data.value;
            unpatchedUsers.push(...azureUsers.filter(filter));
        }

        for (const user of unpatchedUsers) {
            await delay();
            const workspace_default_id = users.find((u) => u.user_id === user[this.ownerUserProp])?.workspace_default_id;
            workspace_default_id &&
                (await this.client.api(`/users/${user.id}`).patch({
                    [this.accessTokenWorkspaceProp]: String(workspace_default_id),
                }));
        }
    }
}
