export interface AzureUserTokenDto {
    access_token: string;
    token_type: string;
    expires_in: string;
    refresh_token: string;
}

export interface AzureUser {
    id: string;
    internalUser?: number;
    ownerUser?: number;
    accessToken?: boolean;
    accessTokenExp?: string;
    verified: boolean;
    isFirstSocialLogin: boolean;
    email: string;
    name: string;
    workspace_id?: number;
    roles?: string;
    permissions?: string;
    scopes?: string;
}

export interface AzureTokenPayload {
    exp: number;
    nbf: number;
    ver: string;
    iss: string;
    sub: string;
    aud: string;
    acr: string;
    nonce: string;
    iat: number;
    auth_time: number;
    name: string;
    extension_internalUser?: string;
    extension_ownerUser?: number;
    extension_accessToken?: boolean;
    extension_accessTokenExp?: string;
    extension_verified: boolean;
    tid: string;
    oid?: string;
    authenticationSource?: string;
    email: string;
    workspace_id: string;
    roles?: string;
    permissions?: string;
    scopes?: string;
}
