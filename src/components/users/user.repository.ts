import { EntityRepository, EntityManager, FindManyOptions } from "typeorm";

import { Role } from "../role/role.model";
import { Permission } from "../permission/permission.model";
import { User, UserSettingsObject } from "./user.model";

@EntityRepository()
export class UserRepository {
    constructor(private manager: EntityManager) {}

    public async updateSettings(userId: number, settings: UserSettingsObject): Promise<void> {
        await this.manager.createQueryBuilder().update(User, { user_settings: settings }).where("user_id = :id", { id: userId }).execute();
    }

    public async findUserByExternalId(id: string): Promise<User | undefined> {
        return this.manager.findOne(User, { where: { user_external_id: id } });
    }

    public async findUserByEmail(email: string): Promise<User | undefined> {
        return this.manager.findOne(User, { where: { user_email: email } });
    }

    public async patchExternalId(email: string, id: string): Promise<void> {
        await this.manager.createQueryBuilder().update("users", { user_external_id: id }).where("user_email = :email", { email }).execute();
    }

    public async updateWorkspaceDefaultId(user_id: number, new_workspace_default_id: number | undefined): Promise<void> {
        await this.manager.update(User, { user_id }, { workspace_default_id: new_workspace_default_id });
    }

    public async save(
        email: string | null,
        settings: UserSettingsObject,
        scope: { role?: Role; permissions?: Permission[] },
        workspace_id: number
    ): Promise<User> {
        return this.manager.save(User, {
            user_email: email,
            role: scope.role,
            permissions: scope.permissions,
            user_settings: settings,
            workspace_default_id: workspace_id,
        } as User);
    }

    public async deleteByUserId(user_id: number): Promise<void> {
        await this.manager.delete(User, user_id);
    }

    public async findUserById<TEntity extends User>(id: number, opts?: FindManyOptions<TEntity>): Promise<TEntity | undefined> {
        return this.manager.findOne<TEntity>(User, { where: { user_id: id }, ...opts });
    }

    public async countAllUsers(): Promise<number> {
        return this.manager.count(User);
    }

    public async findUserScopeById(
        id: number
    ): Promise<
        | (Required<Pick<User, "user_id" | "user_email" | "user_settings" | "workspace_default_id">> & Pick<User, "role" | "permissions">)
        | undefined
    > {
        return this.manager
            .createQueryBuilder(User, "users")
            .leftJoin("users.role", "role")
            .leftJoin("users.permissions", "permissions")
            .select([
                "users.user_id",
                "users.user_email",
                "users.user_settings",
                "users.workspace_default_id",
                "permissions.permission_id",
                "permissions.permission_name",
                "permissions.permission_description",
                "role.role_id",
                "role.role_name",
            ])
            .where("users.user_id = :user_id", { user_id: id })
            .getOne() as Promise<
            Required<Pick<User, "role" | "permissions" | "user_id" | "user_email" | "user_settings" | "workspace_default_id">> | undefined
        >;
    }
}
