import Container, { Service } from "typedi";
import { EntityManager, FindManyOptions, getCustomRepository, getManager } from "typeorm";
import _ from "lodash";

import { Contact } from "./../auth/auth.contracts";
import { UserRepository } from "./user.repository";
import { User, UserSettingsObject } from "../users/user.model";
import { RoleRepository } from "../role/role.repository";
import { RoleName, WorkspaceUserRole } from "../role/role.model";
import { PermissionRepository } from "../permission/permission.repository";
import { AuthSource } from "../azure-auth/azure-auth.service";
import { SendGridAdapter } from "../../adapters/sendgrid-adapter";
import { Workspace } from "../workspace/workspace.model";
import { UserWorkspace } from "../users-workspaces/user-workspace.model";
import { DEFAULT_WORKSPACE_NAME } from "../workspace/workspace.model";
import { UserWithRole } from "./user.contracts";
import { UserWorkspaceRepository } from "../users-workspaces/user-workspace.repository";

@Service()
export class UserService {
    private userRepository: UserRepository = getCustomRepository(UserRepository);
    private userWorkspaceRepository: UserWorkspaceRepository = getCustomRepository(UserWorkspaceRepository);
    private roleRepository: RoleRepository = getCustomRepository(RoleRepository);
    private permissionRepository: PermissionRepository = getCustomRepository(PermissionRepository);
    private sendGridAdapter: SendGridAdapter = Container.get(SendGridAdapter);

    public async updateSettings(userId: number, data: UserSettingsObject): Promise<void> {
        const user = await this.userRepository.findUserById(userId);
        const settings: UserSettingsObject = {
            ...user?.user_settings,
            ...data,
            // Props should be overwritten only by internal methods
            auth_sources: user?.user_settings.auth_sources,
            two_factors_authentication: user?.user_settings.two_factors_authentication,
        };
        await this.userRepository.updateSettings(userId, settings);
    }

    public async setTwoFactorAuthentification(userId: number, newValue: boolean): Promise<void> {
        const user = await this.userRepository.findUserById(userId);
        const settings: UserSettingsObject = {
            ...user?.user_settings,
            two_factors_authentication: newValue,
        };
        await this.userRepository.updateSettings(userId, settings);
    }

    public async addAuthSource(userId: number, userSettings: UserSettingsObject, authSource: AuthSource): Promise<void> {
        const settings = { ...userSettings };
        if (settings.auth_sources?.length) {
            settings.auth_sources = Array.from(new Set([...settings.auth_sources, authSource]));
        } else {
            settings.auth_sources = [authSource];
        }
        await this.userRepository.updateSettings(userId, settings);
    }

    public async findUserByExternalId(id: string): Promise<User | undefined> {
        return this.userRepository.findUserByExternalId(id);
    }

    public async findUserByEmail(email: string): Promise<User | undefined> {
        return this.userRepository.findUserByEmail(email);
    }

    public async patchExternalId(email: string, id: string): Promise<void> {
        await this.userRepository.patchExternalId(email, id);
    }

    public async create(
        email: string | null,
        authSource: AuthSource,
        scope: { role: RoleName; permissions: string[] },
        workspace_id: number | null
    ): Promise<User> {
        const user: User = new User({ user_email: email });
        let default_workspace: Workspace;

        // Only access tokens will have workspace_id when being created
        if (workspace_id) {
            user.workspace_default_id = workspace_id;
        } else {
            default_workspace = new Workspace({ workspace_name: DEFAULT_WORKSPACE_NAME });
            user.default_workspace = default_workspace;
        }

        const scopeRole = await this.roleRepository.findRoleByName(scope.role);
        const userWorkspace = new UserWorkspace();
        userWorkspace.role = scopeRole;
        userWorkspace.user = user;

        // If role is AccessToken, then instead of assigning full role power use role permissions.
        user.role = scope.role && scope.role !== RoleName.AccessToken ? await this.roleRepository.findRoleByName(scope.role) : undefined;
        user.permissions = scope.permissions
            ? await this.permissionRepository.findRolePermissions(scope.permissions, scope.role)
            : undefined;
        user.user_settings = { auth_sources: [authSource], show_onboarding: true };

        await getManager().transaction(async (txManager: EntityManager) => {
            !workspace_id && (await txManager.save(default_workspace));
            await txManager.save(user);
            userWorkspace.workspace_id = workspace_id || default_workspace.workspace_id;
            await txManager.save(userWorkspace);
        });

        return user;
    }

    public async saveWorkspaceDefaultId(user: User, new_workspace_default_id: number | undefined): Promise<void> {
        if (new_workspace_default_id !== user.workspace_default_id) {
            await this.userRepository.updateWorkspaceDefaultId(user.user_id, new_workspace_default_id);
        }
    }

    public async findUserById<TEntity extends User>(id: number, opts?: FindManyOptions<TEntity>): Promise<TEntity | undefined> {
        return this.userRepository.findUserById<TEntity>(id, opts);
    }

    public async findUsersByWorkspaceId(workspace_id: number): Promise<UserWithRole[]> {
        const userWorkspaces = await this.userWorkspaceRepository.findByWorkspaceId(workspace_id);

        return userWorkspaces.map((uw) => ({
            user_email: uw.user.user_email,
            user_id: uw.user.user_id,
            role_name: uw.role.role_name,
        }));
    }

    public async findOneWorkspaceByUserId(user_id: number): Promise<number | undefined> {
        return (await this.userWorkspaceRepository.findOneByUserId(user_id))?.workspace_id;
    }

    public async countAllUsers(): Promise<number> {
        return this.userRepository.countAllUsers();
    }

    public async addContact(contact: Contact): Promise<void> {
        await this.sendGridAdapter.addContact(contact);
    }

    public async remove(workspace_id: number, user_ids: number[]): Promise<void> {
        await this.userWorkspaceRepository.deleteByWorkspaceIdUserIds(workspace_id, user_ids);
    }

    public async updateWorkspaceRole(workspace_id: number, user_id: number, role_name: WorkspaceUserRole): Promise<void> {
        const new_role = await this.roleRepository.findRoleByName(role_name);
        await this.userWorkspaceRepository.updateRoleByWorkspaceAndUserId(workspace_id, user_id, new_role.role_id);
    }
}
