import { WorkspaceUserRole } from "../role/role.model";

export interface UserWithRole {
    user_id: number;
    user_email: string;
    role_name: string;
}

export type UsersRemoveBody = {
    user_ids: number[];
};
export interface UpdateRoleRequest {
    role_name: WorkspaceUserRole;
}
