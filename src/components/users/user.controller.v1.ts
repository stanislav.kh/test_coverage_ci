import { ApiResponse, ForbiddenError } from "syntropy-common-ts";
import { Controller, Get, OperationId, Patch, Route, Security, SuccessResponse, Tags, Request, Body, Post } from "tsoa";
import Container from "typedi";
import { IdNumber } from "../../common/contract";
import { AuthorizedRequest } from "../../common/types";
import { getEnv } from "../../config/env";
import { UserSelfConflictError } from "../../errors/user-selfconflict-error";
import { ContactRequest, UserDataResponse } from "../auth/auth.contracts";
import { RedisService } from "../redis/redis.service";
import { UserSettingsObject } from "../users/user.model";
import { UserService } from "../users/user.service";
import { GetUserInvitationResponse } from "../workspaces-invitations/workspace-invitation.contract";
import { WorkspaceInvitationService } from "../workspaces-invitations/workspace-invitation.service";
import { UpdateRoleRequest, UsersRemoveBody, UserWithRole } from "./user.contracts";

@Route("api/v1/network/auth")
@Tags("Users")
export class UserControllerV1 extends Controller {
    private userService: UserService = Container.get(UserService);
    private redisService: RedisService = Container.get(RedisService);
    private workspaceInvitationService: WorkspaceInvitationService = Container.get(WorkspaceInvitationService);

    /**
     * Returns authorized User data. This is recommended way to Get the latest user's information.
     * @summary Get User info
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @SuccessResponse(200, "OK")
    @OperationId("V1NetworkAuthUserGet")
    @Get("user")
    public async GetUser(@Request() { authData, token }: AuthorizedRequest): Promise<ApiResponse<UserDataResponse>> {
        this.setHeader("Set-Cookie", [`token=${token}; ${getEnv().JWT_COOKIES_OPTIONS}`]);

        const {
            enable_rules,
            auth_sources,
            network_disable_sdn_connections,
            show_onboarding,
            show_registration_form,
            two_factors_authentication,
            user_timezone,
            theme,
        } = authData.user_settings;
        return {
            data: {
                user_id: authData.user_id,
                user_email: authData.user_email,
                user_scopes: authData.user_scopes,
                user_settings: {
                    enable_rules,
                    auth_sources,
                    network_disable_sdn_connections,
                    show_onboarding,
                    show_registration_form,
                    two_factors_authentication,
                    user_timezone,
                    theme,
                },
            },
        };
    }

    /**
     * Updates User settings.
     * @summary Update User settings
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @SuccessResponse(204, "No Content")
    @OperationId("V1NetworkAuthSettingsUpdate")
    @Patch("user/settings")
    public async UpdateUserSettings(@Body() body: UserSettingsObject, @Request() request: AuthorizedRequest): Promise<void> {
        await this.userService.updateSettings(request.authData.user_id, body);
        this.redisService.deleteMatchingKeys(`*SELECT "User"*PARAMETERS: ?${request.authData.user_id}*`);
    }

    /**
     * Updates User contact information.
     * @summary Update User contacts
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @SuccessResponse(204, "No Content")
    @OperationId("V1NetworkAuthContactUpdate")
    @Patch("user/contacts")
    public async UpdateContact(@Body() body: ContactRequest, @Request() request: AuthorizedRequest): Promise<void> {
        await this.userService.addContact({
            email: request.authData.user_email,
            ...body,
        });
    }

    /**
     * Gets invitations for authenticated user.
     * @summary Get invitations for authenticated user
     */
    @OperationId("V1GetUserInvitations")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @SuccessResponse(200, "OK")
    @Get("user/invitations")
    public async GetUserInvitations(@Request() request: AuthorizedRequest): Promise<ApiResponse<GetUserInvitationResponse[]>> {
        return { data: await this.workspaceInvitationService.getAllByUserId(request.authData.user_id) };
    }

    /**
     * Updates User role in the workspace.
     * @summary Update User role in the workspace
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN"])
    @SuccessResponse(204, "No Content")
    @OperationId("V1UpdateRole")
    @Patch("users/{user_id}/role")
    public async UpdateRole(user_id: IdNumber, @Body() body: UpdateRoleRequest, @Request() request: AuthorizedRequest): Promise<void> {
        if (!request.authData.workspace_id) {
            throw new ForbiddenError();
        }
        if (user_id === request.authData.user_id) {
            throw new UserSelfConflictError(request.authData.user_id);
        }
        await this.userService.updateWorkspaceRole(request.authData.workspace_id, user_id, body.role_name);
    }

    /**
     * Gets users.
     * @summary Get users
     */
    @OperationId("V1GetUsers")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @SuccessResponse(200, "OK")
    @Get("users")
    public async GetUsers(@Request() request: AuthorizedRequest): Promise<ApiResponse<UserWithRole[]>> {
        if (!request.authData.workspace_id) {
            throw new ForbiddenError();
        }
        const users = await this.userService.findUsersByWorkspaceId(request.authData.workspace_id);

        return { data: users };
    }

    /**
     * Removes users from workspace.
     * @summary Remove users from workspace
     */
    @OperationId("V1RemoveUsers")
    @SuccessResponse(204, "No Content")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN"])
    @Post("users/remove")
    public async RemoveUsers(@Body() body: UsersRemoveBody, @Request() request: AuthorizedRequest): Promise<void> {
        if (!request.authData.workspace_id) {
            throw new ForbiddenError();
        }
        if (body.user_ids.find((u) => u === request.authData.user_id)) {
            throw new UserSelfConflictError(request.authData.user_id);
        }
        await this.userService.remove(request.authData.workspace_id, body.user_ids);
    }
}
