import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToMany,
    OneToMany,
    JoinTable,
    OneToOne,
    JoinColumn,
    ManyToOne,
} from "typeorm";
import { IsEmail, IsOptional, IsNumber, IsInt } from "class-validator";
import { Model } from "syntropy-common-ts";

import { Permission } from "../permission/permission.model";
import { Role } from "../role/role.model";
import { AuthSource } from "../azure-auth/azure-auth.service";
import { UserWorkspace } from "../users-workspaces/user-workspace.model";
import { Workspace } from "../workspace/workspace.model";

interface UserObject {
    user_email?: string | null;
    user_password?: string;
}

export enum UserTheme {
    Default = "default",
    Dark = "dark",
}

export interface UserSettingsObject {
    show_onboarding?: boolean;
    show_registration_form?: boolean;
    user_timezone?: string;
    auth_sources?: AuthSource[];
    network_disable_sdn_connections?: boolean;
    two_factors_authentication?: boolean;
    enable_rules?: boolean;
    theme?: UserTheme;
}

@Entity("users")
export class User extends Model<UserObject> implements UserObject {
    constructor(object?: UserObject) {
        super(object);
    }

    @PrimaryGeneratedColumn()
    @IsNumber()
    @IsOptional()
    public user_id!: number;

    @Column()
    @IsEmail()
    public user_email!: string;

    @Column()
    @IsOptional()
    public user_password!: string;

    @CreateDateColumn({ type: "timestamptz" })
    public user_created_at!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    public user_updated_at!: Date;

    @Column({ type: "jsonb", nullable: false, default: {} })
    public user_settings!: UserSettingsObject;

    @Column()
    @IsInt()
    @IsOptional()
    public workspace_default_id?: number;

    // --------------------------------------------------
    // Relations
    // --------------------------------------------------

    @ManyToMany((_) => Permission)
    @JoinTable({
        name: "permissions_users",
        joinColumn: { name: "user_id", referencedColumnName: "user_id" },
        inverseJoinColumn: {
            name: "permission_id",
            referencedColumnName: "permission_id",
        },
    })
    public permissions?: Permission[];

    @OneToMany(() => UserWorkspace, (uw) => uw.user)
    public user_workspaces!: UserWorkspace[];

    @OneToOne((_) => Role)
    @JoinColumn({ referencedColumnName: "role_id", name: "role_id" })
    public role?: Role;

    @ManyToOne(() => Workspace, (w) => w.default_workspace_users)
    @JoinColumn({ referencedColumnName: "workspace_id", name: "workspace_default_id" })
    public default_workspace!: Workspace;
}
