import { getCustomRepository } from "typeorm";
import { Service } from "typedi";

import { RoleName } from "../role/role.model";
import { PermissionRepository } from "./permission.repository";
import { PermissionObject } from "./permission.model";

@Service()
export class PermissionService {
    private permissionRepository: PermissionRepository = getCustomRepository(PermissionRepository);

    public async roleAvailablePermissionsList(role: RoleName): Promise<PermissionObject[]> {
        const permissions = await this.permissionRepository.findRolePermissionsList(role);
        return permissions.map(({ permission_id, permission_name, permission_description }) => ({
            permission_id,
            permission_name,
            permission_description,
        }));
    }
}
