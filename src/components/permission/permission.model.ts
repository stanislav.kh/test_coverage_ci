import { Column, Entity, PrimaryColumn, ManyToMany, JoinTable } from "typeorm";
import { IsString, IsOptional } from "class-validator";
import { Model } from "syntropy-common-ts";

import { User } from "../users/user.model";
import { Role } from "../role/role.model";

export interface PermissionObject {
    permission_id: number;
    permission_name: string;
    permission_description: string;
}

@Entity("permissions")
export class Permission extends Model<PermissionObject> implements PermissionObject {
    constructor(object?: PermissionObject) {
        super(object);
    }

    @PrimaryColumn()
    public permission_id!: number;

    @Column()
    @IsString()
    @IsOptional()
    public permission_name!: string;

    @Column()
    @IsString()
    @IsOptional()
    public permission_description!: string;

    // --------------------------------------------------
    // Relations
    // --------------------------------------------------

    @ManyToMany((_) => User)
    @JoinTable({
        name: "permissions_users",
        joinColumn: { name: "permission_id", referencedColumnName: "permission_id" },
        inverseJoinColumn: {
            name: "user_id",
            referencedColumnName: "user_id",
        },
    })
    public users?: User[];

    @ManyToMany((_) => Role)
    @JoinTable({
        name: "permissions_roles",
        joinColumn: { name: "permission_id", referencedColumnName: "permission_id" },
        inverseJoinColumn: {
            name: "role_id",
            referencedColumnName: "role_id",
        },
    })
    public roles?: Role[];
}
