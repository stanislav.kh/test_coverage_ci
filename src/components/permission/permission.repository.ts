import { EntityRepository, EntityManager } from "typeorm";

import { Permission } from "./permission.model";
import { Role, RoleName } from "../role/role.model";

@EntityRepository()
export class PermissionRepository {
    constructor(public manager: EntityManager) {}

    public async findRolePermissions(permissions: string[], role_name: RoleName): Promise<Permission[]> {
        if (!permissions.length) {
            return [];
        }
        // Filter permissions to valid existing permissions. role.permissions is a subset of permissions.
        const role = await this.manager
            .createQueryBuilder(Role, "roles")
            .leftJoinAndSelect("roles.permissions", "permissions")
            .where("roles.role_name = :role_name AND permissions.permission_name IN (:...permissions)", {
                role_name,
                permissions,
            })
            .getOne();
        if (role == null) {
            return [];
        }
        return role.permissions!;
    }

    public async findRolePermissionsList(role_name: RoleName): Promise<Permission[]> {
        const role = await this.manager
            .createQueryBuilder(Role, "roles")
            .leftJoinAndSelect("roles.permissions", "permissions")
            .where("roles.role_name = :role_name", {
                role_name,
            })
            .getOneOrFail();
        return role.permissions!;
    }
}
