import { EntityRepository, EntityManager } from "typeorm";
import { RoleName, Role } from "./role.model";

@EntityRepository()
export class RoleRepository {
    constructor(public manager: EntityManager) {}

    public async findRoleByName(name: RoleName): Promise<Role> {
        return this.manager.findOneOrFail(Role, { where: { role_name: String(name) } });
    }
}
