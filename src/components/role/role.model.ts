import { Column, Entity, PrimaryColumn, OneToMany, ManyToMany, JoinTable } from "typeorm";
import { IsString, IsOptional } from "class-validator";
import { Model } from "syntropy-common-ts";

import { User } from "../users/user.model";
import { Permission } from "../permission/permission.model";
import { UserWorkspace } from "../users-workspaces/user-workspace.model";
import { WorkspaceInvitation } from "../workspaces-invitations/workspace-invitation.model";

export enum RoleName {
    /**
     * The same as USER, but with ability to bring (invite) other users into organisation or promote/demote admins.
     */
    Admin = "ADMIN",
    /**
     * Default platform user capabilities.
     */
    User = "USER",
    /**
     * This is basically a "god" mode for developers. Should NEVER be given to regular users.
     */
    GlobalAdmin = "GLOBAL_ADMIN",
    /**
     * This role serves two purposes:
     * - to list all possible access token roles;
     * - to whitelist access token permissions when user is creating new access token.
     * There is no advantage to give this role to access token. In normal scenarios permissions from this role will be cherry-picked and
     * assigned to access token.
     */
    AccessToken = "ACCESS_TOKEN",
}

export type WorkspaceUserRole = RoleName.Admin | RoleName.User;

export interface RoleObject {
    role_id: number;
    role_name: string;
}

@Entity("roles")
export class Role extends Model<RoleObject> implements RoleObject {
    constructor(object?: RoleObject) {
        super(object);
    }

    @PrimaryColumn()
    public role_id!: number;

    @Column()
    @IsString()
    @IsOptional()
    public role_name!: string;

    // --------------------------------------------------
    // Relations
    // --------------------------------------------------

    @OneToMany((_) => User, (n) => n.role)
    public users?: User[];

    @OneToMany((_) => UserWorkspace, (n) => n.role)
    public user_workspaces?: UserWorkspace[];

    @OneToMany((_) => WorkspaceInvitation, (n) => n.role)
    public workspace_invitations?: WorkspaceInvitation[];

    @ManyToMany((_) => Permission)
    @JoinTable({
        name: "permissions_roles",
        joinColumn: { name: "role_id", referencedColumnName: "role_id" },
        inverseJoinColumn: {
            name: "permission_id",
            referencedColumnName: "permission_id",
        },
    })
    public permissions?: Permission[];
}
