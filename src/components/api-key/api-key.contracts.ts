import { TsoaPick } from "../../helpers/tsoa.contract";
import { ApiKey } from "./api-key.model";

export interface ApiKeyCreateRequest {
    /**
     * @minLength 1 API key name is too short.
     * @maxLength 255 API key name is too long.
     */
    api_key_name: string;
    /**
     * @maxLength 255 API key description is too long.
     */
    api_key_description?: string;
    api_key_valid_until?: string | Date;
    api_key_allowed_tag_names?: string[];
}

export interface ApiKeyCreateObject extends ApiKeyCreateRequest {
    api_key_secret: string;
    user_id: number;
    workspace_id: number;
}
export interface ApiKeyObject extends ApiKeyCreateObject {
    api_key_id: number;
    api_key_created_at: Date | string;
    api_key_updated_at: Date | string;
}

export interface ApiKeyDto {
    api_key_name: string;
    api_key_description?: string;
    api_key_valid_until?: string | Date;
    api_key_status: boolean;
    api_key_id: number;
    api_key_created_at: Date | string;
    api_key_updated_at: Date | string;
}

export interface Filter {
    [key: string]: string | string[];
}

/**
 * @isInt api_key_id must be integer
 * @example 103
 */
export type ApiKeyId = number;

export type ApiKeyFindAndCountObject = TsoaPick<
    ApiKey,
    | "api_key_id"
    | "api_key_created_at"
    | "api_key_description"
    | "api_key_name"
    | "api_key_valid_until"
    | ("api_key_updated_at" & {
          api_key_status: boolean;
      })
>;

/**
 * @pattern ^(access_token_expiration|access_token_name):(ASC|DESC)$
 */
export type ApiKeyOrder = string;
