import { CustomValidationError, GenericHelpers, NotFoundError, QueryOpts } from "syntropy-common-ts";
import Container, { Service } from "typedi";
import { getCustomRepository } from "typeorm";
import * as moment from "moment";
import * as crypto from "crypto";

import { ApiKeyCreateRequest, ApiKeyFindAndCountObject, Filter } from "./api-key.contracts";
import { ApiKey } from "./api-key.model";
import { ApiKeyRepository } from "./api-key.repository";
import { ControllerClient } from "../../clients/controller/controller.client";

@Service()
export class ApiKeyService {
    private apiKeyRepository: ApiKeyRepository = getCustomRepository(ApiKeyRepository);
    private controllerClient: ControllerClient = Container.get(ControllerClient);

    public async save(userId: number, workspace_id: number, data: ApiKeyCreateRequest): Promise<ApiKey> {
        const secret = GenericHelpers.getRandomString(32);
        const hashedSecret = crypto.createHash("sha256").update(secret, "utf8").digest("hex");

        // Check for hash collisions
        const existingApiKey = await this.apiKeyRepository.findOneBySecret(hashedSecret);
        if (existingApiKey) {
            return this.save(userId, workspace_id, data);
        }

        const apiKey = new ApiKey({
            api_key_secret: hashedSecret,
            user_id: userId,
            api_key_name: data.api_key_name,
            api_key_description: data.api_key_description,
            api_key_valid_until: moment(data.api_key_valid_until)
                .utcOffset(0)
                .set({ hour: 23, minute: 59, second: 0, millisecond: 0 })
                .toISOString(),
            api_key_allowed_tag_names: data.api_key_allowed_tag_names ? data.api_key_allowed_tag_names : [],
            workspace_id,
        });

        await GenericHelpers.validateConstraints(ApiKey, apiKey);
        const newApiKey = await this.apiKeyRepository.save(apiKey);
        newApiKey.api_key_secret = secret;
        return newApiKey;
    }

    public async findAndCount(userId: number, filter: Filter, queryOpts: QueryOpts): Promise<[ApiKeyFindAndCountObject[], number]> {
        return await this.apiKeyRepository.findAndCount(userId, filter, queryOpts);
    }

    public async findOneByUserIdApiKeyId(userId: number, apiKeyId: number): Promise<ApiKey | undefined> {
        return await this.apiKeyRepository.findOneByUserIdApiKeyId(userId, apiKeyId);
    }

    public async remove(apiKeyId: number, userId: number): Promise<void> {
        const apiKey = await this.findOneByUserIdApiKeyId(userId, apiKeyId);

        if (!apiKey) {
            throw new NotFoundError("AGENT_TOKEN_NOT_FOUND", "Agent token was not found.", "api_key_id");
        }

        const agents = await this.controllerClient.findAgentsByApiKey(apiKey.api_key_id);
        if (agents.length > 0) {
            const ids = agents.map((a) => a.agent_id);
            throw new CustomValidationError("AGENT_TOKEN_IS_BEING_USED", `Agent token is being used by these agents: [${ids.join(",")}]`);
        }

        await this.apiKeyRepository.remove(apiKey);
    }
}
