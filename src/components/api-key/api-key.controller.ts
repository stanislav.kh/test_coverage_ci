import {
    Body,
    Controller,
    Delete,
    Deprecated,
    Get,
    OperationId,
    Post,
    Query,
    Request,
    Response,
    Route,
    Security,
    SuccessResponse,
    Tags,
} from "tsoa";
import Container from "typedi";
import { ApiKeyCreateRequest, ApiKeyFindAndCountObject, ApiKeyId, ApiKeyObject } from "./api-key.contracts";
import { ApiKeyService } from "../api-key/api-key.service";
import { ControllerHelpers, ForbiddenError } from "syntropy-common-ts";
import { AuthorizedRequest } from "../../common/types";

interface ApiResponse<TData> {
    data?: TData;
}

@Tags("API Keys")
@Route("auth/api-key")
export class ApiKeyController extends Controller {
    private apiKeyService: ApiKeyService = Container.get(ApiKeyService);

    /**
     * Create API key.
     * @summary Create API key
     */
    @OperationId("CreateApiKey")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER", "platform.api-keys.write"])
    @Response(400, "Bad Request")
    @Post()
    public async createApiKey(
        @Body() body: ApiKeyCreateRequest,
        @Request() request: AuthorizedRequest
    ): Promise<ApiResponse<ApiKeyObject>> {
        if (!request.authData.workspace_id) {
            throw new ForbiddenError();
        }
        return { data: await this.apiKeyService.save(request.authData.user_id, request.authData.workspace_id, body) };
    }

    /**
     * Get API keys.
     * @summary Get API keys
     */
    @OperationId("GetApiKey")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER", "platform.api-keys.read"])
    @Get()
    public async indexApiKey(
        @Request() request: AuthorizedRequest,
        @Query() skip?: number,
        @Query() take?: number,
        /**
         * string: "ASC" | "DESC"
         */
        @Query("order") serializedOrder?: string,
        /**
         * api_key_id: string, api_key_name: string
         */
        @Query("filter") serializedWhere?: string
    ): Promise<ApiResponse<ApiKeyFindAndCountObject[]>> {
        const filter = ControllerHelpers.deserializeWhere(serializedWhere || "", ["api_key_id", "api_key_name"]);

        const queryOpts = ControllerHelpers.queryOpts(skip, take, serializedOrder);
        const [apiKeys, count] = await this.apiKeyService.findAndCount(request.authData.user_id, filter, queryOpts);

        ControllerHelpers.setPaginationHeaders(count, apiKeys.length, queryOpts.skip, this);

        return { data: apiKeys };
    }

    /**
     * Delete API key.
     * @summary Delete API keys
     */
    @OperationId("DeleteApiKey")
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER", "platform.api-keys.write"])
    @Response(404, "Not Found")
    @SuccessResponse(204, "No Content")
    @Delete("{api_key_id}")
    public async deleteApiKey(api_key_id: ApiKeyId, @Request() request: AuthorizedRequest): Promise<void> {
        await this.apiKeyService.remove(api_key_id, request.authData.user_id);
    }
}
