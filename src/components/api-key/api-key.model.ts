import { Column, Entity, PrimaryGeneratedColumn, UpdateDateColumn, CreateDateColumn, OneToOne, JoinColumn } from "typeorm";
import { IsString, IsOptional, IsNumber, IsDateString, MinLength, MaxLength, IsArray } from "class-validator";
import { User } from "../users/user.model";
import { IsNotBlank, Model } from "syntropy-common-ts";
import { ApiKeyCreateObject } from "./api-key.contracts";
import { DoesNotHaveSingleQuote } from "../../validators/does-not-have-single-quote";

@Entity("api_keys")
export class ApiKey extends Model<ApiKeyCreateObject> {
    constructor(object?: ApiKeyCreateObject) {
        super(object);
    }

    @PrimaryGeneratedColumn()
    public api_key_id!: number;

    @Column()
    @IsNumber()
    public user_id!: number;

    @Column()
    @IsNumber()
    public workspace_id!: number;

    @Column()
    @IsString()
    @MinLength(1, { message: "Agent token name must be at least one character long." })
    @MaxLength(36, { message: "Agent token name maximum length is 36 characters." })
    @IsNotBlank({ message: "Agent token name must not be blank." })
    public api_key_name!: string;

    @Column({ nullable: true })
    @IsString()
    @IsOptional()
    public api_key_description?: string;

    @Column()
    @IsString()
    public api_key_secret!: string;

    @Column({ type: "json", default: [] })
    @IsOptional()
    @IsArray()
    @IsNotBlank({ message: "Tag name must not be blank.", each: true })
    @DoesNotHaveSingleQuote({ message: "Tag name can not contain single quote symbol.", each: true })
    @MinLength(3, { message: "Tag name must be at least 3 characters long.", each: true })
    @MaxLength(32, { message: "Tag name maximum length is 32 characters.", each: true })
    public api_key_allowed_tag_names!: string[];

    @Column({ nullable: true })
    @IsOptional()
    @IsDateString({ message: "Date must be a valid ISO date." })
    public api_key_valid_until?: string;

    @CreateDateColumn({ type: "timestamptz" })
    public api_key_created_at!: Date;

    @UpdateDateColumn({ type: "timestamptz" })
    public api_key_updated_at!: Date;

    // --------------------------------------------------
    // Relations
    // --------------------------------------------------

    @OneToOne((_) => User)
    @JoinColumn({ referencedColumnName: "user_id", name: "user_id" })
    public user?: User;
}
