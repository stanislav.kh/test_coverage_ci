import { ControllerClient } from "./../../clients/controller/controller.client";
import { RedisService } from "./../redis/redis.service";
import { ApiKeyRepositoryMock, RedisServiceMock, getRepoMockImplementation } from "./../../tests/mocks";
import { ObjectLiteral } from "typeorm";
import * as request from "supertest";
import { registerPlatformAdapter } from "../../register-platform-adapter";
import Container from "typedi";
import { MockHelpers } from "../../helpers/mock-helpers";

jest.mock("typeorm", () => ({
    getCustomRepository: jest.fn().mockImplementation(getRepoMockImplementation),
}));
const app = registerPlatformAdapter().$app;

describe("auth", () => {
    const headers: ObjectLiteral = { Accept: "application/json" };
    const authData = {
        user_email: "user_email",
        user_id: 1,
        user_owner_id: 1,
        user_scopes: ["user_scopes"],
        user_settings: {},
        azure_user_id: "1",
        workspace_id: 1,
    };

    beforeAll(async () => {
        Container.set(RedisService, new RedisServiceMock());
        RedisServiceMock.prototype.deleteMatchingKeys = jest.fn().mockResolvedValueOnce(null);
    });

    it("should create api key", async () => {
        ApiKeyRepositoryMock.prototype.save = jest.fn().mockResolvedValueOnce({});
        ApiKeyRepositoryMock.prototype.findOneBySecret = jest.fn().mockResolvedValueOnce(null);
        MockHelpers.mockAuthentification(authData);
        const retrieveResponse = await request(app).post("/auth/api-key").set(headers).send({
            api_key_name: "api_key_name",
            api_key_valid_until: new Date(),
        });

        expect(retrieveResponse.status).toBe(200);
        expect(ApiKeyRepositoryMock.prototype.save).toBeCalled();
    });

    it("should create api key even with hash colission", async () => {
        ApiKeyRepositoryMock.prototype.save = jest.fn().mockResolvedValueOnce({});
        ApiKeyRepositoryMock.prototype.findOneBySecret = jest.fn().mockResolvedValueOnce({});
        MockHelpers.mockAuthentification(authData);
        const retrieveResponse = await request(app).post("/auth/api-key").set(headers).send({
            api_key_name: "api_key_name",
            api_key_valid_until: new Date(),
        });

        expect(retrieveResponse.status).toBe(200);
        expect(ApiKeyRepositoryMock.prototype.save).toBeCalledTimes(1);
        expect(ApiKeyRepositoryMock.prototype.findOneBySecret).toBeCalledTimes(2);
    });

    it("should get api keys", async () => {
        const data = [
            {
                api_key_name: "api_key_name_1",
                api_key_id: 1,
            },
            {
                api_key_name: "api_key_name_2",
                api_key_id: 2,
            },
        ];

        ApiKeyRepositoryMock.prototype.findAndCount = jest.fn().mockResolvedValueOnce([data, 2]);
        MockHelpers.mockAuthentification(authData);
        const retrieveResponse = await request(app).get("/auth/api-key").set(headers).send();

        expect(retrieveResponse.status).toBe(206);
        expect(retrieveResponse.body).toEqual({ data });
        expect(ApiKeyRepositoryMock.prototype.save).toBeCalled();
    });

    it("should delete api key", async () => {
        const data = {};
        ApiKeyRepositoryMock.prototype.remove = jest.fn().mockResolvedValueOnce(null);
        ApiKeyRepositoryMock.prototype.findOneByUserIdApiKeyId = jest.fn().mockResolvedValueOnce({
            api_key_name: "api_key_name",
            api_key_secret: "123",
            user_id: authData.user_id,
        });
        ControllerClient.prototype.findAgentsByApiKey = jest.fn().mockResolvedValueOnce([]);

        MockHelpers.mockAuthentification(authData);

        const retrieveResponse = await request(app).delete("/auth/api-key/1").set(headers).send(data);

        expect(retrieveResponse.status).toBe(204);
        expect(ApiKeyRepositoryMock.prototype.findOneByUserIdApiKeyId).toBeCalled();
        expect(ControllerClient.prototype.findAgentsByApiKey).toBeCalled();
        expect(ApiKeyRepositoryMock.prototype.remove).toBeCalled();
    });

    it("should fail to delete api key if id validation fails", async () => {
        MockHelpers.mockAuthentification(authData);
        const retrieveResponse = await request(app).delete("/auth/api-key/:whatever").set(headers).send({});
        expect(retrieveResponse.status).toBe(400);
    });

    it("should fail to delete api key if key is used by agents", async () => {
        const data = {};
        ApiKeyRepositoryMock.prototype.remove = jest.fn().mockResolvedValueOnce(null);
        ApiKeyRepositoryMock.prototype.findOneByUserIdApiKeyId = jest.fn().mockResolvedValueOnce({
            api_key_name: "api_key_name",
            api_key_secret: "123",
            user_id: authData.user_id,
        });
        ControllerClient.prototype.findAgentsByApiKey = jest.fn().mockResolvedValueOnce([
            {
                agent_id: 1,
            },
        ]);

        MockHelpers.mockAuthentification(authData);

        const retrieveResponse = await request(app).delete("/auth/api-key/1").set(headers).send(data);

        expect(retrieveResponse.status).toBe(400);
        expect(ApiKeyRepositoryMock.prototype.findOneByUserIdApiKeyId).toBeCalled();
        expect(ControllerClient.prototype.findAgentsByApiKey).toBeCalled();
    });
});
