import { ApiResponse, ControllerHelpers, ForbiddenError } from "syntropy-common-ts";
import { Route, Controller, Security, Tags, SuccessResponse, OperationId, Get, Query, Post, Body, Request, Delete } from "tsoa";
import Container from "typedi";
import { FilterParamV1 } from "../../common/contract";
import { AuthorizedRequest } from "../../common/types";
import { ApiKeyFindAndCountObject, ApiKeyCreateRequest, ApiKeyObject, ApiKeyId, ApiKeyOrder } from "./api-key.contracts";
import { ApiKeyService } from "./api-key.service";

@Route("api/v1/network/auth")
export class ApiKeyControllerV1 extends Controller {
    private apiKeyService: ApiKeyService = Container.get(ApiKeyService);

    /**
     * Gets API keys.
     * @summary Get API keys
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER", "platform.api-keys.read"])
    @Tags("API Keys")
    @SuccessResponse(206, "Partial Content")
    @OperationId("V1NetworkAuthApiKeysGet")
    @Get("api-keys")
    public async GetApiKeys(
        @Request() request: AuthorizedRequest,
        @Query() skip?: number,
        @Query() take?: number,
        /**
         * string: "ASC" | "DESC"
         */
        @Query("order") order?: ApiKeyOrder,
        /**
         * array of api key ids
         */
        @Query("filter") filter?: FilterParamV1
    ): Promise<ApiResponse<ApiKeyFindAndCountObject[]>> {
        const queryOpts = ControllerHelpers.queryOpts(skip, take, order);
        const [apiKeys, count] = await this.apiKeyService.findAndCount(
            request.authData.user_id,
            { ...(filter ? { ["api_key_id"]: filter.split(",") } : undefined) },
            queryOpts
        );

        ControllerHelpers.setPaginationHeaders(count, apiKeys.length, queryOpts.skip, this);
        return { data: apiKeys };
    }

    /**
     * Creates API key.
     * @summary Create API key
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER", "platform.api-keys.write"])
    @SuccessResponse(200, "OK")
    @Tags("API Keys")
    @OperationId("V1NetworkAuthApiKeysCreate")
    @Post("api-keys")
    public async CreateApiKey(
        @Body() body: ApiKeyCreateRequest,
        @Request() request: AuthorizedRequest
    ): Promise<ApiResponse<ApiKeyObject>> {
        if (!request.authData.workspace_id) {
            throw new ForbiddenError();
        }
        return { data: await this.apiKeyService.save(request.authData.user_id, request.authData.workspace_id, body) };
    }

    /**
     * Deletes API key.
     * @summary Delete API key
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER", "platform.api-keys.write"])
    @Tags("API Keys")
    @SuccessResponse(204, "No Content")
    @OperationId("V1NetworkAuthApiKeysDelete")
    @Delete("api-keys/{api_key_id}")
    public async DeleteApiKey(@Query("api_key_id") api_key_id: ApiKeyId, @Request() request: AuthorizedRequest): Promise<void> {
        await this.apiKeyService.remove(api_key_id, request.authData.user_id);
    }
}
