import { QueryOpts, RepositoryHelpers } from "syntropy-common-ts";
import { EntityRepository, EntityManager } from "typeorm";
import { ApiKeyFindAndCountObject, Filter } from "./api-key.contracts";
import { ApiKey } from "./api-key.model";

@EntityRepository()
export class ApiKeyRepository {
    constructor(private manager: EntityManager) {}

    public async save(apiKey: ApiKey): Promise<ApiKey> {
        return this.manager.save(ApiKey, apiKey);
    }

    public async findAndCount(userId: number, filter: Filter, queryOpts: QueryOpts): Promise<[ApiKeyFindAndCountObject[], number]> {
        const order = RepositoryHelpers.getMappedOrder(queryOpts.order, {
            api_key_valid_until: "api_keys.api_key_valid_until",
            api_key_name: "api_keys.api_key_name",
            api_key_status: "api_key_status",
        });

        const statusQuery = "(api_keys.api_key_valid_until > NOW()) as api_key_status";
        const query = this.manager
            .createQueryBuilder(ApiKey, "api_keys")
            .select([
                "api_key_name",
                "api_key_description",
                "api_key_valid_until",
                "api_key_id",
                "api_key_created_at",
                "api_key_updated_at",
                statusQuery,
            ])
            .where({ user_id: userId })
            .skip(queryOpts.skip)
            .take(queryOpts.take);

        if (order) {
            query.orderBy(order);
        }

        const countQuery = this.manager.createQueryBuilder(ApiKey, "api_keys").where({ user_id: userId });

        if (filter["api_key_name"]?.length > 0) {
            query.andWhere("LOWER(api_keys.api_key_name) LIKE LOWER(:name)", { name: `%${filter["api_key_name"]}%` });
        }

        if (filter["api_key_id"]?.length > 0) {
            query.andWhere("api_keys.api_key_id = :id", { id: filter["api_key_id"] });
        }

        const [apiKeys, count] = await Promise.all([query.getRawMany(), countQuery.getCount()]);
        return [apiKeys, count];
    }

    public async findOneByUserIdApiKeyId(userId: number, apiKeyId: number): Promise<ApiKey | undefined> {
        return await this.manager.findOne(ApiKey, { where: { user_id: userId, api_key_id: apiKeyId } });
    }

    public async findOneBySecret(secret: string): Promise<ApiKey | undefined> {
        return await this.manager.findOne(ApiKey, { where: { api_key_secret: secret } });
    }

    public async remove(apiKey: ApiKey): Promise<void> {
        await this.manager.remove(ApiKey, apiKey);
    }
}
