import { AuthSource } from "../azure-auth/azure-auth.service";
import { UserSettingsObject } from "../users/user.model";

export interface AuthData {
    user_id: number;
    user_email: string;
    user_settings: UserSettingsObject;
    user_scopes: string[];
    azure_user_id: string;
    workspace_id?: number;
}

export interface UserDataResponse {
    user_id: number;
    user_email: string;
    user_settings: UserSettingsObject;
    user_scopes: string[];
}

export interface AuthDto {
    data: {
        user_id: number;
        user_email: string;
        user_scopes: string[];
    };
    iat: number;
    exp: number;
}

interface IdObject {
    id: string;
}

interface AnyObject {
    [key: string]: unknown;
}

export type IdTokenPayload = IdObject & AnyObject;

export interface CorruptedUserData {
    id: string;
    email: string;
    authSource: AuthSource;
}

export interface AccessTokenData {
    access_token: string;
}

export interface AccessTokenWriteData {
    permissions: string[];
    access_token_expiration: Date;
    /**
     * @minLength 1 Access token name is too short.
     * @maxLength 255 Access token name is too long.
     */
    access_token_name?: string;
    /**
     * @maxLength 255 Access token description is too long.
     */
    access_token_description?: string;
}

export interface AccessTokenReadData {
    access_token_name?: string;
    access_token_expiration: string;
    access_token_description?: string;
    access_token_id: string;
}

export interface ContactRequest {
    /**
     * @isString name must be string
     * @minLength 1 name must be at least one symbols length
     * @maxLength 30 name must be less than 30 symbols length
     * @example "Jhon"
     */
    name: string;
    /**
     * @isString company must be string
     * @minLength 1 company must be at least one symbols length
     * @maxLength 30 company must be less than 30 symbols length
     * @example "Test Soft Inc."
     */
    company?: string;
    /**
     * @isString domain must be string
     * @minLength 1 domain must be at least one symbols length
     * @maxLength 30 domain must be less than 30 symbols length
     * @example "Software Engineering"
     */
    domain: string;
    /**
     * @isBool marketing email flag must be boolean
     * @example true
     */
    marketing_email_agreed: boolean;
}

export interface Contact extends ContactRequest {
    email: string;
}

/**
 * @pattern ^(access_token_expiration|access_token_name):(ASC|DESC)$ order must be access_token_expiration|access_token_name:ASC|DESC
 */
export type AccessTokenOrder = string;

export enum Subjects {
    MergeSocialAccount = "merge-social-account",
    CreateAzureUser = "create-azure-user",
    SendVerificationEmail = "send-verification-email",
}

export interface MergeSocialAccountData {
    userId: number;
    userSettings: UserSettingsObject;
    authSource: AuthSource;
    azureUserId: string;
    email: string;
}

export interface CreateAzureUserData {
    userId: number;
    authSource: AuthSource;
    azureUserId: string;
    email: string;
}

export interface SendVerificationEmailData {
    azureUserId: string;
    email: string;
}

// TODO: get rid of the events below by moving them to protobuf
export interface MergeSocialAccountEvent extends Event {
    subject: Subjects.MergeSocialAccount;
    data: MergeSocialAccountData;
}

export interface CreateAzureUserEvent extends Event {
    subject: Subjects.CreateAzureUser;
    data: CreateAzureUserData;
}

export interface SendVerificationEmailEvent extends Event {
    subject: Subjects.SendVerificationEmail;
    data: SendVerificationEmailData;
}

export interface Event {
    subject: Subjects;
    data: any;
}

export interface LoginRequest {
    user_email: string;
    user_password: string;
}

export interface AccessTokenLoginRequestV1 {
    access_token: string;
}

export interface CreateAccessTokenBody {
    permissions: string[];
    access_token_expiration: string;
    /**
     * @minLength 1 Access token name is too short.
     * @maxLength 255 Access token name is too long.
     */
    access_token_name?: string;
    /**
     * @maxLength 255 Access token description is too long.
     */
    access_token_description?: string;
}
