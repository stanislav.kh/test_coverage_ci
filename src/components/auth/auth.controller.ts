import { isUUID } from "class-validator";
import Container from "typedi";
import { ForbiddenError, CustomValidationError, ControllerHelpers } from "syntropy-common-ts";

import { UserService } from "./../users/user.service";
import { AuthorizedRequest } from "./../../common/types";
import {
    Route,
    Post,
    Body,
    Response,
    Tags,
    Security,
    Controller,
    Request,
    Get,
    OperationId,
    SuccessResponse,
    Put,
    Delete,
    Query,
    Hidden,
    Deprecated,
} from "tsoa";
import { AuthService } from "./auth.service";
import { User, UserSettingsObject } from "../users/user.model";
import { AzureUserTokenDto } from "../azure-auth/azure-auth.contracts";
import { getEnv } from "../../config/env";
import {
    ContactRequest,
    AccessTokenWriteData,
    AccessTokenData,
    AccessTokenReadData,
    UserDataResponse,
    AccessTokenOrder,
    LoginRequest,
} from "./auth.contracts";
import { RedisService } from "../redis/redis.service";
import { PermissionService } from "../permission/permission.service";
import { PermissionObject } from "../permission/permission.model";
import { RoleName } from "../role/role.model";
import { AzureService } from "../azure-auth/azure-auth.service";
import { UserWorkspace } from "../users-workspaces/user-workspace.model";

@Tags("Auth")
@Route("auth/authorization")
export class AuthController extends Controller {
    private authService: AuthService = Container.get(AuthService);
    private azureService: AzureService = Container.get(AzureService);
    private userService: UserService = Container.get(UserService);
    private redisService: RedisService = Container.get(RedisService);
    private permissionService: PermissionService = Container.get(PermissionService);

    /**
     * Verifies jwt token and returns entity data.
     */
    // TODO: allow only with api-key
    @Hidden()
    @Security("jwt")
    @Response(500, "Internal Server Error")
    @Response(404, "User not found by jwt token")
    @Response(200, "Success")
    @Get("")
    public async auth(@Request() request: AuthorizedRequest): Promise<string> {
        this.setHeader("X-Forwarded-User", request.authData.user_id.toString());
        this.setHeader("X-Forwarded-Permissions", request.authData.user_scopes.join(" "));
        return "OK";
    }

    /**
     * Logs in user using resource owner password credentials flow.
     * @summary External login
     */
    @Deprecated()
    @Hidden()
    @OperationId("AuthExternalLogin")
    @Response(500, "Internal Server Error")
    @Post("external/login")
    public async externalLogin(@Body() body: LoginRequest): Promise<AzureUserTokenDto> {
        return this.authService.login(body.user_email, body.user_password);
    }

    /**
     * Deletes session cookies.
     * @summary Logout
     */
    @Deprecated()
    @OperationId("AuthLogout")
    @Response(500, "Internal Server Error")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Post("logout")
    public async logout(): Promise<void> {
        // @ts-ignore
        this.setHeader("Set-Cookie", [
            `token=undefined; HttpOnly; ${getEnv().JWT_COOKIES_OPTIONS}`,
            `refreshToken=undefined; HttpOnly; ${getEnv().JWT_COOKIES_OPTIONS}`,
        ]);
        return;
    }

    /**
     * Verifies user's email address.
     * @summary Verify email
     */
    @Deprecated()
    @OperationId("AuthVerifyEmail")
    @SuccessResponse(204, "No Content")
    @Response(400, "Bad Request")
    @Response(404, "Not Found")
    @Response(500, "Internal Server Error")
    @Get("verify-email/{code}")
    public async verifyEmail(
        /**
         * Email verification code (received by mail).
         */
        code: string
    ): Promise<any> {
        await this.authService.verifyUser(code);
    }

    /**
     * Used by Azure as web hook after new user is registered.
     */
    @Hidden()
    @OperationId("AuthCreateUser")
    @Security("azure_api_key", ["AZURE_API_KEY"])
    @Response(400, "Bad Request")
    @Post("create-user")
    public async createUser(@Request() _: AuthorizedRequest, @Body() body: any): Promise<void> {
        await this.authService.createUser(body.objectId, body.email, body.authenticationSource);
    }

    /**
     * Retrieve JWT from access token.
     * @summary Login (access token)
     */
    @Deprecated()
    @OperationId("AuthAccessTokenLogin")
    @Response(500, "Internal Server Error")
    @Post("access-token/login")
    public async accessTokenLogin(@Body() body: AccessTokenData): Promise<AzureUserTokenDto> {
        const buffer = Buffer.from(body.access_token, "base64");
        const str = buffer.toString("utf-8");
        const [username, user_password] = str.split(":") as [string | undefined, string | undefined];
        if (!username?.length || !user_password?.length) {
            throw new ForbiddenError();
        }
        return this.authService.login(username, user_password);
    }

    /**
     * Create scopes access token.
     * @summary Create access token
     */
    @Deprecated()
    @OperationId("AuthAccessTokenUserCreate")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Post("access-token")
    public async createAccessToken(@Request() req: AuthorizedRequest, @Body() body: AccessTokenWriteData): Promise<AccessTokenData> {
        if (!req.authData.workspace_id) {
            throw new ForbiddenError();
        }
        const access_token = await this.authService.createAccessToken(
            req.authData.user_id,
            req.authData.workspace_id,
            body.permissions,
            body.access_token_expiration.toISOString(),
            body.access_token_name,
            body.access_token_description
        );
        return { access_token };
    }

    /**
     * Delete scopes access token.
     * @summary Delete access token
     */
    @Deprecated()
    @OperationId("AuthAccessTokenUserDelete")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Delete("access-token/{id}")
    public async deleteAccessToken(id: string, @Request() req: AuthorizedRequest): Promise<any> {
        if (!isUUID(id)) {
            throw new CustomValidationError("INVALID_ACCESS_TOKEN", "Access token is not valid UUID.");
        }
        if (!req.authData.workspace_id) {
            throw new ForbiddenError();
        }
        await this.authService.deleteAccessToken(req.authData.workspace_id, id);
    }

    /**
     * Entire list of available permissions access tokens can have.
     * @summary Get access token permissions
     */
    @Deprecated()
    @OperationId("AuthAccessTokenPermissionsList")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Get("permissions/access-token")
    public async listAccessTokenPermissions(): Promise<PermissionObject[]> {
        return this.permissionService.roleAvailablePermissionsList(RoleName.AccessToken);
    }

    /**
     * List access tokens.
     * @summary Get access tokens
     */
    @Deprecated()
    @OperationId("AuthAccessTokenList")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Get("access-token")
    public async indexAccessTokens(
        @Request() req: AuthorizedRequest,
        @Query() skip: number = 0,
        @Query() take: number = 10,
        @Query() order?: AccessTokenOrder
    ): Promise<AccessTokenReadData[]> {
        if (!req.authData.workspace_id) {
            throw new ForbiddenError();
        }
        const [accessTokens, count] = await this.authService.listAccessTokensAndCount(req.authData.workspace_id, skip, take, order);
        ControllerHelpers.setPaginationHeaders(count, take, skip, this);
        return accessTokens;
    }

    /**
     * Returns authorized user data. This is recommended way to get the latest user's information.
     * @summary Get user info
     */
    @Deprecated()
    @OperationId("AuthShowUser")
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Get("user")
    public async showUser(@Request() { authData, token }: AuthorizedRequest): Promise<UserDataResponse> {
        // @ts-ignore
        this.setHeader("Set-Cookie", [`token=${token}; ${getEnv().JWT_COOKIES_OPTIONS}`]);

        const {
            enable_rules,
            auth_sources,
            network_disable_sdn_connections,
            show_onboarding,
            show_registration_form,
            two_factors_authentication,
            user_timezone,
            theme,
        } = authData.user_settings;
        return {
            user_id: authData.user_id,
            user_email: authData.user_email,
            user_scopes: authData.user_scopes,
            user_settings: {
                enable_rules,
                auth_sources,
                network_disable_sdn_connections,
                show_onboarding,
                show_registration_form,
                two_factors_authentication,
                user_timezone,
                theme,
            },
        };
    }

    /**
     * Validates user captcha.
     */
    @Hidden()
    @OperationId("ValidateCaptcha")
    @Security("azure_api_key", ["AZURE_API_KEY"])
    @Response(400, "Bad Request")
    @Post("validate-captcha")
    public async validateCaptcha(@Body() body: { extension_captchaUserResponseToken: string }): Promise<void> {
        await this.authService.verifyCaptcha(body.extension_captchaUserResponseToken);
    }

    @Hidden()
    @OperationId("EnrichTokenWithExternalClaims")
    @Security("azure_api_key", ["AZURE_API_KEY"])
    @Response(400, "Bad Request")
    @Post("enrich-token")
    public async enrichTokenwithExternalClaims(
        @Body() body: { extension_internalUser?: string; objectId?: string; workspace_id: string | null }
    ): Promise<{ workspace_id: string; roles: string; permissions: string; scopes: string; email?: string }> {
        console.info("enrich-token", body);
        // Even though we do ask for internalUser property for some reason it is not available after sign up via social.
        // It later on becomes available in future sign ins though...
        // It is strange and unexpected behavior, so as workaroud we just query azure for internalUser id.
        const userId =
            (body.extension_internalUser != null && parseInt(body.extension_internalUser)) ||
            (body.objectId != null && (await this.azureService.getInternalUserId(body.objectId)));
        if (!userId) {
            throw new Error();
        }

        let workspace_id: number | undefined;
        let user_workspace: Required<Pick<UserWorkspace, "user_id" | "role">> | undefined;
        let roles = "";

        const user = await this.userService.findUserById<User & Required<Pick<User, "permissions">>>(userId, {
            relations: ["permissions"],
        });
        if (!user) {
            throw new Error();
        }

        if (body.workspace_id) {
            workspace_id = parseInt(body.workspace_id);
        } else {
            workspace_id = user.workspace_default_id;
        }

        // get user workspace from azure body or user default workspace id
        user_workspace = await this.authService.findUserClaimsByWorkspaceId(userId, workspace_id);
        if (!user_workspace) {
            // get any workspace user belongs to
            workspace_id = await this.userService.findOneWorkspaceByUserId(userId);
            user_workspace = await this.authService.findUserClaimsByWorkspaceId(userId, workspace_id);
        }

        if (user_workspace) {
            roles = user_workspace.role.role_name;
        }

        this.userService.saveWorkspaceDefaultId(user, workspace_id);

        const permissions = user.permissions.map((p) => p.permission_name).join(" ") || "";

        return {
            email: user.user_email,
            workspace_id: workspace_id?.toString() || "",
            roles,
            permissions,
            scopes: permissions.length ? `${roles} ${permissions}` : roles,
        };
    }

    /**
     * Update user settings.
     * @summary Update user settings
     */
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Response(404, "Not Found")
    @Put("settings")
    public async updateSettings(@Body() body: UserSettingsObject, @Request() request: AuthorizedRequest): Promise<any> {
        await this.userService.updateSettings(request.authData.user_id, body);
        this.redisService.deleteMatchingKeys(`*SELECT "User"*PARAMETERS: ?${request.authData.user_id}*`);
    }

    /**
     * Update contacts information.
     * @summary Update user contacts
     */
    @Deprecated()
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Response(400, "Bad Request")
    @Post("contact")
    public async addContact(@Body() body: ContactRequest, @Request() request: AuthorizedRequest): Promise<any> {
        await this.userService.addContact({
            email: request.authData.user_email,
            ...body,
        });
    }

    /**
     * Validates if user limit has been reached.
     * @summary Check CAPTCHA limit
     */
    @Hidden()
    @OperationId("ValidateCaptcha")
    @Security("azure_api_key", ["AZURE_API_KEY"])
    @Response(400, "Bad Request")
    @Post("validate-limit")
    public async validateUserLimit(): Promise<void> {
        await this.authService.validateUserLimit();
    }
}
