import { RedisService } from "./../redis/redis.service";
import { RedisServiceMock, UserRepositoryMock, getRepoMockImplementation } from "./../../tests/mocks";
import { AuthService } from "./auth.service";
import { AuthSource, AzureService } from "./../azure-auth/azure-auth.service";
import { ObjectLiteral } from "typeorm";
import * as request from "supertest";
import { registerPlatformAdapter } from "../../register-platform-adapter";
import Container from "typedi";
import { MockHelpers } from "../../helpers/mock-helpers";
import { CreateAzureUserPublisher, MergeSocialAccountPublisher, SendVerificationEmailPublisher } from "./auth.publishers";
import { UserService } from "../users/user.service";

jest.mock("typeorm", () => ({
    getCustomRepository: jest.fn().mockImplementation(getRepoMockImplementation),
    getManager: () => ({
        transaction: jest.fn(),
    }),
}));
const app = registerPlatformAdapter().$app;

// RETHINK THESE TESTS
describe("auth", () => {
    const headers: ObjectLiteral = { Accept: "application/json" };
    const authData = {
        user_email: "user_email",
        user_id: 1,
        user_scopes: ["user_scopes"],
        user_settings: {},
        azure_user_id: "1",
        workspace_id: 1,
    };

    beforeAll(async () => {
        Container.set(RedisService, new RedisServiceMock());
        RedisServiceMock.prototype.deleteMatchingKeys = jest.fn().mockResolvedValueOnce(null);
        process.env.SECRET = "SECRET";
        process.env.AZURE_API_KEY = "AZURE_API_KEY";
        process.env.JWT_TOKEN_DURATION = (7 * 24 * 60 * 60).toString();
    });

    it("should login", async () => {
        AzureService.prototype.getTokenForUser = jest.fn().mockResolvedValueOnce({
            access_token: "123",
            token_type: "123",
            expires_in: "123",
            refresh_token: "123",
        });

        const retrieveResponse = await request(app).post("/auth/authorization/external/login").set(headers).send({
            user_email: "admin@noia.network",
            user_password: "noia2020",
        });
        expect(retrieveResponse.status).toBe(200);
        expect(retrieveResponse.body.access_token).toBeDefined();
        expect(retrieveResponse.body.refresh_token).toBeDefined();
        expect(AzureService.prototype.getTokenForUser).toBeCalled();
    });

    it("should not access forbidden endpoint", async () => {
        const retrieveResponse = await request(app).post("/auth/authorization/logout").set(headers).send();
        expect(retrieveResponse.status).toBe(403);
    });

    it("should logout", async () => {
        AuthService.prototype.validateJwt = jest.fn().mockResolvedValueOnce({});
        const retrieveResponse = await request(app).post("/auth/authorization/logout").set(headers).send();
        expect(retrieveResponse.status).toBe(204);
        expect(retrieveResponse.header["set-cookie"]?.length).toEqual(2);
    });

    it("should verify user", async () => {
        AzureService.prototype.verifyUserEmail = jest.fn().mockResolvedValueOnce({});
        const token = Container.get(AuthService).createIdToken("user_id", { isEmailVerification: true });
        const retrieveResponse = await request(app)
            .get(`/auth/authorization/verify-email/${token}`)
            .set({ ...headers, "x-functions-key": "azure_api_key" })
            .send();
        expect(retrieveResponse.status).toBe(204);
        expect(AzureService.prototype.verifyUserEmail).toBeCalledWith("user_id");
    });

    it("should create user", async () => {
        UserService.prototype.create = jest.fn().mockResolvedValueOnce({ user_id: 1 });
        UserRepositoryMock.prototype.findUserByEmail = jest.fn().mockResolvedValueOnce(null);
        CreateAzureUserPublisher.prototype.publish = jest.fn().mockResolvedValueOnce(null);

        const retrieveResponse = await request(app)
            .post(`/auth/authorization/create-user`)
            .set({ ...headers, "x-functions-key": "AZURE_API_KEY" })
            .send({
                objectId: "objectId",
                email: "email",
                localAccountAuthentication: AuthSource.local,
            });
        expect(retrieveResponse.status).toBe(204);

        expect(UserRepositoryMock.prototype.findUserByEmail).toBeCalled();
        expect(UserService.prototype.create).toBeCalled();

        expect(CreateAzureUserPublisher.prototype.publish).toBeCalledWith({
            userId: 1,
            azureUserId: "objectId",
            authSource: "localAccountAuthentication",
            email: "email",
        });
    });

    it("should create user in azure", async () => {
        SendVerificationEmailPublisher.prototype.publish = jest.fn().mockResolvedValueOnce(null);
        AzureService.prototype.createUser = jest.fn().mockResolvedValueOnce({});

        await Container.get(AuthService).createAzureUser({
            userId: 1,
            authSource: AuthSource.local,
            azureUserId: "1",
            email: "email",
        });

        expect(AzureService.prototype.createUser).toBeCalledWith("1", 1, "email");
        expect(SendVerificationEmailPublisher.prototype.publish).toBeCalledWith({
            azureUserId: "1",
            email: "email",
        });
    });

    // TODO: replace with checking if a message is sent to notification service
    // it("should send verification email", async () => {
    //     mailService.send = jest.fn().mockResolvedValueOnce({});

    //     await Container.get(AuthService).sendVerificationEmail({
    //         azureUserId: "1",
    //         email: "email",
    //     });

    //     expect(mailService.send).toBeCalled();
    // });

    it("should create social user", async () => {
        UserRepositoryMock.prototype.findUserByEmail = jest.fn().mockResolvedValueOnce({ user_id: 1, user_settings: {} });
        MergeSocialAccountPublisher.prototype.publish = jest.fn().mockResolvedValueOnce(null);

        const retrieveResponse = await request(app)
            .post(`/auth/authorization/create-user`)
            .set({ ...headers, "x-functions-key": "AZURE_API_KEY" })
            .send({
                objectId: "objectId",
                email: "email",
                authenticationSource: AuthSource.social,
            });
        expect(retrieveResponse.status).toBe(204);

        expect(UserRepositoryMock.prototype.findUserByEmail).toBeCalled();
        expect(MergeSocialAccountPublisher.prototype.publish).toBeCalledWith({
            userId: 1,
            userSettings: {},
            azureUserId: "objectId",
            authSource: AuthSource.social,
            email: "email",
        });
    });

    it("should merge social account", async () => {
        UserRepositoryMock.prototype.updateSettings = jest.fn().mockResolvedValueOnce({});
        AzureService.prototype.transferMFASecretForUser = jest.fn().mockResolvedValueOnce({});
        CreateAzureUserPublisher.prototype.publish = jest.fn().mockResolvedValueOnce(null);

        await Container.get(AuthService).mergeSocialAccount({
            userId: 1,
            azureUserId: "objectId",
            authSource: AuthSource.social,
            email: "email",
            userSettings: {
                auth_sources: [AuthSource.local],
            },
        });

        expect(UserRepositoryMock.prototype.updateSettings).toBeCalledWith(1, { auth_sources: [AuthSource.local, AuthSource.social] });
        expect(AzureService.prototype.transferMFASecretForUser).toBeCalledWith(1, "objectId");
        expect(CreateAzureUserPublisher.prototype.publish).toBeCalledWith({
            userId: 1,
            azureUserId: "objectId",
            authSource: AuthSource.social,
            email: "email",
        });
    });

    describe("access-token", () => {
        it("should create", async () => {
            const access_token_expiration = new Date();
            const access_token_name = "access-token-name";
            const access_token_description = "access-token-description";
            const access_token_user_id = 2;

            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.createAccessToken = jest.fn().mockResolvedValueOnce({});
            AzureService.prototype.hasAccessTokenName = jest.fn().mockResolvedValueOnce(false);
            UserService.prototype.create = jest.fn().mockResolvedValueOnce({ user_id: access_token_user_id });

            const retrieveResponse = await request(app)
                .post(`/auth/authorization/access-token`)
                .set({ ...headers })
                .send({
                    permissions: ["access-token.read", "this-permission-does-not-exist"],
                    access_token_expiration: access_token_expiration.toISOString(),
                    access_token_name,
                    access_token_description,
                });
            expect(retrieveResponse.status).toBe(200);
            expect(AzureService.prototype.createAccessToken).toBeCalledWith(
                authData.user_id,
                authData.workspace_id,
                access_token_user_id,
                expect.any(String),
                expect.any(String),
                access_token_expiration.toISOString(),
                access_token_name,
                access_token_description
            );
        });

        it("duplicate token name", async () => {
            const access_token_expiration = new Date();
            const access_token_name = "access-token-name";
            const access_token_description = "access-token-description";
            const access_token_user_id = 2;

            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.createAccessToken = jest.fn().mockResolvedValueOnce({});
            AzureService.prototype.hasAccessTokenName = jest.fn().mockResolvedValueOnce(true);
            UserService.prototype.create = jest.fn().mockResolvedValueOnce({ user_id: access_token_user_id });

            const retrieveResponse = await request(app)
                .post(`/auth/authorization/access-token`)
                .set({ ...headers })
                .send({
                    permissions: ["access-token.read", "this-permission-does-not-exist"],
                    access_token_expiration: access_token_expiration.toISOString(),
                    access_token_name,
                    access_token_description,
                });
            expect(retrieveResponse.status).toBe(400);
        });

        describe("delete", () => {
            it("should fail (invalid UUID)", async () => {
                MockHelpers.mockAuthentification(authData);

                const retrieveResponse = await request(app)
                    .delete(`/auth/authorization/access-token/123' or displayName ne 'hello'`)
                    .set({ ...headers })
                    .send();
                expect(retrieveResponse.status).toBe(400);
                expect(retrieveResponse.body).toStrictEqual({
                    errors: [{ code: "INVALID_ACCESS_TOKEN", message: "Access token is not valid UUID.", type: "VALIDATION_ERROR" }],
                });
            });

            it("should succeed", async () => {
                MockHelpers.mockAuthentification(authData);
                AzureService.prototype.deleteAccessToken = jest.fn().mockResolvedValueOnce(15);
                AzureService.prototype.findAccessTokenUserId = jest.fn().mockResolvedValueOnce(15);
                UserRepositoryMock.prototype.deleteByUserId = jest.fn().mockResolvedValueOnce({});

                const retrieveResponse = await request(app)
                    .delete(`/auth/authorization/access-token/508b62f2-31b8-444a-855b-3b3bc8eef0e7`)
                    .set({ ...headers })
                    .send();
                expect(retrieveResponse.status).toBe(204);

                expect(AzureService.prototype.deleteAccessToken).toBeCalledWith(1, "508b62f2-31b8-444a-855b-3b3bc8eef0e7");
                expect(UserRepositoryMock.prototype.deleteByUserId).toBeCalledWith(15);
            });
        });

        it("should list (user owned)", async () => {
            MockHelpers.mockAuthentification(authData);
            AzureService.prototype.listAccessTokensAndCount = jest.fn().mockResolvedValueOnce({});

            const retrieveResponse = await request(app)
                .get(`/auth/authorization/access-token`)
                .set({ ...headers })
                .send();
            expect(retrieveResponse.status).toBe(206);

            expect(AzureService.prototype.listAccessTokensAndCount).toBeCalledWith(1, 0, 10, undefined);
        });
    });

    describe("permissions", () => {
        it("should list access token", async () => {
            MockHelpers.mockAuthentification(authData);

            const retrieveResponse = await request(app)
                .get(`/auth/authorization/permissions/access-token`)
                .set({ ...headers })
                .send();
            expect(retrieveResponse.status).toBe(200);
            expect(retrieveResponse.body).toStrictEqual([
                { permission_description: "Access token read.", permission_id: 1000, permission_name: "access-token.read" },
                { permission_description: "Access token write.", permission_id: 1001, permission_name: "access-token.write" },
            ]);
        });
    });

    it("should get user info", async () => {
        MockHelpers.mockAuthentification(authData);
        const res = await request(app)
            .get(`/auth/authorization/user`)
            .set({ ...headers, authorization: "123" })
            .send();
        expect(res.status).toBe(200);
        const { workspace_id, ...rest } = authData;
        expect(res.body).toEqual({
            ...rest,
            azure_user_id: undefined,
        });
        expect(res.header["set-cookie"]?.[0].includes("token=123")).toBeTruthy();
    });

    it("should update user settings", async () => {
        MockHelpers.mockAuthentification(authData);
        UserRepositoryMock.prototype.updateSettings = jest.fn().mockResolvedValueOnce(null);
        UserRepositoryMock.prototype.findUserById = jest
            .fn()
            .mockResolvedValueOnce({ user_settings: { auth_sources: ["socialIdpAuthentication"] } });
        const retrieveResponse = await request(app)
            .put(`/auth/authorization/settings`)
            .set({ ...headers })
            .send({
                show_onboarding: false,
                user_timezone: "timezone",
            });

        expect(UserRepositoryMock.prototype.updateSettings).toBeCalledWith(1, {
            show_onboarding: false,
            user_timezone: "timezone",
            auth_sources: ["socialIdpAuthentication"],
        });
        expect(RedisServiceMock.prototype.deleteMatchingKeys).toBeCalled();
        expect(retrieveResponse.status).toBe(204);
    });

    it("should not change user auth source", async () => {
        MockHelpers.mockAuthentification(authData);
        UserRepositoryMock.prototype.updateSettings = jest.fn().mockResolvedValueOnce(null);
        UserRepositoryMock.prototype.findUserById = jest
            .fn()
            .mockResolvedValueOnce({ user_settings: { auth_sources: ["socialIdpAuthentication"] } });

        const retrieveResponse = await request(app)
            .put(`/auth/authorization/settings`)
            .set({ ...headers })
            .send({
                show_onboarding: false,
                user_timezone: "timezone",
                auth_sources: [],
            });

        expect(UserRepositoryMock.prototype.updateSettings).toBeCalledWith(1, {
            show_onboarding: false,
            user_timezone: "timezone",
            auth_sources: ["socialIdpAuthentication"],
        });
        expect(RedisServiceMock.prototype.deleteMatchingKeys).toBeCalled();
        expect(retrieveResponse.status).toBe(204);
    });

    it("should check users limit and succeed", async () => {
        UserRepositoryMock.prototype.countAllUsers = jest.fn().mockResolvedValueOnce(10);
        AzureService.prototype.createUser = jest.fn().mockResolvedValueOnce({});
        //@ts-ignore
        process.env.MAX_USERS_COUNT = 20;

        const retrieveResponse = await request(app)
            .post(`/auth/authorization/validate-limit`)
            .set({ ...headers, "x-functions-key": "AZURE_API_KEY" })
            .send();

        expect(retrieveResponse.status).toBe(204);
        expect(UserRepositoryMock.prototype.countAllUsers).toBeCalled();
    });

    it("should check users limit and fail", async () => {
        UserRepositoryMock.prototype.countAllUsers = jest.fn().mockResolvedValueOnce(10);
        AzureService.prototype.createUser = jest.fn().mockResolvedValueOnce({});
        //@ts-ignore
        process.env.MAX_USERS_COUNT = 5;

        const retrieveResponse = await request(app)
            .post(`/auth/authorization/validate-limit`)
            .set({ ...headers, "x-functions-key": "AZURE_API_KEY" })
            .send();

        expect(retrieveResponse.status).toBe(400);
        expect(UserRepositoryMock.prototype.countAllUsers).toBeCalled();
    });
});
