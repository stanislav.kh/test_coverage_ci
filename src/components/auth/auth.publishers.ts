import { Service } from "typedi";
import { Publisher } from "../../common/events/base.publisher";
import { CreateAzureUserEvent, MergeSocialAccountEvent, SendVerificationEmailEvent, Subjects } from "./auth.contracts";

// TODO: move publishers to src/events
@Service()
export class MergeSocialAccountPublisher extends Publisher<MergeSocialAccountEvent> {
    public subject: Subjects.MergeSocialAccount = Subjects.MergeSocialAccount;
}

@Service()
export class CreateAzureUserPublisher extends Publisher<CreateAzureUserEvent> {
    public subject: Subjects.CreateAzureUser = Subjects.CreateAzureUser;
}

@Service()
export class SendVerificationEmailPublisher extends Publisher<SendVerificationEmailEvent> {
    public subject: Subjects.SendVerificationEmail = Subjects.SendVerificationEmail;
}
