import { CreateAzureUserPublisher, MergeSocialAccountPublisher, SendVerificationEmailPublisher } from "./auth.publishers";
import { AuthSource } from "./../azure-auth/azure-auth.service";
import * as jwt from "jsonwebtoken";
import Container, { Service } from "typedi";
import { CustomValidationError, ForbiddenError, UnauthorizedError } from "syntropy-common-ts";
import { getCustomRepository } from "typeorm";
import { generate as passGen } from "generate-password";
import { User } from "../users/user.model";
import { getEnv } from "../../config/env";
import { GenericHelpers } from "../../helpers/generic-helpers";
import { GenericHelpers as GenericHelpersCommon } from "syntropy-common-ts";
import { UserService } from "../users/user.service";
import {
    AuthData,
    IdTokenPayload,
    AccessTokenReadData,
    MergeSocialAccountData,
    CreateAzureUserData,
    SendVerificationEmailData,
} from "./auth.contracts";
import { AzureService } from "../azure-auth/azure-auth.service";
import { AuthorizedRequest } from "../../common/types";
import { AzureUserTokenDto } from "../azure-auth/azure-auth.contracts";
import { Logger } from "../../config/logger";
import { UserRepository } from "../users/user.repository";
import { RoleName } from "../role/role.model";
import { UserWorkspace } from "../users-workspaces/user-workspace.model";
import { UserWorkspaceRepository } from "../users-workspaces/user-workspace.repository";
import { ConfirmEmailNotificationPublisher } from "../../events/notifications/confirm-email-notification.publisher";

@Service()
export class AuthService {
    private userService: UserService = Container.get(UserService);
    private userRepository: UserRepository = getCustomRepository(UserRepository);
    private userWorkspaceRepository: UserWorkspaceRepository = getCustomRepository(UserWorkspaceRepository);
    private azureService: AzureService = Container.get(AzureService);
    private mergeSocialAccountPublisher: MergeSocialAccountPublisher = Container.get(MergeSocialAccountPublisher);
    private createAzureUserPublisher: CreateAzureUserPublisher = Container.get(CreateAzureUserPublisher);
    private sendVerificationEmailPublisher: SendVerificationEmailPublisher = Container.get(SendVerificationEmailPublisher);

    public async validateJwt(request: AuthorizedRequest, _: string, scopes: string[] = []): Promise<any> {
        // Header is used only for first authorized request before switching to cookies
        let token = request.headers.authorization?.replace("Bearer ", "");
        if ((!token || token === "undefined") && request.headers.cookie) {
            const cookies = GenericHelpers.getCookies(request.headers.cookie);
            token = cookies.token;
        }

        if (!token || token === "undefined") {
            throw new ForbiddenError();
        }

        request.authData = await this.getAuthDtoFromJwt(token, scopes);
        request.token = token;
        return request.authData;
    }

    public createIdToken(id: string, extraData: {}): string {
        if (getEnv().SECRET == null) {
            throw new Error("Invalid Syntropy controller secret key.");
        }

        const token = jwt.sign({ data: { id, ...extraData } }, getEnv().SECRET!, {
            expiresIn: getEnv().JWT_TOKEN_DURATION!,
            algorithm: "HS256",
        });
        return token;
    }

    public async verifyIdToken(token: string): Promise<{ data: IdTokenPayload }> {
        return new Promise<{ data: IdTokenPayload }>((resolve, _) => {
            jwt.verify(token.replace("Bearer ", ""), getEnv().SECRET!, (err, decoded) => {
                if (err) {
                    throw new ForbiddenError();
                } else {
                    resolve(decoded as { data: IdTokenPayload });
                }
            });
        });
    }

    public async findUserClaimsById(
        id: number
    ): Promise<
        | (Required<Pick<User, "user_id" | "user_email" | "user_settings" | "workspace_default_id">> & Pick<User, "role" | "permissions">)
        | undefined
    > {
        return this.userRepository.findUserScopeById(id);
    }

    public async findUserClaimsByWorkspaceId(
        user_id: number,
        workspace_id: number | undefined
    ): Promise<Required<Pick<UserWorkspace, "user_id" | "role">> | undefined> {
        if (workspace_id) {
            return this.userWorkspaceRepository.findByUserAndWorkspaceId(user_id, workspace_id);
        }
        return undefined;
    }

    public async getAuthDtoFromJwt(token: string | undefined, scopes: string[] = []): Promise<AuthData> {
        if (!token) {
            throw new UnauthorizedError();
        }

        const azureUser = await this.azureService.getUserFromToken(token);

        if (azureUser.accessToken != null && azureUser.accessTokenExp != null) {
            if (new Date(azureUser.accessTokenExp).getTime() < new Date().getTime()) {
                throw new ForbiddenError();
            }
        }

        if (!azureUser.internalUser || !azureUser.workspace_id || !azureUser.scopes) {
            throw new ForbiddenError();
        }

        let user_id: number;
        if (azureUser.ownerUser != null) {
            user_id = azureUser.ownerUser;
        } else {
            user_id = azureUser.internalUser;
        }

        const scopesObj = {};
        const user_scopes = azureUser.scopes.split(" ");
        for (const scope of user_scopes) {
            scopesObj[scope] = true;
        }

        if (!scopes.some((x) => scopesObj[x])) {
            throw new ForbiddenError();
        }

        // TODO: this is outside the scope of current function name "getAuthDtoFromJwt". Topic for RnD.
        const dbUser = await this.userRepository.findUserById<User & Required<Pick<User, "user_settings">>>(user_id);
        if (!dbUser) {
            throw new ForbiddenError();
        }

        return {
            user_settings: dbUser.user_settings,
            user_email: azureUser.email,
            user_scopes,
            azure_user_id: azureUser.id,
            user_id,
            workspace_id: azureUser.workspace_id,
        };
    }

    public validateApiKey(apiKey: string | string[] | undefined, scopes: string[]): void {
        if (!apiKey || (apiKey?.length && Array.isArray(apiKey))) {
            throw new ForbiddenError("Invalid api key.");
        }

        let isApiKeyFound = false;
        scopes.forEach((scope) => {
            if (getEnv()[scope] === apiKey) {
                isApiKeyFound = true;
            }
        });

        if (!isApiKeyFound) {
            throw new ForbiddenError("Invalid api key.");
        }
    }

    public async createUser(userId: string, email: string, authSource: AuthSource = AuthSource.local): Promise<void> {
        let user = await this.userService.findUserByEmail(email);
        if (!user) {
            user = await this.userService.create(email, authSource, { role: RoleName.Admin, permissions: [] }, null);
            this.createAzureUserPublisher.publish({
                userId: user.user_id,
                azureUserId: userId,
                authSource,
                email,
            });
        } else {
            await this.mergeSocialAccountPublisher.publish({
                userId: user.user_id,
                userSettings: user.user_settings,
                azureUserId: userId,
                authSource,
                email,
            });
        }
    }

    public async mergeSocialAccount({ userId, userSettings, authSource, azureUserId, email }: MergeSocialAccountData): Promise<void> {
        await this.userService.addAuthSource(userId, userSettings, authSource);
        await this.azureService.transferMFASecretForUser(userId, azureUserId);
        await this.createAzureUserPublisher.publish({ userId, azureUserId, authSource, email });
    }

    public async createAzureUser({ userId, authSource, azureUserId, email }: CreateAzureUserData): Promise<void> {
        if (AuthSource.social === authSource) {
            await this.azureService.createUserFromSocialIdp(azureUserId, userId, email);
        } else {
            await this.azureService.createUser(azureUserId, userId, email);
            this.sendVerificationEmailPublisher.publish({ azureUserId, email });
        }
    }

    public async sendVerificationEmail({ azureUserId, email }: SendVerificationEmailData): Promise<void> {
        const token = this.createIdToken(azureUserId, { isEmailVerification: true });
        Container.get(ConfirmEmailNotificationPublisher).publish({
            recipient: email,
            variables: {
                url: `${getEnv().PLATFORM_ADDRESS}/verify-email/${token}`,
            },
        });
    }

    public async createAccessToken(
        userExternalId: number,
        workspaceId: number,
        permissions: string[] = [],
        expAt: string,
        name?: string,
        description?: string
    ): Promise<string> {
        if (name && (await this.azureService.hasAccessTokenName(name, workspaceId))) {
            throw new CustomValidationError("name", "Duplicate access token name.");
        }

        const username: string = GenericHelpersCommon.getRandomString(20);
        // Complies with microsoft threat protection security policy settings (password must meet complexity requirements).
        let password: string = passGen({
            length: 19,
            lowercase: true,
            uppercase: true,
            numbers: true,
            symbols: true,
        });
        // Do not us ":" symbol. Symbol ":" is used as a separator. Any other symbol is fine.
        password = password.split(":").join("$");
        const buffer = Buffer.from(`${username}:${password}`, "utf-8");
        const access_token = buffer.toString("base64");
        const accessTokenUser = await this.userService.create(
            null,
            AuthSource.local,
            { role: RoleName.AccessToken, permissions },
            workspaceId
        );

        const tokenName = name || `token_${username}${accessTokenUser.user_created_at.getTime()}`;
        await this.azureService.createAccessToken(
            userExternalId,
            workspaceId,
            accessTokenUser.user_id,
            username,
            password,
            expAt,
            tokenName,
            description
        );
        return access_token;
    }

    public async deleteAccessToken(workspaceId: number, accessTokenId: string): Promise<void> {
        const userId = await this.azureService.deleteAccessToken(workspaceId, accessTokenId);
        if (userId) {
            await this.userRepository.deleteByUserId(userId);
        }
    }

    public async listAccessTokensAndCount(
        workspaceId: number,
        skip: number,
        take: number,
        order?: string
    ): Promise<[AccessTokenReadData[], number]> {
        return this.azureService.listAccessTokensAndCount(workspaceId, skip, take, order);
    }

    public async verifyUser(token: string): Promise<void> {
        const { data } = await this.verifyIdToken(token);
        if (!data?.id || !data?.isEmailVerification) {
            throw new ForbiddenError("Invalid token");
        }

        await this.azureService.verifyUserEmail(data.id);
    }

    public async login(email: string, password: string): Promise<AzureUserTokenDto> {
        return this.azureService.getTokenForUser({ email, password });
    }

    public async verifyCaptcha(captchaUserResponseToken?: string): Promise<void> {
        if (!captchaUserResponseToken?.length) {
            throw new CustomValidationError("invalid_captcha", "Invalid captcha.");
        }

        const { CAPTCHA_URL, CAPTCHA_SECRET } = getEnv();
        const res = await fetch(CAPTCHA_URL!, {
            method: "POST",
            body: new URLSearchParams({
                secret: CAPTCHA_SECRET!,
                response: captchaUserResponseToken,
            }).toString(),
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
        });

        const data = await res.json();
        if (!data.success) {
            throw new CustomValidationError("invalid_captcha", "Invalid captcha.");
        }
    }

    public async fixCorruptedUsers(): Promise<void> {
        try {
            const corruptedUsers = await this.azureService.getCorruptedUsers();
            Logger.info({ type: "platform", header: 200, message: `Corrupted users found: ${corruptedUsers.length}` });

            for (const u of corruptedUsers) {
                await this.createUser(u.id, u.email);
            }

            Logger.info({ type: "platform", header: 200, message: `Corrupted users fixed: ${corruptedUsers.length}` });
        } catch (e: any) {
            // Not bubbling up error
            Logger.error(e);
        }
    }

    public async validateUserLimit(): Promise<void> {
        const userCount = await this.userService.countAllUsers();

        if (userCount >= Number(getEnv().MAX_USERS_COUNT!)) {
            throw new CustomValidationError("userCount", "Maximum users count has been reached.");
        }
    }
}
