import { AuthService } from "./auth.service";
import { Message } from "node-nats-streaming";
import Container, { Service } from "typedi";

import { Listener } from "../../common/events/base.listener";
import { CreateAzureUserEvent, MergeSocialAccountEvent, SendVerificationEmailEvent, Subjects } from "./auth.contracts";

// TODO: move listeners to src/events
@Service()
export class MergeSocialAccountListener extends Listener<MergeSocialAccountEvent> {
    private authService: AuthService = Container.get(AuthService);
    public subject: Subjects.MergeSocialAccount = Subjects.MergeSocialAccount;
    public queueGroupName: undefined = undefined;

    public async onMessage(data: MergeSocialAccountEvent["data"], msg: Message): Promise<void> {
        await this.authService.mergeSocialAccount(data);
        msg.ack();
    }
}

@Service()
export class CreateAzureUserListener extends Listener<CreateAzureUserEvent> {
    private authService: AuthService = Container.get(AuthService);
    public subject: Subjects.CreateAzureUser = Subjects.CreateAzureUser;
    public queueGroupName: undefined = undefined;

    public async onMessage(data: CreateAzureUserEvent["data"], msg: Message): Promise<void> {
        await this.authService.createAzureUser(data);
        msg.ack();
    }
}

@Service()
export class SendVerificationEmailListener extends Listener<SendVerificationEmailEvent> {
    private authService: AuthService = Container.get(AuthService);
    public subject: Subjects.SendVerificationEmail = Subjects.SendVerificationEmail;
    public queueGroupName: undefined = undefined;

    public async onMessage(data: SendVerificationEmailEvent["data"], msg: Message): Promise<void> {
        await this.authService.sendVerificationEmail(data);
        msg.ack();
    }
}
