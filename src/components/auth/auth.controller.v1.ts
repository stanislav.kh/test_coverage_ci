import { isUUID } from "class-validator";
import { ApiResponse, ControllerHelpers, CustomValidationError, ForbiddenError } from "syntropy-common-ts";
import { Route, Controller, Security, Tags, SuccessResponse, OperationId, Get, Query, Post, Body, Delete, Request, Example } from "tsoa";
import Container from "typedi";
import { AuthorizedRequest } from "../../common/types";
import { getEnv } from "../../config/env";
import { AzureUserTokenDto } from "../azure-auth/azure-auth.contracts";
import { PermissionObject } from "../permission/permission.model";
import { PermissionService } from "../permission/permission.service";
import { RoleName } from "../role/role.model";
import { AccessTokenOrder, AccessTokenReadData, AccessTokenData, AccessTokenLoginRequestV1, CreateAccessTokenBody } from "./auth.contracts";
import { AuthService } from "./auth.service";

@Route("api/v1/network/auth")
export class AuthControllerV1 extends Controller {
    private authService: AuthService = Container.get(AuthService);
    private permissionService: PermissionService = Container.get(PermissionService);

    /**
     * Lists Access tokens.
     * @summary Get Access tokens
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Tags("Access tokens")
    @SuccessResponse(206, "Partial Content")
    @OperationId("V1NetworkAuthAccessTokensGet")
    @Get("access-tokens")
    public async GetAccessTokens(
        @Request() req: AuthorizedRequest,
        @Query() skip: number = 0,
        @Query() take: number = 10,
        @Query() order?: AccessTokenOrder
    ): Promise<ApiResponse<AccessTokenReadData[]>> {
        if (!req.authData.workspace_id) {
            throw new ForbiddenError();
        }
        const [accessTokens, count] = await this.authService.listAccessTokensAndCount(req.authData.workspace_id, skip, take, order);
        ControllerHelpers.setPaginationHeaders(count, take, skip, this);
        return { data: accessTokens };
    }

    /**
     * Creates Access token.
     * @summary Create Access token
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Tags("Access tokens")
    @SuccessResponse(200, "OK")
    @OperationId("V1NetworkAuthAccessTokensCreate")
    @Post("access-tokens")
    public async CreateAccessToken(
        @Request() req: AuthorizedRequest,
        @Body() body: CreateAccessTokenBody
    ): Promise<ApiResponse<AccessTokenData>> {
        if (!req.authData.workspace_id) {
            throw new ForbiddenError();
        }
        const access_token = await this.authService.createAccessToken(
            req.authData.user_id,
            req.authData.workspace_id,
            body.permissions,
            body.access_token_expiration,
            body.access_token_name,
            body.access_token_description
        );
        return { data: { access_token } };
    }

    /**
     * Deletes Access token.
     * @summary Delete Access token
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Tags("Access tokens")
    @SuccessResponse(204, "No Content")
    @OperationId("V1NetworkAuthAccessTokensDelete")
    @Delete("access-tokens/{access_token_id}")
    public async DeleteAccessToken(access_token_id: string, @Request() req: AuthorizedRequest): Promise<any> {
        if (!isUUID(access_token_id)) {
            throw new CustomValidationError("INVALID_ACCESS_TOKEN", "Access token is not valid UUID.");
        }
        if (!req.authData.workspace_id) {
            throw new ForbiddenError();
        }
        await this.authService.deleteAccessToken(req.authData.workspace_id, access_token_id);
    }

    /**
     * Retrieves a list of Access token permissions
     * @summary Get Access token permissions
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Tags("Access tokens")
    @SuccessResponse(200, "OK")
    @OperationId("V1NetworkAuthAccessTokensPermissionsGet")
    @Get("access-tokens/permissions")
    public async GetAccessTokensPermissions(): Promise<ApiResponse<PermissionObject[]>> {
        return { data: await this.permissionService.roleAvailablePermissionsList(RoleName.AccessToken) };
    }

    /**
     * Retrieve JWT from Access token.
     * @summary Login (Access token)
     */
    @Example<ApiResponse<AzureUserTokenDto>>({
        data: {
            access_token: "eyJoeX1iOXJkV2...",
            token_type: "Bearer",
            expires_in: "604800",
            refresh_token: "eyJraaQioi...",
        },
    })
    @Tags("Auth")
    @SuccessResponse(200, "OK")
    @OperationId("V1NetworkAuthAccessTokenLogin")
    @Post("access-token/login")
    public async Login(@Body() body: AccessTokenLoginRequestV1): Promise<ApiResponse<AzureUserTokenDto>> {
        const buffer = Buffer.from(body.access_token, "base64");
        const str = buffer.toString("utf-8");
        const [username, user_password] = str.split(":") as [string | undefined, string | undefined];
        if (!username?.length || !user_password?.length) {
            throw new ForbiddenError();
        }
        return { data: await this.authService.login(username, user_password) };
    }

    /**
     * Deletes session cookies.
     * @summary Logout
     */
    @Security("jwt", ["GLOBAL_ADMIN", "ADMIN", "USER"])
    @Tags("Auth")
    @SuccessResponse(204, "No Content")
    @OperationId("V1NetworkAuthLogout")
    @Post("logout")
    public async Logout(): Promise<void> {
        this.setHeader("Set-Cookie", [
            `token=undefined; HttpOnly; ${getEnv().JWT_COOKIES_OPTIONS}`,
            `refreshToken=undefined; HttpOnly; ${getEnv().JWT_COOKIES_OPTIONS}`,
        ]);
    }

    /**
     * Verifies User's email address.
     * @summary Verify email
     */
    @Tags("Auth")
    @SuccessResponse(204, "No Content")
    @OperationId("V1NetworkAuthVerifyEmail")
    @Post("verify-email/{code}")
    public async VerifyEmail(
        /**
         * Email verification code (received by mail).
         */
        code: string
    ): Promise<any> {
        await this.authService.verifyUser(code);
    }
}
