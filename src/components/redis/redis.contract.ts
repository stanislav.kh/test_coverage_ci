export enum RedisLockPresets {
    Kubernetes = "KUBERNETES_SERVICES",
    Docker = "CONTAINER_SERVICES",
    HOST = "HOST_SERVICES",
    Network = "NETWORK",
}
