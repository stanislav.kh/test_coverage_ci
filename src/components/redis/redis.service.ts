import { Service } from "typedi";
import * as redis from "redis";
import { promisify } from "util";

import { Logger } from "../../config/logger";
import { getEnv } from "../../config/env";
import { RedisLockPresets } from "./redis.contract";
import { BusyError } from "syntropy-common-ts";

@Service()
export class RedisService {
    private defaultCacheDurationInMilliseconds: number = 3600 * 1_000;
    private cacheDurationInSeconds: number | undefined = parseInt(getEnv().REDIS_CACHE_DURATION!);
    private host: string | undefined = getEnv().REDIS_HOST;
    private redisClient: redis.RedisClient | undefined = this.host ? redis.createClient({ host: this.host }) : undefined;
    private scan: ((...args: Array<string | [string, string[]]>) => Promise<[string, string[]]>) | undefined = this.redisClient
        ? promisify(this.redisClient.scan).bind(this.redisClient)
        : undefined;
    private del: ((...args: Array<string | string[]>) => Promise<number>) | undefined = this.redisClient
        ? promisify(this.redisClient.del).bind(this.redisClient)
        : undefined;
    public flushalll: (() => Promise<unknown>) | undefined = this.redisClient
        ? promisify(this.redisClient.flushall).bind(this.redisClient)
        : undefined;
    public quit: (() => Promise<unknown>) | undefined = this.redisClient
        ? promisify(this.redisClient.quit).bind(this.redisClient)
        : undefined;
    private getInternal: ((pattern: string) => Promise<string | null>) | undefined = this.redisClient
        ? promisify(this.redisClient.get).bind(this.redisClient)
        : undefined;

    public async getMatchingKeys(pattern: string): Promise<[string, string[]] | void> {
        try {
            if (this.scan) {
                return await this.scan("0", "MATCH", pattern);
            }
        } catch (e: any) {
            Logger.error({ message: e.message, type: "redis", header: 400 });
        }
    }

    public getCacheOptions(key: string): { id: string; milliseconds: number } | boolean {
        if (!this.isCacheAvailable()) {
            return false;
        }

        return {
            id: key,
            milliseconds: this.cacheDurationInSeconds ? this.cacheDurationInSeconds * 1_000 : this.defaultCacheDurationInMilliseconds,
        };
    }

    public async deleteMatchingKeys(pattern: string): Promise<void> {
        try {
            if (this.scan && this.del) {
                const keys = await this.scanAll(pattern);
                if (keys.length) {
                    await this.del(keys);
                }
            }
        } catch (e: any) {
            Logger.error({ message: e.message, type: "redis", header: 400 });
        }
    }

    public isCacheAvailable(): boolean {
        return this.host ? true : false;
    }

    private async scanAll(pattern): Promise<string[]> {
        if (this.scan) {
            const found: string[] = [];
            let cursor = "0";

            do {
                const reply = await this.scan(cursor, "MATCH", pattern);
                cursor = reply[0];
                found.push(...reply[1]);
            } while (cursor !== "0");
            return found;
        }
        return [];
    }

    public async setLockOrFail(uniqueId: number, preset: RedisLockPresets, expiresSec: number): Promise<void> {
        if (!this.isCacheAvailable()) {
            return;
        }
        const isLocked = await new Promise((res, rej) => {
            // @ts-ignore
            this.redisClient.set(`${preset}_${uniqueId}_LOCK`, "1", "NX", "EX", expiresSec, (err, reply) => {
                if (err) {
                    return rej(err);
                }

                if (reply !== "OK") {
                    return res(true);
                }

                res(false);
            });
        });
        if (isLocked) {
            throw new BusyError(uniqueId, preset);
        }
    }

    public set(key: string, value: string, expInSeconds?: number): void {
        if (!this.isCacheAvailable()) {
            return;
        }
        this.redisClient.setex(key, expInSeconds || this.cacheDurationInSeconds!, value);
    }

    public async get(pattern: string): Promise<string | null> {
        if (!this.isCacheAvailable()) {
            return null;
        }
        return await this.getInternal!(pattern);
    }

    public async removeLock(uniqueId: number, preset: RedisLockPresets): Promise<void> {
        if (!this.isCacheAvailable()) {
            return;
        }
        await this.del!(`${preset}_${uniqueId}_LOCK`);
    }
}
