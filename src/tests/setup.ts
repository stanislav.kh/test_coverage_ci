import Container from "typedi";

import { NatsAdapter } from "../adapters/nats-adapter";
import { NatsAdapterMock } from "./mocks";

Container.set(NatsAdapter, new NatsAdapterMock());
