import { PermissionRepository } from "../components/permission/permission.repository";
import { RoleRepository } from "../components/role/role.repository";
import { UserRepository } from "../components/users/user.repository";
import { UserWorkspaceRepository } from "../components/users-workspaces/user-workspace.repository";
import { BackupCodeRepository } from "../components/backup-codes/backup-code.repository";
import { QueryOpts } from "syntropy-common-ts";
import { Service } from "typedi";
import { ApiKey } from "../components/api-key/api-key.model";
import { User } from "../components/users/user.model";
import { RoleName, Role } from "../components/role/role.model";
import { Permission } from "../components/permission/permission.model";
import { BackupCodeRepositoryMock } from "../components/backup-codes/backup-code.repository.mock";
import { ApiKeyRepository } from "../components/api-key/api-key.repository";
import { WorkspaceRepository } from "../components/workspace/workspace.repository";
import { Workspace } from "../components/workspace/workspace.model";
import { WorkspaceInvitationRepository } from "../components/workspaces-invitations/workspace-invitation.repository";

export class UserRepositoryMock {
    public async save(_: string, __: string): Promise<User> {
        throw new Error("Not implemented");
    }

    public async deleteByUserId(_: number): Promise<void> {
        throw new Error("Not implemented");
    }

    public async findUserByExternalId(_: string): Promise<User | undefined> {
        throw new Error("Not implemented");
    }

    public async findUserByEmail(_: string): Promise<User> {
        throw new Error("Not implemented");
    }

    public async patchExternalId(_: string, __: string): Promise<void> {
        throw new Error("Not implemented");
    }

    public async updateSettings(_: string): Promise<User> {
        throw new Error("Not implemented");
    }

    public async countAllUsers(): Promise<number> {
        throw new Error("Not implemented");
    }

    public async findUserScopeById<TResult extends User & Required<Pick<User, "role" | "permissions">>>(
        _: number
    ): Promise<TResult | undefined> {
        throw new Error("Not implemented");
    }

    public async findUserScopeByEmail<TResult extends User & Required<Pick<User, "role" | "permissions">>>(
        _: string
    ): Promise<TResult | undefined> {
        throw new Error("Not implemented");
    }

    public async findUserById(_: number): Promise<User> {
        throw new Error("Not implemented");
    }
}

export class RoleRepositoryMock {
    public async findRoleByName(role_name: RoleName): Promise<Role | undefined> {
        switch (role_name) {
            case RoleName.User:
                return new Role({ role_id: 1, role_name });
            case RoleName.Admin:
                return new Role({ role_id: 10, role_name });
            case RoleName.GlobalAdmin:
                return new Role({ role_id: 100, role_name });
            default:
                throw new Error("Not implemented");
        }
    }
}

export class PermissionRepositoryMock {
    public async findRolePermissions(permissions: string[], role_name: RoleName): Promise<Permission[]> {
        if (!permissions.length) {
            return [];
        }

        const perms = await PermissionRepositoryMock.getAllPermissions(role_name);

        return perms.filter((p) => permissions.includes(p.permission_name));
    }

    public async findRolePermissionsList(role_name: RoleName): Promise<Permission[]> {
        const perms = PermissionRepositoryMock.getAllPermissions(role_name);

        return perms;
    }

    public static async getAllPermissions(role_name: RoleName): Promise<Permission[]> {
        let perms: Permission[] = [];
        switch (role_name) {
            case RoleName.User:
                perms = [new Permission({ permission_id: 1, permission_name: "user.read", permission_description: "User read." })];
                break;
            case RoleName.Admin:
                perms = [new Permission({ permission_id: 10, permission_name: "admin.read", permission_description: "Admin read." })];
                break;
            case RoleName.GlobalAdmin:
                perms = [
                    new Permission({
                        permission_id: 100,
                        permission_name: "global-admin.read",
                        permission_description: "Global admin read.",
                    }),
                ];
                break;
            case RoleName.AccessToken:
                perms = [
                    new Permission({
                        permission_id: 1000,
                        permission_name: "access-token.read",
                        permission_description: "Access token read.",
                    }),

                    new Permission({
                        permission_id: 1001,
                        permission_name: "access-token.write",
                        permission_description: "Access token write.",
                    }),
                ];
                break;
            default:
                throw new Error("Not implemented");
        }

        return perms;
    }
}

export class ApiKeyRepositoryMock {
    public async save(_: ApiKey): Promise<void> {
        throw new Error("Not implemented");
    }

    public async findAndCount(_: number, __: string, ___: string, ____: QueryOpts): Promise<[ApiKey[], number]> {
        throw new Error("Not implemented");
    }

    public async findOneByUserIdApiKeyId(_: number, __: number): Promise<ApiKey | undefined> {
        throw new Error("Not implemented");
    }

    public async remove(_: ApiKey): Promise<void> {
        throw new Error("Not implemented");
    }

    public async findOneBySecret(_: string): Promise<ApiKey | undefined> {
        throw new Error("Not implemented");
    }
}

export class WorkspaceRepositoryMock {
    public async findByUserId(_: number): Promise<any> {
        throw new Error("Not implemented");
    }

    public async findByUserAndWorkspace(_: number, __: number): Promise<any> {
        throw new Error("Not implemented");
    }

    public async create(_: number, __: string): Promise<any> {
        throw new Error("Not implemented");
    }

    public async remove(_: Workspace): Promise<void> {
        throw new Error("Not implemented");
    }
}

export class UserWorkspaceRepositoryMock {
    public async findByUserAndWorkspaceId(_: number, __: number): Promise<any> {
        throw new Error("Not implemented");
    }
}

export class WorkspaceInvitationRepositoryMock {
    public async findByWorkspaceId(_: number): Promise<any> {
        throw new Error("Not implemented");
    }

    public async create(_: any): Promise<any> {
        throw new Error("Not implemented");
    }

    public async deleteByWorkspaceAndInvitationIds(_: number, __: number[]): Promise<void> {
        throw new Error("Not implemented");
    }
}

export class RedisServiceMock {
    public async deleteMatchingKeys(_: string): Promise<void> {
        throw new Error("Not implemented");
    }
}

@Service()
export class NatsAdapterMock {
    public client: any = {
        close: () => {
            //
        },
        on: () => {
            //
        },
    };

    public connect = (): any =>
        Promise.resolve({
            close: () => {
                //
            },
            on: () => {
                //
            },
        });

    public close = (): any => {
        //
    };
}

export const getRepoMockImplementation = (module) => {
    switch (module) {
        case BackupCodeRepository:
            return new BackupCodeRepositoryMock();
        case UserRepository:
            return new UserRepositoryMock();
        case RoleRepository:
            return new RoleRepositoryMock();
        case PermissionRepository:
            return new PermissionRepositoryMock();
        case ApiKeyRepository:
            return new ApiKeyRepositoryMock();
        case WorkspaceRepository:
            return new WorkspaceRepositoryMock();
        case UserWorkspaceRepository:
            return new UserWorkspaceRepositoryMock();
        case WorkspaceInvitationRepository:
            return new WorkspaceInvitationRepositoryMock();
        default:
            throw new Error("Mock not implemented.");
    }
};
