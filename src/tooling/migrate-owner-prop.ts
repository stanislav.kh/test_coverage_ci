import { AzureService } from "../components/azure-auth/azure-auth.service";
import Container from "typedi";
import { Database } from "../config/database";

async function fix(): Promise<void> {
    await Database.createConnection(Database.getConnectionOptions());
    await Container.get(AzureService).getAllUsersForOwnerPropMigration();
}

fix().then(() => {
    console.info("Patching users script finished running.");
});
