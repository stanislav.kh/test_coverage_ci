import { AzureService } from "../components/azure-auth/azure-auth.service";
import Container from "typedi";
import { Database } from "../config/database";
import { getManager } from "typeorm";
import { User } from "../components/users/user.model";

async function fix(): Promise<void> {
    await Database.createConnection(Database.getConnectionOptions());

    const users = await getManager().find(User);
    await Container.get(AzureService).migrateAccessTokenWorkspacesFromDefaultUserWorkspace(users);
}

fix().then(() => {
    console.info("Patching access token workspace script finished running.");
});
