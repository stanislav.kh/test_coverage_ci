import { AuthService } from "../components/auth/auth.service";
import Container from "typedi";
import { Database } from "../config/database";
import { NatsAdapter } from "../adapters/nats-adapter";
import { getEnv, loadDefaultsAndTestEnv } from "../config/env";

async function fix(): Promise<void> {
    loadDefaultsAndTestEnv();
    await Database.createConnection(Database.getConnectionOptions());
    (await Container.get(NatsAdapter).connect(getEnv().NATS_CLUSTER_ID!, "auth-service-script", getEnv().NATS_URL!)).on("close", () => {
        process.exit(0);
    });
    await Container.get(AuthService).fixCorruptedUsers();
    setTimeout(() => {
        Container.get(NatsAdapter).close();
    }, 1000 * 60 * 10);
}

fix().then(() => {
    console.info("Corrupted users script finished running.");
});
