// this is required in order for ES APM to start before any ES module is imported/checked by TS
// it automatically uses predefined variables from .env file such as ELASTIC_APM_SECRET_TOKEN
require("dotenv").config();
if (process.env.ELASTIC_APM_SECRET_TOKEN && process.env.ELASTIC_APM_SERVER_URL && process.env.ELASTIC_APM_ENVIRONMENT) {
    require("elastic-apm-node").start();
}
import { Logger } from "./config/logger";
import { controller } from "./controller";

controller()
    .start()
    .catch((err) => {
        Logger.error({ message: err, header: 500, type: "main" });
        process.exit(1);
    });
