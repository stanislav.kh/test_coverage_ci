import { Service } from "typedi";
import { getEnv } from "../../config/env";
import { AgentObject } from "./controller.contracts";
import { HttpClient, RequestParams } from "../http-client";

@Service()
export class ControllerClient<TSecurityDataType = unknown> extends HttpClient<TSecurityDataType> {
    constructor() {
        super({
            baseUrl: getEnv().CONTROLLER_SERVICE_BASE_URL,
        });
    }

    public async findAgentsByApiKey(apiKeyId: number, params: RequestParams = {}): Promise<AgentObject[]> {
        const res = await this.request<AgentObject[], void>({
            path: `/api/agents/api-key/${apiKeyId}`,
            method: "GET",
            secure: true,
            format: "json",
            headers: {
                ...params.headers,
                "x-api-key": getEnv().CONTROLLER_SERVICE_API_KEY,
            },
            ...params,
        });

        return res.data;
    }
}
