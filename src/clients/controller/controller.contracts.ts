export enum PlatformAgentTypeLocal {
    Linux = "Linux",
    MacOS = "macOS",
    Windows = "Windows",
    Virtual = "Virtual",
}

export enum PlatformAgentStatus {
    OK = "OK",
    WG_ERROR = "WG_ERROR",
    DUPLICATE_NAME_ERROR = "DUPLICATE_NAME_ERROR",
}

export interface AgentLockedFields {
    agent_location_country?: boolean;
    agent_location_city?: boolean;
    agent_location_lat?: boolean;
    agent_location_lon?: boolean;
    agent_provider_name?: boolean;
    agent_tags?: string[];
    agent_name?: boolean;
    [key: string]: any;
}

export interface AgentObject {
    agent_services_default_status: boolean;
    api_key_id: number;
    agent_public_ipv4: string;
    agent_location_lat?: number;
    agent_location_lon?: number;
    agent_location_city?: string;
    agent_location_country?: string;
    agent_device_id: string;
    agent_name: string;
    agent_status?: PlatformAgentStatus;
    agent_version?: string;
    agent_provider_id?: number;
    agent_is_online?: boolean;
    agent_locked_fields?: AgentLockedFields;
    agent_modified_at?: string;
    agent_is_virtual?: boolean;
    agent_type?: PlatformAgentTypeLocal;
    [key: string]: any;
}
