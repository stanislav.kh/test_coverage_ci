FROM node:16-alpine

RUN apk update \
    && apk add --virtual build-dependencies git \
    build-base \
    gcc

RUN apk --no-cache add --virtual native-deps \
    g++ gcc libgcc libstdc++ linux-headers make python3 \
    && rm -rf /var/cache/apk/*

RUN mkdir -p /app
COPY *.json /app/
COPY src/ /app/src/
RUN cd /app && npm install && npm run build
EXPOSE 8080
WORKDIR "/app"
ARG VERSION
ENV VERSION=${VERSION}
CMD [ "npm", "run", "start"] 

