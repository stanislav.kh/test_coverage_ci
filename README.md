# NOIA Auth Service

This README documents whatever steps are necessary to get NOIA Auth Service up and running.

# Quickstart (TL;DR;)

1. Run PostgreSQL database.

2. Configure `.env` file (see configuration section) based on `.env.example`.

3. Install node dependencies

4. Run Redis

5. Run Nats Streaming Server

```
npm install
```

5. Build routes and swagger.

```
npm run build
```

5. Start NOIA Auth Service (append `>/dev/null 2>&1` to start in silent mode).

```
npm run start
```

6. Inspect `http://[ip:port|domain]/auth/ui` endpoint.

# Setup

## Preconditions to run NOIA Auth Service

-   PostgreSQL database (can be run via included `docker-compose.yml` file)
-   NodeJS
-   Docker (if using PostgreSQL from container)
-   Redis
-   Nats streaming server

## Configuration

Use `/src/config/env.ts` as a base for `.env` configuration file. Refer to `./src/contracts.ts` `EnvConfig` for `.env` options.

## Installing NOIA Auth Service dependencies

Only first time to install dependencies:

```
npm install
```

## Running NOIA Auth Service

```
npm run start
```

## Running NOIA Auth Service in silent mode

```
npm run start >/dev/null 2>&1
```

Once NOIA contAuth Service roller is started, swagger docs can be accessed via `http://[ip:port|domain]/auth/ui` endpoint.

## Running NOIA Auth Service tests

```
npm run test:usecases
```

## Using PostgreSQL from container

### Running PostgreSQL and PgAdmin (browser based PostgreSQL client):

```
docker-compose up
```

### Destroying PostgreSQL and PgAdmin volumes:

```
docker-compose down -v
```

### Populating database migrations:

```
npm run migrations:run
```

## Azure AD B2C Policies

1. To build the policies, install `Azure AD B2C extension` from VS Code marketplace. Ensure "Runtime Status" in the extension does not show any error (install some older version if it does)

2. Run `B2C Policy build` command from VS Code command menu.
